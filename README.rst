========================
SAM Spaghetti
========================

.. {# pkglts, doc

.. #}

**SAM Sequence Primordia Alignment, GrowtH Estimation, Tracking & Temporal Indexation**

:Author: Guillaume Cerutti
:Contributors:  Christophe Godin, Jonathan Legrand, Carlos Galvan-Ampudia, Teva Vernoux

:Teams:  `RDP <http://www.ens-lyon.fr/RDP/>`_ Team Signal, Inria project team `Mosaic <https://team.inria.fr/mosaic/>`_

:Institutes: `Inria <http://www.inria.fr>`_, `INRA <https://inra.fr>`_, `CNRS <https://cnrs.fr>`_

:Language: Python

:Supported OS: Linux, MacOS

:Licence: `Cecill-C`

See the `documentation website <https://mosaic.gitlabpages.inria.fr/publications/sam_spaghetti/>`_ for installation guidelines, examples and tutorials.

