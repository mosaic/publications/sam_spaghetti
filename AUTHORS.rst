=======
Credits
=======

Development Lead
----------------

.. {# pkglts, doc.authors

* Guillaume Cerutti, <guillaume.cerutti@inria.fr>

.. #}

Contributors
------------

.. {# pkglts, doc.contributors

* Guillaume Cerutti <guillaume.cerutti@inria.fr>
* CERUTTI Guillaume <guillaume.cerutti@inria.fr>

.. #}
