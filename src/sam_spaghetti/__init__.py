"""
SAM Sequence Primordia Alignment, GrowtH Estimation, Tracking & Temporal Indexation
"""
# {# pkglts, src
# FYEO
# #}
# {# pkglts, version, after src
from . import version

__version__ = version.__version__
# #}
