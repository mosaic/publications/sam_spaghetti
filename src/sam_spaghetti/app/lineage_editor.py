import os
import argparse

from PyQt5 import Qt, QtGui, QtWidgets
from PyQt5.QtCore import pyqtSignal

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.collections import PolyCollection

from visu_core.matplotlib import glasbey
from visu_core.matplotlib.figure import figure_widgets

from sam_spaghetti.sam_sequence_info import get_experiment_name
from sam_spaghetti.sequence_growth_estimation import segmentation_element_topomeshes

from cellcomplex.property_topomesh.visualization.matplotlib import mpl_draw_topomesh


class LineageEditorInteractorStyle(Qt.QObject):
    dragged = pyqtSignal(float,float)
    zoomed = pyqtSignal(float)
    cell_picked = pyqtSignal(int)
    unpicked = pyqtSignal()
    deleted = pyqtSignal()

    def __init__(self, topomesh, figure):

        super().__init__()

        self.topomesh = topomesh
        self.figure = figure

        self.connects = {}

        self.shift = False
        self.dragging = False

        self.drag_initial_point = None

        self.artist = None

        self.wisp_id = None
        self.highlight_artist = []

    def set_topomesh(self, topomesh):
        self.clear()
        self.topomesh = topomesh

    def clear(self):
        for c in self.connects.keys():
            self.figure.canvas.mpl_disconnect(self.connects[c])
        self.connects = {}

        if self.artist is not None:
            self.artist.set_picker(None)

        self.wisp_id = None
        self.refresh()

    def set_artist(self, artist):
        self.clear()
        self.artist = artist
        self.connect()

    def connect(self):
        self.connects['press'] = self.figure.canvas.mpl_connect('button_press_event', self.press)
        self.connects['move'] = self.figure.canvas.mpl_connect('motion_notify_event', self.move)
        self.connects['release'] = self.figure.canvas.mpl_connect('button_release_event', self.release)
        self.connects['key_press'] = self.figure.canvas.mpl_connect('key_press_event', self.on_key_press)
        self.connects['key_release'] = self.figure.canvas.mpl_connect('key_release_event', self.on_key_release)
        self.connects['scrolll'] = self.figure.canvas.mpl_connect('scroll_event', self.scroll)

    def refresh(self):
        if self.highlight_artist is not None:
            for a in self.highlight_artist:
                a.remove()
                del a
        if self.wisp_id is not None:
            cell_faces = list(self.topomesh.borders(3, self.wisp_id))
            cell_face_vertices = [list(self.topomesh.borders(2, fid, 2)) for fid in cell_faces]
            cell_center = self.topomesh.wisp_property('barycenter', 3)[self.wisp_id]
            cell_face_points = self.topomesh.wisp_property('barycenter', 0).values(cell_face_vertices)
            cell_face_points = cell_center + 0.9 * (cell_face_points - cell_center)
            self.highlight_artist = [PolyCollection([p[:, :2]],
                                                    color='y', linewidth=0,
                                                    alpha=0.2) for p in cell_face_points]
            for a in self.highlight_artist:
                self.figure.gca().add_collection(a)
        else:
            self.highlight_artist = None

    def press(self, event):
        if self.shift:
            self.dragging = True
            self.drag_initial_point = (event.xdata, event.ydata)
        else:
            picked, ind = self.artist.contains(event)
            if picked:
                fid = list(self.topomesh.wisps(2))[ind["ind"][0]]
                wid = list(self.topomesh.regions(2,fid))[0]

                self.wisp_id = wid
                self.cell_picked.emit(self.wisp_id)
            else:
                self.wisp_id = None
                self.unpicked.emit()

        self.refresh()

    def move(self, event):
        if self.dragging:
            point = (event.xdata, event.ydata)
            self.dragged.emit(point[0] - self.drag_initial_point[0],point[1] - self.drag_initial_point[1])
        self.refresh()

    def release(self, event):
        self.dragging = False
        self.drag_initial_point = None
        self.refresh()

    def on_key_press(self, event):
        if event.key == 'shift':
            self.shift = True
        if event.key == 'backspace':
            self.deleted.emit()

    def on_key_release(self, event):
        if event.key == 'shift':
            self.shift = False

    def scroll(self, event):
        step = event.step

        point = [event.xdata, event.ydata]

        xlim = list(self.figure.gca().get_xlim())
        x_size = xlim[1]-xlim[0]
        point_x = (point[0] - xlim[0]) / x_size

        ylim = list(self.figure.gca().get_ylim())
        y_size = ylim[1]-ylim[0]
        point_y = (point[1] - ylim[0]) / y_size

        self.zoomed.emit(-step)
        self.dragged.emit(-step * (point_x-0.5) * x_size/100.,
                          -step * (point_y-0.5) * y_size/100.)


class LineageEditor(QtWidgets.QWidget):
    save_pressed = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)

        self.topomesh_series = {}
        self.lineage = {}
        self.filenames = []
        self.time_points = []
        self.lineage_time = None

        self.daughter_label = None
        self.lineage_artists = {}

        self.layout = QtWidgets.QHBoxLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.figures = {}
        self.figure_widgets = {}
        self.extent = [1,1]
        self.centers = {}
        self.figure_interactors = {}
        for figure_index in [0,1]:
            self.figures[figure_index] = plt.figure(figure_index)
            self.figure_widgets[figure_index] = figure_widgets[figure_index]
            self.figure_widgets[figure_index].setToolTip(None)
            self.figure_widgets[figure_index].setSizePolicy(Qt.QSizePolicy.Expanding, Qt.QSizePolicy.Expanding)
            self.layout.addWidget(self.figure_widgets[figure_index])
            self.centers[figure_index] = [0,0]
            self.figure_interactors[figure_index] = LineageEditorInteractorStyle(None, self.figures[figure_index])
            self.figure_interactors[figure_index].connect()
            self.figure_interactors[figure_index].dragged.connect(self.drag)
            self.figure_interactors[figure_index].zoomed.connect(self.zoom)

        self.figure_interactors[1].cell_picked.connect(self.pick_daughter_cell)
        self.figure_interactors[1].unpicked.connect(self.pick_daughter_cell)
        self.figure_interactors[0].cell_picked.connect(self.pick_mother_cell)
        self.figure_interactors[1].deleted.connect(self.delete_lineage)

        self.button_layout = QtWidgets.QVBoxLayout()
        self.button_pane = QtWidgets.QWidget(self)
        self.layout.addWidget(self.button_pane)

        self.prev_button = None
        self.next_button = None
        self.initialize_button_pane()

    def initialize_button_pane(self):
        self.button_layout.setContentsMargins(0, 0, 0, 0)

        save_button = QtWidgets.QPushButton("Save Lineage")
        self.button_layout.addWidget(save_button)
        save_button.clicked.connect(lambda: self.save_pressed.emit())

        prev_next_layout = QtWidgets.QHBoxLayout()
        prev_next_layout.setContentsMargins(0, 0, 0, 0)

        self.prev_button = QtWidgets.QPushButton("< < <")
        prev_next_layout.addWidget(self.prev_button)
        self.prev_button.clicked.connect(self.to_previous_time_point)

        self.next_button = QtWidgets.QPushButton("> > >")
        prev_next_layout.addWidget(self.next_button)
        self.next_button.clicked.connect(self.to_next_time_point)

        prev_next_widget = QtWidgets.QWidget()
        prev_next_widget.setLayout(prev_next_layout)
        self.button_layout.addWidget(prev_next_widget)

        self.button_pane.setFixedWidth(300)
        self.button_pane.setLayout(self.button_layout)

    def set_topomesh_series(self, topomesh_series):
        assert len(topomesh_series) > 1

        self.topomesh_series = topomesh_series
        self.filenames = np.sort(list(self.topomesh_series.keys()))
        self.time_points = np.array([int(f[-2:]) for f in self.filenames])

        self.extent = [0,0]
        for i_t, (time, filename) in enumerate(zip(self.time_points,self.filenames)):
            topomesh = self.topomesh_series[filename]
            points = topomesh.wisp_property('barycenter',0).values()
            self.extent = np.maximum(self.extent, np.max(points,axis=0)[:2]-np.min(points,axis=0)[:2])
            self.centers[time] = np.mean(points,axis=0)[:2]
            self.lineage_artists[time] = {}

        self.extent /= 10
        self.lineage_time = self.time_points[0]

        self.prev_button.setEnabled(False)
        if len(self.time_points) < 3:
            self.next_button.setEnabled(False)

        self.set_lineage()

        self.refresh_plot()

    def set_lineage(self, lineage=None):
        if lineage is not None:
            self.lineage = lineage
        else:
            self.lineage = {t:{} for t in self.time_points}

        self.draw_lineage()

    def to_next_time_point(self):
        time_index = np.where(self.time_points==self.lineage_time)[0][0]
        self.lineage_time = self.time_points[time_index + 1]
        self.prev_button.setEnabled((time_index + 1) > 0)
        self.next_button.setEnabled((time_index + 1) < (len(self.time_points) - 2))
        self.refresh_plot()
        self.draw_lineage()

    def to_previous_time_point(self):
        time_index = np.where(self.time_points == self.lineage_time)[0][0]
        self.lineage_time = self.time_points[time_index - 1]
        self.prev_button.setEnabled((time_index - 1) > 0)
        self.next_button.setEnabled((time_index - 1) < (len(self.time_points) - 2))
        self.refresh_plot()
        self.draw_lineage()

    def refresh_plot(self):
        for figure_index in [0,1]:
            figure = self.figures[figure_index]
            time_index = np.where(self.time_points==self.lineage_time)[0][0] + figure_index
            topomesh = self.topomesh_series[self.filenames[time_index]]

            self.figure_interactors[figure_index].set_topomesh(topomesh)

            figure.clf()
            cell_polys = mpl_draw_topomesh(topomesh,figure,3,alpha=0.2)
            mpl_draw_topomesh(topomesh,figure,1,linewidth=0.5,alpha=0.1)
            mpl_draw_topomesh(topomesh,figure,1,cell_edges=True,linewidth=2)

            self.figure_interactors[figure_index].set_artist(cell_polys)

            figure.gca().axis('off')
            figure.gca().axis('equal')
            figure.tight_layout()

        self.update_plot_limits()

    def drag(self,dx,dy):
        for time in self.time_points:
            self.centers[time][0] -= dx
            self.centers[time][1] -= dy
        self.update_plot_limits()

    def zoom(self,dz):
        self.extent *= 1. + dz/100.
        self.update_plot_limits()

    def pick_daughter_cell(self, label=None):
        self.daughter_label = label

    def pick_mother_cell(self, label):
        if self.daughter_label is not None:
            self.lineage[self.lineage_time][self.daughter_label] = label
            self.draw_lineage()
            self.daughter_label = None
            self.figure_interactors[1].wisp_id = None
            self.figure_interactors[1].refresh()
        self.figure_interactors[0].wisp_id = None
        self.figure_interactors[0].refresh()

    def delete_lineage(self):
        if self.daughter_label is not None:
            if self.daughter_label in self.lineage[self.lineage_time].keys():
                del self.lineage[self.lineage_time][self.daughter_label]
                self.draw_lineage()
                self.daughter_label = None
                self.figure_interactors[1].wisp_id = None
                self.figure_interactors[1].refresh()
                self.figure_interactors[0].wisp_id = None
                self.figure_interactors[0].refresh()

    def update_plot_limits(self):
        for figure_index in [0, 1]:
            time_index = np.where(self.time_points==self.lineage_time)[0][0] + figure_index
            time = self.time_points[time_index]
            figure = self.figures[figure_index]
            figure.gca().set_xlim(self.centers[time][0]-self.extent[0]/2,
                                  self.centers[time][0]+self.extent[0]/2)
            figure.gca().set_ylim(self.centers[time][1]-self.extent[1]/2,
                                  self.centers[time][1]+self.extent[1]/2)

    def draw_lineage(self):
        lineage = self.lineage[self.lineage_time]
        lineage_array = np.transpose([list(lineage.values()),list(lineage.keys())])

        for figure_index in [0,1]:
            figure = self.figures[figure_index]

            lineage_cells = [c for c,f in self.lineage_artists[self.lineage_time].keys() if f == figure_index]
            for c in lineage_cells:
                self.lineage_artists[self.lineage_time][(c,figure_index)].remove()
                del self.lineage_artists[self.lineage_time][(c,figure_index)]

            time_index = np.where(self.time_points==self.lineage_time)[0][0] + figure_index
            topomesh = self.topomesh_series[self.filenames[time_index]]

            cell_to_plot = lineage_array[:,figure_index]
            mother_labels = lineage_array[:,0]
            daughter_labels = lineage_array[:,1]

            positions = topomesh.wisp_property('barycenter',0)

            for c,m,d in zip(cell_to_plot,mother_labels,daughter_labels):
                cell_faces = topomesh.wisp_property('borders',3)[c]
                oriented_cell_faces = topomesh.wisp_property('oriented_vertices', 2).values(cell_faces)
                oriented_cell_face_positions = positions.values(oriented_cell_faces)
                colors = ['glasbey_'+str(m%256) for _ in cell_faces]
                poly_collection = PolyCollection([p[:, :2] for p in oriented_cell_face_positions],facecolors=colors)
                self.lineage_artists[self.lineage_time][(c,figure_index)] = poly_collection
                figure.gca().add_collection(poly_collection)


def main():
    """
    Returns
    -------
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('-dir', '--data-directory', help='Path to SAM sequence data files directory (nomenclature, orientation...)', required=True)
    parser.add_argument('-o', '--output-directory', help='Path to detected nuclei directory [default : data_directory/nuclei_images]', default=None)
    parser.add_argument('-e', '--experiment', help='Experiment identifiers', required=True)
    parser.add_argument('-sam', '--sam-id', help='List of individual SAM identifiers to process (if found)', type=int, required=True)

    args = parser.parse_args()

    data_dirname = args.data_directory
    image_dirname = args.output_directory if args.output_directory is not None else data_dirname+"/nuclei_images"

    experiment_name = get_experiment_name(args.experiment, data_dirname)
    sequence_name = experiment_name+"_sam"+str(args.sam_id).zfill(2)

    sequence_element_topomesh = segmentation_element_topomeshes(sequence_name, image_dirname, surface=True, verbose=True)

    Qt.QLocale.setDefault(Qt.QLocale.c())

    app = Qt.QApplication([])

    window = Qt.QMainWindow()
    window.setWindowTitle("Lineage Editor")

    editor = LineageEditor()
    editor.set_topomesh_series(sequence_element_topomesh)

    def load_sequence_lineage():
        sequence_lineages = {}

        sequence_filenames = []
        for time in range(100):
            filename = sequence_name + "_t" + str(time).zfill(2)
            if os.path.exists(image_dirname + "/" + sequence_name + "/" + filename + "/" + filename + "_l1_cell_topomesh.ply"):
                sequence_filenames += [filename]

        for i_file, (filename, mother_filename) in enumerate(zip(sequence_filenames[1:],sequence_filenames[:-1])):
            file_dirname = image_dirname + "/" + sequence_name + "/" + filename
            mother_time = int(mother_filename[-2:])
            lineage_filename = file_dirname + "/" + filename + "_to_t" + str(mother_time).zfill(2) + "_manual_cell_lineage.csv"
            if os.path.exists(lineage_filename):
                lineage_df = pd.read_csv(lineage_filename)
                sequence_lineages[mother_time] = dict(lineage_df[['label','mother_label']].values)
            else:
                sequence_lineages[mother_time] = {}

        return sequence_lineages

    editor.set_lineage(load_sequence_lineage())

    def save_sequence_lineage():
        for time in editor.lineage.keys():
            lineage = editor.lineage[time]
            time_index = np.where(editor.time_points==time)[0][0]
            daughter_time = editor.time_points[time_index+1]
            lineage_df = pd.DataFrame()
            lineage_df['label'] = [l for l in lineage.keys()]
            lineage_df['time'] = [daughter_time for _ in range(len(lineage))]
            lineage_df['mother_label'] = [l for l in lineage.values()]
            lineage_df['mother_time'] = [time for _ in range(len(lineage))]

            daughter_filename = editor.filenames[time_index+1]
            daughter_dirname = image_dirname + "/" + sequence_name + "/" + daughter_filename

            lineage_filename = daughter_dirname + "/" + daughter_filename + "_to_t" + str(time).zfill(2) + "_manual_cell_lineage.csv"
            lineage_df.to_csv(lineage_filename,index=False)

    editor.save_pressed.connect(save_sequence_lineage)

    window.setCentralWidget(editor)
    window.show()

    def ask_to_save():
        message = QtWidgets.QMessageBox(parent=editor)
        message.setWindowModality(Qt.Qt.WindowModal)
        message.setIcon(QtWidgets.QMessageBox.Warning)
        message.setText("Do you want to save lineages before exiting?")
        message.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)

        if message.exec_() == QtWidgets.QMessageBox.Yes:
            save_sequence_lineage()

    app.aboutToQuit.connect(ask_to_save)
    app.exec_()


if __name__ == '__main__':
    main()
