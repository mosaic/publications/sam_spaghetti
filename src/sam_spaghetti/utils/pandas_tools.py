import re

import numpy as np
import pandas as pd


def array_from_printed_string(string, null_value=np.nan):
    if not pd.isnull(string):
        s = re.sub("[ ]+"," ",string)
        s = re.sub("\[ ","[",s)
        s = re.sub("\n","",s)
        s = re.sub(" ",",",s)
        s = re.sub("nan","np.nan",s)
        return np.array(eval(s))
    else:
        return null_value