import numpy as np
import pandas as pd

import sam_spaghetti
from sam_spaghetti.sam_microscopy_loading import load_image_from_microscopy
from sam_spaghetti.sam_sequence_info import get_experiment_name, get_experiment_microscopy, get_nomenclature_name, get_experiment_channels, get_experiment_membrane, \
    get_sequence_orientation, get_experiment_microscope_orientation, update_lut_ranges
from sam_spaghetti.segmentation_quantification import segment
from sam_spaghetti.detection_quantification import quantify_from_segmentation, quantify_cell_signal
from sam_spaghetti.polarity_quantification import extract_sequence_walls, compute_sequence_wall_signals
from sam_spaghetti.sam_sequence_loading import load_sequence_signal_images, load_sequence_segmented_images, load_sequence_signal_image_slices, load_sequence_signal_data, load_sequence_wall_meshes, load_sequence_signal_wall_data
from sam_spaghetti.signal_image_slices import sequence_signal_image_slices
from sam_spaghetti.signal_image_plot import signal_image_plot, segmented_image_plot, signal_wall_plot
from sam_spaghetti.signal_data_compilation import compile_cell_data

from timagetk.io import imsave

import logging
import argparse
import os

from timagetk.algorithms.reconstruction import pts2transfo

guillaume_dirname = "/Users/gcerutti/Data/"
calculus_dirname = "/projects/SamMaps/"
sam_spaghetti_dirname = sam_spaghetti.__path__[0]+"/../../share/data"

# dirname = guillaume_dirname
# dirname = calculus_dirname
dirname = sam_spaghetti_dirname

max_sam_id = 100
max_time = 100

plot_choices = ['sequence_raw']

def main():
    """

    Returns:

    """

    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--experiments', help='List of experiment identifiers', nargs='+', required=True)
    parser.add_argument('-sam', '--sam-ids', default=range(max_sam_id), nargs='+', help='List of individual SAM identifiers to process (if found)', type=int)
    parser.add_argument('-dir', '--data-directory', help='Path to SAM sequence data files directory (nomenclature, orientation...)', default=dirname)
    parser.add_argument('-Mdir', '--microscopy-directory', help='Path to CZI image directory [default : data_directory/microscopy]', default=None)
    parser.add_argument('-o', '--output-directory', help='Path to detected nuclei directory [default : data_directory/nuclei_images]', default=None)

    parser.add_argument('-a', '--all', default=False, action='store_true', help='Run all the segmentation and quantification steps')

    parser.add_argument('-M', '--from-microscopy', default=False, action='store_true', help='Whether to load images from the microscopy image directory')
    parser.add_argument('-S', '--segmentation', default=False, action='store_true', help='Run membrane segmentation on all experiments')
    parser.add_argument('-hmin', '--h-min', default=1000, type=int, help='Value of the h-min used for seed detection in watershed segmentation')
    parser.add_argument('-Vmax', '--max-volume', default=1e3, type=float, help='Maximum cell volume in segmentation')
    parser.add_argument('-W', '--wall-extraction', default=False, action='store_true', help='Run sequence wall mesh extraction on all experiments')
    parser.add_argument('-Q', '--quantification', default=False, action='store_true', help='Run wall signal quantification of image intensities on all experiments')

    parser.add_argument('-i', '--image-plot', default=[], nargs='+', help='List of image projections types to plot',choices=plot_choices)
    parser.add_argument('-p', '--projection-type', default='max_intensity', help='Projection type for the image plots',choices=['max_intensity', 'L1_slice'])
    parser.add_argument('-c', '--cell-plot', default=[], nargs='+', help='List of signal map types to plot', choices=plot_choices)
    parser.add_argument('-w', '--wall-plot', default=[], nargs='+', help='List of wall types to plot', choices=plot_choices)
    parser.add_argument('-N', '--normalized', default=False, action='store_true', help='Display normalized signals when plotting')
    parser.add_argument('-C', '--data-compilation', default=False, action='store_true', help='Compile all the data from the experiments into .csv files in the data directory')

    parser.add_argument('-v', '--verbose', default=False, action='store_true', help='Verbose')
    parser.add_argument('-d', '--debug', default=False, action='store_true', help='Debug')

    args = parser.parse_args()

    logging.getLogger().setLevel(logging.INFO if args.verbose else logging.DEBUG if args.debug else logging.ERROR)

    data_dirname = args.data_directory

    microscopy_dirname = args.microscopy_directory if args.microscopy_directory is not None else data_dirname+"/microscopy"
    if not os.path.exists(microscopy_dirname):
        logging.warning(microscopy_dirname+" does not exist!")
        logging.warning("Microscopy directory not found! No detection will be performed.")

    experiments = args.experiments
    sam_ids = args.sam_ids
    image_dirname = args.output_directory if args.output_directory is not None else data_dirname+"/nuclei_images"

    update_lut_ranges(data_dirname)

    for exp in experiments:
        experiment_name = get_experiment_name(exp,data_dirname)
        if experiment_name == "":
            logging.error("Experiment identifier \""+exp+"\" not recognized (consider adding it to the experiment_info.csv file in the data directory)")
            experiments.remove(exp)
        else:
            if args.from_microscopy and (microscopy_dirname is not None):
                experiment_dirname = microscopy_dirname+"/"+get_experiment_microscopy(exp,data_dirname)
                # if os.path.exists(experiment_dirname+"/RAW"):
                #     experiment_dirname += "/RAW"

                if not os.path.exists(experiment_dirname):
                    logging.warning("Microscopy directory not found for "+exp+", no segmentation will be performed.")
                else:
                    microscopy_filenames = [experiment_dirname+"/"+f for f in os.listdir(experiment_dirname) if np.any([ext in f for ext in ['.czi','.lsm','.tif']])]
                    nomenclature_names = [get_nomenclature_name(microscopy_filename,data_dirname,experiment=exp) for microscopy_filename in microscopy_filenames]
                    microscopy_filenames = [m for m,n in zip(microscopy_filenames,nomenclature_names) if n is not None]

                    channel_names = get_experiment_channels(exp, data_dirname)

                    if not os.path.exists(image_dirname):
                        os.makedirs(image_dirname)

                    for microscopy_filename in microscopy_filenames:
                        nomenclature_name = get_nomenclature_name(microscopy_filename, data_dirname, experiment=exp)

                        if nomenclature_name is not None:
                            sequence_name = nomenclature_name[:-4]

                            sam_id = int(sequence_name[-2:])

                            if sam_id in sam_ids:
                                if not os.path.exists(image_dirname+"/"+sequence_name):
                                    os.makedirs(image_dirname+"/"+sequence_name)
                                if not os.path.exists(image_dirname+"/"+sequence_name+"/"+nomenclature_name):
                                    os.makedirs(image_dirname+"/"+sequence_name+"/"+nomenclature_name)

                                load_image_from_microscopy(microscopy_filename, save_images=True, image_dirname=image_dirname, nomenclature_name=nomenclature_name, channel_names=channel_names, verbose=args.verbose, debug=args.debug, loglevel=1)
                        else:
                            logging.warning("--> No nomenclature found for " + microscopy_filename + ", skipping...")

    if not os.path.exists(image_dirname):
        logging.error("Result output directory not found, nothing left to do!")
    else:
        sequence_names = {}
        for exp in experiments:
            experiment_name = get_experiment_name(exp,data_dirname)
            membrane_name = get_experiment_membrane(exp, data_dirname)
            logging.info("--> Loading sequences for experiment "+str(exp))

            sequence_names[exp] = []
            for sam_id in sam_ids:
                sequence_name = experiment_name+"_sam"+str(sam_id).zfill(2)
                logging.debug("--> Trying to load sequence "+str(sequence_name))

                signal_names = [membrane_name]
                signal_images = load_sequence_signal_images(sequence_name, image_dirname,signal_names=signal_names,verbose=args.verbose, debug=args.debug, loglevel=1)
                if len(signal_images) > 0:
                    sequence_names[exp] += [sequence_name]
                    logging.debug("--> Loaded sequence "+str(sequence_name)+"!")

        if args.segmentation or args.all:
            for exp in experiments:
                membrane_name = get_experiment_membrane(exp, data_dirname)
                microscope_orientation = get_experiment_microscope_orientation(exp, data_dirname)

                for sequence_name in sequence_names[exp]:
                    logging.info("--> Sequence image segmentation " + sequence_name)
                    membrane_images = load_sequence_signal_images(sequence_name, image_dirname, signal_names=[membrane_name], verbose=args.verbose, debug=args.debug, loglevel=1)

                    for filename in membrane_images[membrane_name].keys():
                        img_dict = {membrane_name:membrane_images[membrane_name][filename]}
                        h_min = args.h_min if membrane_images[membrane_name][filename].dtype == np.uint16 else 2
                        volume_threshold = args.max_volume
                        logging.info("--> Running segmentation on "+filename+" "+membrane_name)
                        seg_img = segment(img_dict, membrane_name, save_files=True, image_dirname=image_dirname, nomenclature_name=filename,
                                          h_min=h_min, volume_threshold=volume_threshold, verbose=args.verbose, debug=args.debug, loglevel=1)
                        segmentation_filename = image_dirname + "/" + sequence_name + "/" + filename + "/" + filename + "_" + membrane_name + "_seg.tif"
                        imsave(segmentation_filename, seg_img)

                        quantify_from_segmentation(seg_img, save_files=True, image_dirname=image_dirname, nomenclature_name=filename, microscope_orientation=microscope_orientation,
                                                   surface_matching='cell', verbose=args.verbose, debug=args.debug, loglevel=1)

        for exp in experiments:
            membrane_name = get_experiment_membrane(exp, data_dirname)
            signal_names = get_experiment_channels(exp, data_dirname)
            microscope_orientation = get_experiment_microscope_orientation(exp, data_dirname)
            for sequence_name in sequence_names[exp]:
                if 'sequence_raw' in args.image_plot:
                    logging.info("--> Plotting signal images "+sequence_name)
                    signal_image_slices = load_sequence_signal_image_slices(sequence_name, image_dirname, projection_type=args.projection_type, aligned=False, verbose=args.verbose, debug=args.debug, loglevel=1)
                    if len(signal_image_slices)==0:
                        signal_image_slices = sequence_signal_image_slices(sequence_name, image_dirname, reference_name=membrane_name, signal_names=signal_names, microscope_orientation=microscope_orientation, projection_type=args.projection_type, resolution=None, aligned=False, save_files=True, verbose=args.verbose, debug=args.debug, loglevel=1)
                    figure = signal_image_plot(signal_image_slices, signal_names=signal_names, blend=False, projection_type=args.projection_type, resolution=0.25, aligned=False, verbose=args.verbose, debug=args.debug, loglevel=1)
                    figure.savefig(image_dirname+"/"+sequence_name+"/"+sequence_name+"_"+args.projection_type+"_signals.png")

                if 'sequence_raw' in args.cell_plot:
                    sequence_segmented_images = load_sequence_segmented_images(sequence_name, image_dirname, verbose=args.verbose, debug=args.debug, loglevel=1)
                    cell_data = load_sequence_signal_data(sequence_name, image_dirname, nuclei=False, normalized=False, aligned=False, verbose=args.verbose, debug=args.debug, loglevel=1)
                    figure = segmented_image_plot(sequence_segmented_images, cell_data, signal_names=['volume', 'mean_curvature']+signal_names, microscope_orientation=microscope_orientation, loglevel=1)
                    figure.savefig(image_dirname+"/"+sequence_name+"/"+sequence_name+"_segmentation_cells.png")

        if args.wall_extraction or args.all:
            for exp in experiments:
                membrane_name = get_experiment_membrane(exp, data_dirname)
                microscope_orientation = get_experiment_microscope_orientation(exp, data_dirname)
                for sequence_name in sequence_names[exp]:
                    logging.info("--> Sequence wall mesh extraction "+sequence_name)
                    extract_sequence_walls(sequence_name, save_files=True, image_dirname=image_dirname, membrane_name=membrane_name, microscope_orientation=microscope_orientation, resampling_voxelsize=0.5, target_edge_length=2., verbose=args.verbose, debug=args.debug, loglevel=1)

        if args.quantification or args.all:
            for exp in experiments:
                membrane_name = get_experiment_membrane(exp, data_dirname)
                channel_names = get_experiment_channels(exp, data_dirname)
                for sequence_name in sequence_names[exp]:
                    logging.info("--> Sequence wall signal quantification "+sequence_name)
                    compute_sequence_wall_signals(sequence_name, save_files=True, image_dirname=image_dirname, membrane_name=membrane_name, signal_names=channel_names, loglevel=1)

        for exp in experiments:
            membrane_name = get_experiment_membrane(exp, data_dirname)
            for sequence_name in sequence_names[exp]:
                if 'sequence_raw' in args.wall_plot:
                    wall_topomeshes = load_sequence_wall_meshes(sequence_name, image_dirname, loglevel=1)
                    signal_images = load_sequence_signal_images(sequence_name, image_dirname, signal_names=[membrane_name], verbose=args.verbose, debug=args.debug, loglevel=1)
                    wall_data = load_sequence_signal_wall_data(sequence_name, image_dirname, loglevel=1)
                    x_max = list(signal_images[membrane_name].values())[0].shape[0] * list(signal_images[membrane_name].values())[0].voxelsize[0] / 2.
                    y_max = list(signal_images[membrane_name].values())[0].shape[1] * list(signal_images[membrane_name].values())[0].voxelsize[1] / 2.
                    r_max = np.maximum(x_max, y_max)
                    logging.info("--> Plotting wall signals "+sequence_name)
                    signals_to_plot = list(wall_data.values())[0].columns
                    figure = signal_wall_plot(wall_topomeshes, reference_name=membrane_name, signal_names=signals_to_plot, r_max=r_max, linewidth=3, aligned=False, verbose=args.verbose, debug=args.debug, loglevel=1)
                    figure.savefig(image_dirname+"/"+sequence_name+"/"+sequence_name+"_walls.png")

        if args.data_compilation or args.all:
            logging.info("--> Compiling signal data from all experiments " + str(experiments))
            compile_cell_data(experiments, sam_ids=sam_ids, save_files=True, image_dirname=image_dirname, data_dirname=data_dirname, verbose=args.verbose, debug=args.debug, loglevel=1)


if __name__ == "__main__":
    main()