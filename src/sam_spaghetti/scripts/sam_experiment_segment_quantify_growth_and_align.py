import numpy as np
import pandas as pd

import sam_spaghetti
from sam_spaghetti.sam_microscopy_loading import load_image_from_microscopy
from sam_spaghetti.sam_sequence_info import get_experiment_name, get_experiment_microscopy, get_nomenclature_name, get_experiment_channels, get_experiment_reference, get_experiment_membrane, \
    get_sequence_orientation, get_experiment_microscope_orientation, update_lut_ranges, get_sequence_sam_centers
from sam_spaghetti.segmentation_quantification import segment
from sam_spaghetti.detection_quantification import detect_from_segmentation_and_quantify, quantify_from_segmentation
from sam_spaghetti.sam_sequence_loading import load_sequence_signal_images, load_sequence_segmented_images, load_sequence_signal_image_slices, load_sequence_signal_data
from sam_spaghetti.signal_image_slices import sequence_signal_image_slices
from sam_spaghetti.signal_image_plot import signal_image_plot, signal_nuclei_plot, signal_map_plot, element_topomesh_plot
from sam_spaghetti.signal_map_computation import compute_signal_maps, compute_average_signal_maps
from sam_spaghetti.sequence_image_registration import register_sequence_images, apply_sequence_registration
from sam_spaghetti.signal_data_compilation import compile_cell_data
from sam_spaghetti.sequence_growth_estimation import compute_growth_from_segmentation, segmentation_element_topomeshes
from sam_spaghetti.sam_sequence_curvature_alignment import align_sam_sequence_curvature_database

from tissue_nukem_3d.signal_map import save_signal_map

import logging
import argparse
import os

from timagetk.algorithms.reconstruction import pts2transfo

guillaume_dirname = "/Users/gcerutti/Data/"
calculus_dirname = "/projects/SamMaps/"
sam_spaghetti_dirname = sam_spaghetti.__path__[0]+"/../../share/data"

# dirname = guillaume_dirname
# dirname = calculus_dirname
dirname = sam_spaghetti_dirname

max_sam_id = 100
max_time = 100

plot_choices = ['sequence_raw', 'sequence_registered', 'sequence_aligned', 'sequence_primordia', 'experiment_aligned', 'experiment_primordia', 'all_aligned', 'all_primordia']

def main():
    """

    Returns:

    """

    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--experiments', help='List of experiment identifiers', nargs='+', required=True)
    parser.add_argument('-sam', '--sam-ids', default=range(max_sam_id), nargs='+', help='List of individual SAM identifiers to process (if found)', type=int)
    parser.add_argument('-dir', '--data-directory', help='Path to SAM sequence data files directory (nomenclature, orientation...)', default=dirname)
    parser.add_argument('-Mdir', '--microscopy-directory', help='Path to CZI image directory [default : data_directory/microscopy]', default=None)
    parser.add_argument('-Ddir', '--database-directory', help='Path to aligned SAM database directory [default : sam_atlas DB]', default=None)
    parser.add_argument('-o', '--output-directory', help='Path to detected nuclei directory [default : data_directory/nuclei_images]', default=None)

    parser.add_argument('-a', '--all', default=False, action='store_true', help='Run all the detection and alignment steps')

    parser.add_argument('-M', '--from-microscopy', default=False, action='store_true', help='Whether to load images from the microscopy image directory')
    parser.add_argument('-S', '--segmentation', default=False, action='store_true', help='Run membrane segmentation on all experiments')
    parser.add_argument('-D', '--detection', default=False, action='store_true', help='Run nuclei detection in segmented images on all experiments')
    parser.add_argument('-R', '--registration', default=False, action='store_true', help='Run sequence image registration on all experiments')
    parser.add_argument('-r', '--registration-type', default='full', help='Whether to estimate rigid or full (non-linear) image registration', choices=['rigid', 'full'])
    parser.add_argument('-i', '--image-plot', default=[], nargs='+', help='List of image projections types to plot',choices=plot_choices)
    parser.add_argument('-p', '--projection-type', default='max_intensity', help='Projection type for the image plots',choices=['max_intensity', 'L1_slice'])
    parser.add_argument('-n', '--nuclei-plot', default=[], nargs='+', help='List of signal map types to plot',choices=plot_choices)
    parser.add_argument('-c', '--cell-plot', default=[], nargs='+', help='List of signal map types to plot', choices=plot_choices)
    parser.add_argument('-m', '--map-plot', default=[], nargs='+', help='List of signal map types to plot',choices=plot_choices)
    parser.add_argument('-N', '--normalized', default=False, action='store_true', help='Display normalized signals when plotting')
    parser.add_argument('-pol', '--polar', default=False, action='store_true', help='Compute maps using polar coordinates')
    parser.add_argument('-G', '--growth-estimation', default=False, action='store_true', help='Estimate tissue-scale growth on all experiments')
    parser.add_argument('-g', '--growth-type', default='surfacic', help='Whether to estimate surfacic or volumetric growth information',choices=['surfacic','volumetric'])
    parser.add_argument('-C', '--data-compilation', default=False, action='store_true', help='Compile all the data from the experiments into .csv files in the data directory')
    parser.add_argument('-A', '--curvature-alignment', default=False, action='store_true', help='Align sequences of all experiments using curvature and a DB of aligned SAMs')

    parser.add_argument('-v', '--verbose', default=False, action='store_true', help='Verbose')
    parser.add_argument('-d', '--debug', default=False, action='store_true', help='Debug')

    args = parser.parse_args()

    logging.getLogger().setLevel(logging.INFO if args.verbose else logging.DEBUG if args.debug else logging.ERROR)

    data_dirname = args.data_directory

    microscopy_dirname = args.microscopy_directory if args.microscopy_directory is not None else data_dirname+"/microscopy"
    if not os.path.exists(microscopy_dirname):
        logging.warning(microscopy_dirname+" does not exist!")
        logging.warning("Microscopy directory not found! No detection will be performed.")

    experiments = args.experiments
    sam_ids = args.sam_ids
    image_dirname = args.output_directory if args.output_directory is not None else data_dirname+"/nuclei_images"

    update_lut_ranges(data_dirname)

    for exp in experiments:
        experiment_name = get_experiment_name(exp,data_dirname)
        if experiment_name == "":
            logging.error("Experiment identifier \""+exp+"\" not recognized (consider adding it to the experiment_info.csv file in the data directory)")
            experiments.remove(exp)
        else:
            if args.from_microscopy and (microscopy_dirname is not None):
                experiment_dirname = microscopy_dirname+"/"+get_experiment_microscopy(exp,data_dirname)
                if os.path.exists(experiment_dirname+"/RAW"):
                    experiment_dirname += "/RAW"

                if not os.path.exists(experiment_dirname):
                    logging.warning("Microscopy directory not found for "+exp+", no detection will be performed.")
                else:                    
                    microscopy_filenames = [experiment_dirname+"/"+f for f in os.listdir(experiment_dirname) if np.any([ext in f for ext in ['.czi','.lsm','.tif']])]
                    nomenclature_names = [get_nomenclature_name(microscopy_filename, data_dirname, experiment=exp) for microscopy_filename in microscopy_filenames]
                    microscopy_filenames = [m for m,n in zip(microscopy_filenames,nomenclature_names) if n is not None]

                    channel_names = get_experiment_channels(exp, data_dirname)

                    if not os.path.exists(image_dirname):
                        os.makedirs(image_dirname)

                    for microscopy_filename in microscopy_filenames:
                        nomenclature_name = get_nomenclature_name(microscopy_filename, data_dirname, experiment=exp)

                        if nomenclature_name is not None:
                            sequence_name = nomenclature_name[:-4]

                            sam_id = int(sequence_name[-2:])

                            if sam_id in sam_ids:
                                if not os.path.exists(image_dirname+"/"+sequence_name):
                                    os.makedirs(image_dirname+"/"+sequence_name)
                                if not os.path.exists(image_dirname+"/"+sequence_name+"/"+nomenclature_name):
                                    os.makedirs(image_dirname+"/"+sequence_name+"/"+nomenclature_name)

                                load_image_from_microscopy(microscopy_filename, save_images=True, image_dirname=image_dirname, nomenclature_name=nomenclature_name, channel_names=channel_names, verbose=args.verbose, debug=args.debug, loglevel=1)
                        else:
                            logging.warning("--> No nomenclature found for " + microscopy_filename + ", skipping...")

    if not os.path.exists(image_dirname):
        logging.error("Result output directory not found, nothing left to do!")
    else:
        sequence_names = {}
        for exp in experiments:
            experiment_name = get_experiment_name(exp,data_dirname)
            reference_name = get_experiment_reference(exp, data_dirname)
            membrane_name = get_experiment_membrane(exp, data_dirname)
            logging.info("--> Loading sequences for experiment "+str(exp))

            sequence_names[exp] = []
            for sam_id in sam_ids:
                sequence_name = experiment_name+"_sam"+str(sam_id).zfill(2)
                logging.debug("--> Trying to load sequence "+str(sequence_name))

                signal_names = [membrane_name]
                if reference_name is not None and reference_name != membrane_name:
                    signal_names += [reference_name]
                signal_images = load_sequence_signal_images(sequence_name, image_dirname,signal_names=signal_names,verbose=args.verbose, debug=args.debug, loglevel=1)
                if len(signal_images) > 0:
                    sequence_names[exp] += [sequence_name]
                    logging.debug("--> Loaded sequence "+str(sequence_name)+"!")

        if args.segmentation or args.all:
            for exp in experiments:
                membrane_name = get_experiment_membrane(exp, data_dirname)

                for sequence_name in sequence_names[exp]:
                    logging.info("--> Sequence image segmentation " + sequence_name)
                    membrane_images = load_sequence_signal_images(sequence_name, image_dirname, signal_names=[membrane_name], verbose=args.verbose, debug=args.debug, loglevel=1)

                    for filename in membrane_images[membrane_name].keys():
                        img_dict = {membrane_name:membrane_images[membrane_name][filename]}
                        h_min = 2 if membrane_images[membrane_name][filename].dtype == np.uint8 else 100
                        logging.info("--> Running segmentation on "+filename+" "+membrane_name)
                        segment(img_dict, membrane_name, save_files=True, image_dirname=image_dirname, nomenclature_name=filename,
                                h_min=h_min, volume_threshold=1e5, verbose=args.verbose, debug=args.debug, loglevel=1)

        if args.detection or args.all:
            for exp in experiments:
                reference_name = get_experiment_reference(exp, data_dirname)
                membrane_name = get_experiment_membrane(exp, data_dirname)
                signal_names = get_experiment_channels(exp, data_dirname)
                microscope_orientation = get_experiment_microscope_orientation(exp, data_dirname)

                for sequence_name in sequence_names[exp]:
                    logging.info("--> Sequence nuclei detection " + sequence_name)
                    segmented_images = load_sequence_segmented_images(sequence_name, image_dirname, membrane_name=membrane_name, verbose=args.verbose, debug=args.debug, loglevel=1)

                    if reference_name is not None:
                        signal_images = load_sequence_signal_images(sequence_name, image_dirname, signal_names=signal_names, verbose=args.verbose, debug=args.debug, loglevel=1)

                        for filename in signal_images[reference_name].keys():
                            logging.info("--> Running detection on " + filename + " " + reference_name)
                            img_dict = {s:signal_images[s][filename] for s in signal_names}
                            seg_img = segmented_images[filename]
                            detect_from_segmentation_and_quantify(img_dict, seg_img, reference_name=reference_name, signal_names=signal_names, save_files=True, image_dirname=image_dirname, nomenclature_name=filename, microscope_orientation=microscope_orientation, verbose=args.verbose, debug=args.debug, loglevel=1)
                    else:
                        for filename in segmented_images.keys():
                            logging.info("--> Running quantification on " + filename)
                            seg_img = segmented_images[filename]
                            quantify_from_segmentation(seg_img,save_files=True, image_dirname=image_dirname, nomenclature_name=filename, microscope_orientation=microscope_orientation, verbose=args.verbose, debug=args.debug, loglevel=1)

        for exp in experiments:
            reference_name = get_experiment_reference(exp, data_dirname)
            microscope_orientation = get_experiment_microscope_orientation(exp, data_dirname)
            for sequence_name in sequence_names[exp]:
                if 'sequence_raw' in args.nuclei_plot:
                    signal_data = load_sequence_signal_data(sequence_name, image_dirname, nuclei=False, normalized=False, aligned=False, verbose=args.verbose, debug=args.debug, loglevel=1)
                    signal_images = load_sequence_signal_images(sequence_name, image_dirname, signal_names=[reference_name], verbose=args.verbose, debug=args.debug, loglevel=1)
                    r_max = list(signal_images[reference_name].values())[0].shape[0]*list(signal_images[reference_name].values())[0].voxelsize[0]/2.
                    logging.info("--> Plotting detected nuclei signals "+sequence_name)
                    figure = signal_nuclei_plot(signal_data, r_max=r_max, normalized=args.normalized, verbose=args.verbose, debug=args.debug, loglevel=1)
                    figure.savefig(image_dirname+"/"+sequence_name+"/"+sequence_name+"_L1_nuclei_signals.png")

                if 'sequence_raw' in args.image_plot:
                    logging.info("--> Plotting signal images "+sequence_name)
                    signal_image_slices = load_sequence_signal_image_slices(sequence_name, image_dirname, projection_type=args.projection_type, aligned=False, verbose=args.verbose, debug=args.debug, loglevel=1)
                    if len(signal_image_slices)==0:
                        signal_image_slices = sequence_signal_image_slices(sequence_name, image_dirname, reference_name=reference_name, microscope_orientation=microscope_orientation, projection_type=args.projection_type, resolution=None, aligned=False, save_files=True, verbose=args.verbose, debug=args.debug, loglevel=1)
                    figure = signal_image_plot(signal_image_slices, reference_name=reference_name, projection_type=args.projection_type, resolution=0.25, aligned=False, verbose=args.verbose, debug=args.debug, loglevel=1)
                    figure.savefig(image_dirname+"/"+sequence_name+"/"+sequence_name+"_"+args.projection_type+"_signals.png")

                if 'sequence_raw' in args.cell_plot:
                    sequence_element_topomesh = segmentation_element_topomeshes(sequence_name, image_dirname, surface=True, save_files=True,
                                                                                microscope_orientation=microscope_orientation,
                                                                                verbose=args.verbose, debug=args.debug, loglevel=1)
                    figure = element_topomesh_plot(sequence_element_topomesh, loglevel=1)
                    figure.savefig(image_dirname+"/"+sequence_name+"/"+sequence_name+"_L1_cells.png")


        if args.data_compilation or args.all:
            logging.info("--> Compiling signal data from all experiments " + str(experiments))
            compile_cell_data(experiments, sam_ids=sam_ids, save_files=True, image_dirname=image_dirname, data_dirname=data_dirname, verbose=args.verbose, debug=args.debug, loglevel=1)

        for exp in experiments:
            membrane_name = get_experiment_membrane(exp, data_dirname)
            reference_name = get_experiment_reference(exp, data_dirname)
            signal_names = get_experiment_channels(exp, data_dirname)
            microscope_orientation = get_experiment_microscope_orientation(exp, data_dirname)
            for sequence_name in sequence_names[exp]:
                if args.registration or args.all:
                    logging.info("--> Sequence image registration " + sequence_name)
                    register_sequence_images(sequence_name, microscope_orientation=microscope_orientation,
                                             compute_vectorfield=args.registration_type=="full", save_files=True,
                                             image_dirname=image_dirname, reference_name=membrane_name,
                                             verbose=args.verbose, debug=args.debug, loglevel=1)
                    apply_sequence_registration(sequence_name, signal_names=signal_names,
                                                reference_name=membrane_name, save_files=True,
                                                microscope_orientation=microscope_orientation,
                                                image_dirname=image_dirname,
                                                verbose=args.verbose, debug=args.debug, loglevel=1)

                if 'sequence_registered' in args.image_plot:
                    signal_image_slices = sequence_signal_image_slices(sequence_name, image_dirname, reference_name=reference_name, microscope_orientation=microscope_orientation,
                                                                       projection_type=args.projection_type, resolution=None, registered=True, aligned=False, save_files=False, verbose=args.verbose,
                                                                       debug=args.debug, loglevel=1)
                    figure = signal_image_plot(signal_image_slices, reference_name=reference_name, projection_type=args.projection_type, resolution=None, aligned=False, verbose=args.verbose,
                                               debug=args.debug, loglevel=1)
                    figure.savefig(image_dirname + "/" + sequence_name + "/" + sequence_name + "_" + args.projection_type + "_registered_signals.png")

        if args.data_compilation or args.all:
            logging.info("--> Compiling signal data from all experiments " + str(experiments))
            compile_cell_data(experiments, sam_ids=sam_ids, save_files=True, image_dirname=image_dirname, data_dirname=data_dirname, verbose=args.verbose, debug=args.debug, loglevel=1)

        for exp in experiments:
            microscope_orientation = get_experiment_microscope_orientation(exp, data_dirname)
            for sequence_name in sequence_names[exp]:
                if args.growth_estimation or args.all:
                    logging.info("--> Computing sequence " + args.growth_type + " growth " + sequence_name)
                    compute_growth_from_segmentation(sequence_name, image_dirname, save_files=True,
                                                     growth_type=args.growth_type, microscope_orientation=microscope_orientation,
                                                     verbose=args.verbose, debug=args.debug, loglevel=1)

        for exp in experiments:
            for sequence_name in sequence_names[exp]:
                if 'sequence_raw' in args.map_plot:
                    signal_normalized_data = load_sequence_signal_data(sequence_name, image_dirname, nuclei=False, verbose=args.verbose, debug=args.debug, loglevel=1)
                    signal_maps = compute_signal_maps(signal_normalized_data, normalized=args.normalized, polar=args.polar, verbose=args.verbose, debug=args.debug, loglevel=1)
                    logging.info("--> Plotting maps "+sequence_name)
                    figure = signal_map_plot(signal_maps, verbose=args.verbose, debug=args.debug, loglevel=1)
                    figure.savefig(image_dirname+"/"+sequence_name+"/"+sequence_name+"_L1_signal_maps.png")

        for exp in experiments:
            for sequence_name in sequence_names[exp]:
                if args.curvature_alignment or args.all:
                    logging.info("--> Sequence curvature alignment "+sequence_name)
                    microscope_orientation = get_experiment_microscope_orientation(exp, data_dirname)

                    sequence_sam_centers = get_sequence_sam_centers(sequence_name, data_dirname)
                    # if sequence_sam_centers is not None:
                    #     membrane_name = get_experiment_membrane(exp, data_dirname)
                    #     sequence_images = load_sequence_signal_images(sequence_name, image_dirname, [membrane_name])
                    #     for filename in sequence_sam_centers.keys():
                    #         sequence_sam_centers[filename] *= np.array(sequence_images[membrane_name][filename].voxelsize)[:2]

                    align_sam_sequence_curvature_database(sequence_name, image_dirname, sequence_sam_centers=sequence_sam_centers,
                                                          database_dirname=args.database_directory, curvature_type='mean_curvature',
                                                          microscope_orientation=microscope_orientation, save_files=True,
                                                          verbose=args.verbose, debug=args.debug, loglevel=1)

        if args.data_compilation or args.all:
            logging.info("--> Compiling signal data from all experiments " + str(experiments))
            compile_cell_data(experiments, sam_ids=sam_ids, save_files=True, image_dirname=image_dirname, data_dirname=data_dirname, verbose=args.verbose, debug=args.debug, loglevel=1)

        sequence_aligned_signal_data = {}
        sequence_aligned_signal_maps = {}
        for exp in experiments:
            sequence_aligned_signal_data[exp] = {}
            sequence_aligned_signal_maps[exp] = {}
            for sequence_name in sequence_names[exp]:
                if np.any([p in args.nuclei_plot for p in ['sequence_aligned', 'experiment_aligned', 'all_aligned']]):
                    signal_data = load_sequence_signal_data(sequence_name, image_dirname, nuclei=False, verbose=args.verbose, debug=args.debug, loglevel=1)
                    sequence_aligned_signal_data[exp][sequence_name] = signal_data

                if 'sequence_aligned' in args.nuclei_plot:
                    logging.info("--> Plotting aligned nuclei signals " + sequence_name)
                    figure = signal_nuclei_plot(sequence_aligned_signal_data[exp][sequence_name], aligned=True, normalized=args.normalized, verbose=args.verbose, debug=args.debug, loglevel=1)
                    figure.savefig(image_dirname + "/" + sequence_name + "/" + sequence_name + "_L1_aligned_nuclei_signals.png")

                if np.any([p in args.map_plot for p in ['sequence_aligned','experiment_aligned','all_aligned']]):
                    signal_aligned_data = load_sequence_signal_data(sequence_name, image_dirname, nuclei=False, verbose=args.verbose, debug=args.debug, loglevel=1)
                    signal_aligned_maps = compute_signal_maps(signal_aligned_data, aligned=True, normalized=args.normalized, polar=args.polar, verbose=args.verbose, debug=args.debug, loglevel=1)
                    for filename, aligned_map in signal_aligned_maps.items():
                        save_signal_map(aligned_map, image_dirname + "/" + sequence_name + "/" + filename + "/" + filename + "_aligned_signal_map.map")
                    sequence_aligned_signal_maps[exp][sequence_name] = signal_aligned_maps

                if 'sequence_aligned' in args.map_plot:
                    logging.info("--> Plotting signal maps "+sequence_name)
                    figure = signal_map_plot(sequence_aligned_signal_maps[exp][sequence_name], aligned=True, verbose=args.verbose, debug=args.debug, loglevel=1)
                    figure.savefig(image_dirname+"/"+sequence_name+"/"+sequence_name+"_L1_aligned_signal_maps.png")

            if 'experiment_aligned' in args.nuclei_plot:
                experiment_data = list(sequence_aligned_signal_data[exp].values())
                experiment_times = np.sort(np.unique(np.concatenate([[int(f[-2:]) for f in d.keys()] for d in  experiment_data])))
                experiment_aligned_signal_data = dict([("t"+str(time).zfill(2),pd.concat([d[f] for d in experiment_data for f in d.keys() if int(f[-2:])==time])) for time in experiment_times])
                figure = signal_nuclei_plot(experiment_aligned_signal_data, aligned=True, normalized=args.normalized, alpha=1./len(sequence_aligned_signal_data[exp]), verbose=args.verbose, debug=args.debug, loglevel=1)
                figure.savefig(image_dirname + "/" + exp + "_L1_aligned_nuclei_signals.png")

            if 'experiment_aligned' in args.map_plot:
                logging.info("--> Plotting average signal maps "+exp)
                experiment_signal_maps = dict([(f,m) for s in sequence_aligned_signal_maps[exp].keys() for f,m in sequence_aligned_signal_maps[exp][s].items()])
                time_average_maps = compute_average_signal_maps(experiment_signal_maps, verbose=args.verbose, debug=args.debug, loglevel=1)
                for t, average_map in time_average_maps.items():
                    save_signal_map(average_map, image_dirname + "/" + exp + "_average_signal_map_" + t + ".map")
                figure = signal_map_plot(time_average_maps, aligned=True, verbose=args.verbose, debug=args.debug, loglevel=1)
                figure.savefig(image_dirname + "/" + exp + "_average_L1_aligned_signal_maps.png")


if __name__ == "__main__":
    main()