import numpy as np
import pandas as pd

import sam_spaghetti
from sam_spaghetti.sam_microscopy_loading import load_image_from_microscopy
from sam_spaghetti.sam_sequence_info import get_experiment_name, get_experiment_microscopy, get_nomenclature_name, get_experiment_channels, get_experiment_reference, get_sequence_orientation, get_experiment_microscope_orientation, get_sequence_info, update_lut_ranges
from sam_spaghetti.detection_quantification import detect_and_quantify, extract_meristem_cells
from sam_spaghetti.sam_sequence_loading import load_sequence_signal_images, load_sequence_signal_image_slices, load_sequence_signal_data
from sam_spaghetti.signal_image_slices import sequence_signal_image_slices
from sam_spaghetti.signal_image_plot import signal_image_plot, signal_nuclei_plot, signal_map_plot
from sam_spaghetti.signal_map_computation import compute_signal_maps
from sam_spaghetti.signal_data_compilation import compile_signal_data

import logging
import argparse
import os

from timagetk.algorithms.reconstruction import pts2transfo

guillaume_dirname = "/Users/gcerutti/Data/"
calculus_dirname = "/projects/SamMaps/"
sam_spaghetti_dirname = sam_spaghetti.__path__[0]+"/../../share/data"

# dirname = guillaume_dirname
# dirname = calculus_dirname
dirname = sam_spaghetti_dirname

max_sam_id = 100
max_time = 100

plot_choices = []
plot_choices += ['sequence_raw', 'sequence_registered', 'sequence_aligned', 'sequence_primordia']
plot_choices += ['experiment_aligned', 'experiment_primordia']
plot_choices += ['all_aligned', 'all_primordia']

def main():
    """

    Returns:

    """

    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--experiments', help='List of experiment identifiers', nargs='+', required=True)
    parser.add_argument('-sam', '--sam-ids', default=range(max_sam_id), nargs='+', help='List of individual SAM identifiers to process (if found)', type=int)
    parser.add_argument('-dir', '--data-directory', help='Path to SAM sequence data files directory (nomenclature, orientation...)', default=dirname)
    parser.add_argument('-Mdir', '--microscopy-directory', help='Path to CZI image directory [default : data_directory/microscopy]', default=None)
    parser.add_argument('-o', '--output-directory', help='Path to detected nuclei directory [default : data_directory/nuclei_images]', default=None)

    parser.add_argument('-a', '--all', default=False, action='store_true', help='Run all the detection and alignment steps')

    parser.add_argument('-M', '--from-microscopy', default=False, action='store_true', help='Whether to load images from the microscopy image directory')
    parser.add_argument('-D', '--detection', default=False, action='store_true', help='Run nuclei detection on all experiments')
    parser.add_argument('-E', '--meristem-extraction', default=False, action='store_true', help='Run meristem nuclei delineation on all experiments')
    parser.add_argument('-C', '--data-compilation', default=False, action='store_true', help='Compile all the data from the experiments into .csv files in the data directory')
    parser.add_argument('-N', '--normalized', default=False, action='store_true', help='Display normalized signals when plotting')

    parser.add_argument('-i', '--image-plot', default=[], nargs='+', help='List of image projections types to plot',choices=plot_choices)
    parser.add_argument('-p', '--projection-type', default='max_intensity', help='Projection type for the image plots',choices=['max_intensity', 'L1_slice'])
    parser.add_argument('-n', '--nuclei-plot', default=[], nargs='+', help='List of signal map types to plot',choices=plot_choices)
    parser.add_argument('-m', '--map-plot', default=[], nargs='+', help='List of signal map types to plot',choices=plot_choices)

    parser.add_argument('-v', '--verbose', default=False, action='store_true', help='Verbose')
    parser.add_argument('-d', '--debug', default=False, action='store_true', help='Debug')

    args = parser.parse_args()

    logging.getLogger().setLevel(logging.INFO if args.verbose else logging.DEBUG if args.debug else logging.ERROR)

    data_dirname = args.data_directory

    microscopy_dirname = args.microscopy_directory if args.microscopy_directory is not None else data_dirname+"/microscopy"
    if not os.path.exists(microscopy_dirname):
        logging.warning(microscopy_dirname+" does not exist!")
        logging.warning("Microscopy directory not found! No detection will be performed.")

    experiments = args.experiments
    sam_ids = args.sam_ids
    image_dirname = args.output_directory if args.output_directory is not None else data_dirname+"/nuclei_images"

    update_lut_ranges(data_dirname)

    for exp in experiments:
        experiment_name = get_experiment_name(exp,data_dirname)
        if experiment_name == "":
            logging.error("Experiment identifier \""+exp+"\" not recognized (consider adding it to the experiment_info.csv file in the data directory)")
            experiments.remove(exp)
        else:
            if args.from_microscopy and (microscopy_dirname is not None):
                experiment_dirname = microscopy_dirname+"/"+get_experiment_microscopy(exp,data_dirname)
                if os.path.exists(experiment_dirname+"/RAW"):
                    experiment_dirname += "/RAW"

                if not os.path.exists(experiment_dirname):
                    logging.warning("Microscopy directory not found for "+exp+", no detection will be performed.")
                else:
                    microscopy_filenames = [experiment_dirname+"/"+f for f in os.listdir(experiment_dirname) if np.any([ext in f for ext in ['.czi','.lsm','.tif']])]
                    nomenclature_names = [get_nomenclature_name(microscopy_filename,data_dirname,experiment=exp) for microscopy_filename in microscopy_filenames]
                    microscopy_filenames = [m for m,n in zip(microscopy_filenames,nomenclature_names) if n is not None]

                    channel_names = get_experiment_channels(exp, data_dirname)

                    if not os.path.exists(image_dirname):
                        os.makedirs(image_dirname)

                    for microscopy_filename in microscopy_filenames:
                        nomenclature_name = get_nomenclature_name(microscopy_filename,data_dirname,experiment=exp)

                        if nomenclature_name is not None:
                            sequence_name = nomenclature_name[:-4]

                            sam_id = int(sequence_name[-2:])

                            if sam_id in sam_ids:
                                if not os.path.exists(image_dirname+"/"+sequence_name):
                                    os.makedirs(image_dirname+"/"+sequence_name)
                                if not os.path.exists(image_dirname+"/"+sequence_name+"/"+nomenclature_name):
                                    os.makedirs(image_dirname+"/"+sequence_name+"/"+nomenclature_name)

                                load_image_from_microscopy(microscopy_filename, save_images=True, image_dirname=image_dirname, nomenclature_name=nomenclature_name, channel_names=channel_names, verbose=args.verbose, debug=args.debug, loglevel=1)
                        else:
                            logging.warning("--> No nomenclature found for " + microscopy_filename + ", skipping...")

    if not os.path.exists(image_dirname):
        logging.error("Result output directory not found, nothing left to do!")
    else:
        sequence_names = {}
        for exp in experiments:
            experiment_name = get_experiment_name(exp,data_dirname)
            reference_name = get_experiment_reference(exp, data_dirname)
            logging.info("--> Loading sequences for experiment "+str(exp))

            sequence_names[exp] = []
            for sam_id in sam_ids:
                sequence_name = experiment_name+"_sam"+str(sam_id).zfill(2)
                logging.debug("--> Trying to load sequence "+str(sequence_name))

                signal_names = [reference_name]
                signal_images = load_sequence_signal_images(sequence_name, image_dirname,signal_names=signal_names,verbose=args.verbose, debug=args.debug, loglevel=1)
                if len(signal_images) > 0:
                    sequence_names[exp] += [sequence_name]
                    logging.debug("--> Loaded sequence "+str(sequence_name)+"!")

        for exp in experiments:
            reference_name = get_experiment_reference(exp, data_dirname)
            channel_names = get_experiment_channels(exp, data_dirname)
            microscope_orientation = get_experiment_microscope_orientation(exp, data_dirname)
            for sequence_name in sequence_names[exp]:
                if 'sequence_raw' in args.image_plot and args.projection_type=='max_intensity':
                    logging.info("--> Plotting signal images "+sequence_name)
                    # signal_images = load_sequence_signal_images(sequence_name, image_dirname, verbose=args.verbose, debug=args.debug, loglevel=1)
                    # signal_data = load_sequence_signal_data(sequence_name, image_dirname, normalized=False, aligned=False, verbose=args.verbose, debug=args.debug, loglevel=1)
                    signal_image_slices = load_sequence_signal_image_slices(sequence_name, image_dirname, signal_names=channel_names, projection_type=args.projection_type, aligned=False, verbose=args.verbose, debug=args.debug, loglevel=1)
                    if len(signal_image_slices)==0:
                        signal_image_slices = sequence_signal_image_slices(sequence_name, image_dirname, reference_name=reference_name, signal_names=channel_names, microscope_orientation=microscope_orientation, projection_type=args.projection_type, resolution=None, aligned=False, save_files=True, verbose=args.verbose, debug=args.debug, loglevel=1)
                    figure = signal_image_plot(signal_image_slices, reference_name=reference_name, projection_type=args.projection_type, resolution=0.25, aligned=False, verbose=args.verbose, debug=args.debug, loglevel=1)
                    figure.savefig(image_dirname+"/"+sequence_name+"/"+sequence_name+"_"+args.projection_type+"_signals.png")


        if args.detection or args.all:
            for exp in experiments:
                channel_names = get_experiment_channels(exp, data_dirname)
                reference_name = get_experiment_reference(exp, data_dirname)

                for sequence_name in sequence_names[exp]:
                    logging.info("--> Sequence nuclei detection " + sequence_name)
                    sequence_signal_images = load_sequence_signal_images(sequence_name, image_dirname, signal_names=channel_names, verbose=args.verbose, debug=args.debug, loglevel=1)
                    for filename in sequence_signal_images[reference_name].keys():
                        img_dict = {c: sequence_signal_images[c][filename] for c in channel_names}
                        detect_and_quantify(img_dict, reference_name=reference_name, signal_names=channel_names, image_dirname=image_dirname, nomenclature_name=filename, verbose=args.verbose, debug=args.debug, loglevel=1)

        if args.meristem_extraction or args.all:
            for exp in experiments:
                for sequence_name in sequence_names[exp]:
                    logging.info("--> Sequence meristem extraction " + sequence_name)
                    extract_meristem_cells(sequence_name, image_dirname, nuclei=True, min_curvature_threshold=-5e-3, save_files=True, verbose=args.verbose, debug=args.debug, loglevel=1)

        for exp in experiments:
            reference_name = get_experiment_reference(exp, data_dirname)
            microscope_orientation = get_experiment_microscope_orientation(exp, data_dirname)
            for sequence_name in sequence_names[exp]:
                if 'sequence_raw' in args.nuclei_plot:
                    signal_data = load_sequence_signal_data(sequence_name, image_dirname, normalized=False, aligned=False, verbose=args.verbose, debug=args.debug, loglevel=1)
                    signal_images = load_sequence_signal_images(sequence_name, image_dirname, signal_names=[reference_name], verbose=args.verbose, debug=args.debug, loglevel=1)
                    r_max = list(signal_images[reference_name].values())[0].shape[0]*list(signal_images[reference_name].values())[0].voxelsize[0]/2.
                    logging.info("--> Plotting detected nuclei signals "+sequence_name)
                    figure = signal_nuclei_plot(signal_data, r_max=r_max, normalized=args.normalized, verbose=args.verbose, debug=args.debug, loglevel=1)
                    figure.savefig(image_dirname+"/"+sequence_name+"/"+sequence_name+"_L1_nuclei_signals.png")

                if 'sequence_raw' in args.image_plot and args.projection_type=='L1_slice':
                    logging.info("--> Plotting signal images "+sequence_name)
                    # signal_images = load_sequence_signal_images(sequence_name, image_dirname, verbose=args.verbose, debug=args.debug, loglevel=1)
                    # signal_data = load_sequence_signal_data(sequence_name, image_dirname, normalized=False, aligned=False, verbose=args.verbose, debug=args.debug, loglevel=1)
                    signal_image_slices = load_sequence_signal_image_slices(sequence_name, image_dirname, projection_type=args.projection_type, aligned=False, verbose=args.verbose, debug=args.debug, loglevel=1)
                    if len(signal_image_slices)==0:
                        signal_image_slices = sequence_signal_image_slices(sequence_name, image_dirname, reference_name=reference_name, microscope_orientation=microscope_orientation, projection_type=args.projection_type, resolution=None, aligned=False, save_files=True, verbose=args.verbose, debug=args.debug, loglevel=1)
                    figure = signal_image_plot(signal_image_slices, reference_name=reference_name, projection_type=args.projection_type, resolution=0.25, aligned=False, verbose=args.verbose, debug=args.debug, loglevel=1)
                    figure.savefig(image_dirname+"/"+sequence_name+"/"+sequence_name+"_"+args.projection_type+"_signals.png")

                if 'sequence_raw' in args.map_plot:
                    signal_normalized_data = load_sequence_signal_data(sequence_name, image_dirname, normalized=True, aligned=False, verbose=args.verbose, debug=args.debug, loglevel=1)
                    signal_maps = compute_signal_maps(signal_normalized_data, normalized=args.normalized, verbose=args.verbose, debug=args.debug, loglevel=1)
                    logging.info("--> Plotting maps "+sequence_name)
                    figure = signal_map_plot(signal_maps, verbose=args.verbose, debug=args.debug, loglevel=1)
                    figure.savefig(image_dirname+"/"+sequence_name+"/"+sequence_name+"_L1_signal_maps.png")

        if args.data_compilation or args.all:
            logging.info("--> Compiling signal data from all experiments " + str(experiments))
            compile_signal_data(experiments, save_files=True, image_dirname=image_dirname, data_dirname=data_dirname, verbose=args.verbose, debug=args.debug, loglevel=1)


if __name__ == "__main__":
    main()