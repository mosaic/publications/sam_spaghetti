import logging
import argparse
import os

from copy import deepcopy

import numpy as np
import scipy.ndimage as nd
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import sam_spaghetti
from sam_spaghetti.sam_microscopy_loading import load_image_from_microscopy
from sam_spaghetti.sam_sequence_info import get_experiment_name, get_experiment_microscopy, get_nomenclature_name, get_experiment_channels, get_experiment_reference, get_experiment_membrane, get_sequence_orientation, get_experiment_microscope_orientation, update_lut_ranges
from sam_spaghetti.segmentation_quantification import segment
from sam_spaghetti.detection_quantification import detect_from_segmentation_and_quantify
from sam_spaghetti.sam_sequence_loading import load_sequence_signal_images, load_sequence_segmented_images, load_sequence_signal_image_slices, load_sequence_signal_data, load_sequence_cell_meshes
from sam_spaghetti.signal_image_slices import sequence_signal_image_slices, sequence_image_primordium_slices, sequence_signal_data_primordium_slices
from sam_spaghetti.signal_map_computation import compute_signal_maps, compute_primordia_signal_maps, compute_average_signal_maps, compute_average_primordia_signal_maps
from sam_spaghetti.sequence_image_registration import register_sequence_images, apply_sequence_registration
from sam_spaghetti.signal_data_compilation import compile_cell_data

from sam_spaghetti.sequence_growth_estimation import topomesh_cell_surfacic_growth_data, segmentation_element_topomeshes
from sam_spaghetti.sam_sequence_curvature_alignment import align_sam_sequence_curvature_database

from sam_spaghetti.utils.signal_luts import signal_colormaps, quantified_signals
from sam_spaghetti.utils.signal_luts import signal_ranges, signal_lut_ranges, contour_ranges
from sam_spaghetti.utils.pandas_tools import array_from_printed_string

from visu_core.matplotlib import glasbey

from tissue_nukem_3d.signal_map import save_signal_map
from timagetk.components import LabelledImage
from timagetk.io import imsave
from timagetk.algorithms.reconstruction import pts2transfo

from cellcomplex.utils.array_dict import array_dict
from cellcomplex.property_topomesh.extraction import clean_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_cell_property_from_faces
from cellcomplex.property_topomesh.transformation import topomesh_transformation
from cellcomplex.property_topomesh.utils.pandas_tools import topomesh_to_dataframe
from cellcomplex.property_topomesh.visualization.matplotlib import mpl_draw_topomesh

from draco_stem.grifone.grifone import segmentation_topological_element_cell_topomesh

guillaume_dirname = "/Users/gcerutti/Data/"
calculus_dirname = "/projects/SamMaps/"
sam_spaghetti_dirname = sam_spaghetti.__path__[0]+"/../../share/data"

# dirname = guillaume_dirname
# dirname = calculus_dirname
dirname = sam_spaghetti_dirname

max_sam_id = 100
max_time = 100

def main():
    """

    Returns:

    """

    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--experiments', help='List of experiment identifiers', nargs='+', required=True)
    parser.add_argument('-sam', '--sam-ids', default=range(max_sam_id), nargs='+', help='List of individual SAM identifiers to process (if found)', type=int)
    parser.add_argument('-dir', '--data-directory', help='Path to SAM sequence data files directory (nomenclature, orientation...)', default=dirname)
    parser.add_argument('-o', '--output-directory', help='Path to detected nuclei directory [default : data_directory/nuclei_images]', default=None)
    parser.add_argument('-G', '--growth-estimation', default=False, action='store_true', help='Estimate cell-scale growth from manual lineage on all experiments')
    parser.add_argument('-A', '--growth-alignment', default=False, action='store_true', help='Compute aligned growth features using aligned SAM data')
    parser.add_argument('-c', '--cell-plot', default=False, action='store_true', help='Whether to plot all available signals on cell templates')

    parser.add_argument('-v', '--verbose', default=False, action='store_true', help='Verbose')
    parser.add_argument('-d', '--debug', default=False, action='store_true', help='Debug')

    args = parser.parse_args()

    logging.getLogger().setLevel(logging.INFO if args.verbose else logging.DEBUG if args.debug else logging.ERROR)

    data_dirname = args.data_directory

    from sam_spaghetti.utils.signal_luts import quantified_signals

    signal_ranges['min_curvature'] = (-0.2, 0.1)
    signal_lut_ranges['min_curvature'] = (-0.1, 0.05)
    signal_colormaps['min_curvature'] = 'PuBu_r'
    quantified_signals += ['min_curvature']

    signal_ranges['max_curvature'] = (-0.1, 0.2)
    signal_lut_ranges['max_curvature'] = (-0.05, 0.1)
    signal_colormaps['max_curvature'] = 'PuRd'
    quantified_signals += ['max_curvature']

    signal_lut_ranges['gaussian_curvature'] = (-0.002, 0.001)
    signal_lut_ranges['GA Input'] = (2, 3)

    signal_ranges['manual_next_surfacic_growth'] = (0.8, 2)
    signal_lut_ranges['manual_next_surfacic_growth'] = (0.9, 1.5)
    signal_colormaps['manual_next_surfacic_growth'] = 'jet'
    quantified_signals += ['manual_next_surfacic_growth']

    signal_ranges['manual_next_image_volume_ratio'] = (0.8, 2)
    signal_lut_ranges['manual_next_image_volume_ratio'] = (0.9, 1.5)
    signal_colormaps['manual_next_image_volume_ratio'] = 'jet'
    quantified_signals += ['manual_next_image_volume_ratio']

    signal_ranges['manual_next_surfacic_growth_anisotropy'] = (0., 1.)
    signal_lut_ranges['manual_next_surfacic_growth_anisotropy'] = (0., 0.25)
    signal_colormaps['manual_next_surfacic_growth_anisotropy'] = 'YlOrRd'
    quantified_signals += ['manual_next_surfacic_growth_anisotropy']

    signal_colormaps['volume'] = 'plasma'
    signal_ranges['volume'] = (0, 1000)
    signal_lut_ranges['volume'] = (0, 400)

    signal_colormaps['manual_next_radial_growth'] = 'jet'
    signal_ranges['manual_next_radial_growth'] = (0.8, 2)
    signal_lut_ranges['manual_next_radial_growth'] = (0.9, 1.5)

    signal_colormaps['manual_next_tangential_growth'] = 'jet'
    signal_ranges['manual_next_tangential_growth'] = (0.8, 2)
    signal_lut_ranges['manual_next_tangential_growth'] = (0.9, 1.5)

    signal_colormaps['manual_next_main_surfacic_growth_radial_angle'] = 'BuYlRd_r'
    signal_ranges['manual_next_main_surfacic_growth_radial_angle'] = (0., 90.)
    signal_lut_ranges['manual_next_main_surfacic_growth_radial_angle'] = (0., 90.)

    signal_colormaps['manual_next_surfacic_growth_radialness'] = 'BuYlRd'
    signal_ranges['manual_next_surfacic_growth_radialness'] = (-1., 1.)
    signal_lut_ranges['manual_next_surfacic_growth_radialness'] = (-0.25, 0.25)

    signal_colormaps['manual_next_divisions'] = 'jet'
    signal_ranges['manual_next_divisions'] = (-1, 2)
    signal_lut_ranges['manual_next_divisions'] = (-1, 2)

    signal_colormaps['manual_next_division_radial_angle'] = 'BuYlRd'
    signal_ranges['manual_next_division_radial_angle'] = (0., 90.)
    signal_lut_ranges['manual_next_division_radial_angle'] = (0., 90.)

    signal_colormaps['next_radial_growth'] = 'jet'
    signal_ranges['next_radial_growth'] = (0.8, 2)
    signal_lut_ranges['next_radial_growth'] = (0.9, 1.5)

    signal_colormaps['next_main_surfacic_growth_radial_angle'] = 'BuYlRd_r'
    signal_ranges['next_main_surfacic_growth_radial_angle'] = (0., 90.)
    signal_lut_ranges['next_main_surfacic_growth_radial_angle'] = (0., 90.)

    signal_colormaps['next_surfacic_growth_radialness'] = 'BuYlRd'
    signal_ranges['next_surfacic_growth_radialness'] = (-1., 1.)
    signal_lut_ranges['next_surfacic_growth_radialness'] = (-0.25, 0.25)

    update_lut_ranges(data_dirname)

    experiments = args.experiments
    sam_ids = args.sam_ids
    image_dirname = args.output_directory if args.output_directory is not None else data_dirname+"/nuclei_images"

    sequence_names = {}
    for exp in experiments:
        experiment_name = get_experiment_name(exp,data_dirname)
        reference_name = get_experiment_reference(exp, data_dirname)
        membrane_name = get_experiment_membrane(exp, data_dirname)
        logging.info("--> Loading sequences for experiment " + str(exp))

        sequence_names[exp] = []
        for sam_id in sam_ids:
            sequence_name = experiment_name + "_sam" + str(sam_id).zfill(2)
            logging.debug("--> Trying to load sequence " + str(sequence_name))

            sequence_segmented_images = load_sequence_segmented_images(sequence_name, image_dirname, verbose=args.verbose, debug=args.debug, loglevel=1)
            if len(sequence_segmented_images) > 0:
                sequence_names[exp] += [sequence_name]
                logging.debug("--> Loaded sequence " + str(sequence_name) + "!")

    for exp in experiments:
        for sequence_name in sequence_names[exp]:
            sequence_segmented_images = load_sequence_segmented_images(sequence_name, image_dirname=image_dirname)

            filenames = np.sort(list(sequence_segmented_images.keys()))
            file_times = [int(f[-2:]) for f in filenames]

            filenames = [f for t, f in zip(file_times, filenames) if t in [0, 10]]

            print(filenames)

            sequence_element_topomesh = segmentation_element_topomeshes(sequence_name, image_dirname=image_dirname, surface=True)

            if args.growth_estimation:
                figure = plt.figure(1)
                figure.clf()

                sequence_lineages = {}
                for i_t, (filename, previous_filename) in enumerate(zip(filenames[1:], filenames[:-1])):

                    seg_img = sequence_segmented_images[filename]
                    extent = [0, seg_img.extent[0], 0, seg_img.extent[1]]

                    time = file_times[i_t]
                    file_dirname = image_dirname + "/" + sequence_name + "/" + filename
                    lineage_filename = file_dirname + "/" + filename + "_to_t" + str(time).zfill(2) + "_manual_cell_lineage.csv"
                    lineage_df = pd.read_csv(lineage_filename)

                    lineage_dict = dict(lineage_df[['label', 'mother_label']].values)
                    sequence_lineages[filename] = lineage_dict

                    previous_element_topomesh = sequence_element_topomesh[previous_filename]
                    previous_element_topomesh.update_wisp_property('lineage_label', 3,
                                                                   {c: c % 256 if c in lineage_dict.values() else np.nan
                                                                    for c in previous_element_topomesh.wisps(3)})

                    element_topomesh = sequence_element_topomesh[filename]
                    element_topomesh.update_wisp_property('lineage_label', 3,
                                                          {c: lineage_dict[c] % 256 if c in lineage_dict.keys() else np.nan
                                                           for c in element_topomesh.wisps(3)})

                    for i_m, topomesh in enumerate([previous_element_topomesh, element_topomesh]):
                        figure.add_subplot(len(filenames)-1, 2, 2 * i_t + i_m + 1)

                        mpl_draw_topomesh(topomesh, figure, 3, property_name='lineage_label', colormap='glasbey', intensity_range=(0, 255))
                        mpl_draw_topomesh(topomesh, figure, 1, color='k', alpha=0.5, linewidth=0.5)
                        mpl_draw_topomesh(topomesh, figure, 1, color='k', cell_edges=True, linewidth=2)

                        figure.gca().axis('off')
                        figure.gca().set_xlim(*extent[:2])
                        figure.gca().set_ylim(*extent[2:])

                figure.set_size_inches(40, 20*(len(filenames) - 1))
                figure.tight_layout()

                figure_filename = image_dirname + "/" + sequence_name + "/" + sequence_name + "_manual_lineage_cells.png"
                figure.savefig(figure_filename)

                figure = plt.figure(1)
                figure.clf()

                sequence_next_division_directions = {}
                for i_t, (filename, previous_filename) in enumerate(zip(filenames[1:], filenames[:-1])):
                    lineage_dict = sequence_lineages[filename]

                    previous_element_topomesh = sequence_element_topomesh[previous_filename]
                    previous_labels = list(previous_element_topomesh.wisps(3))

                    element_topomesh = sequence_element_topomesh[filename]

                    previous_cell_divisions = {}
                    cell_division_direction = {}
                    for c in previous_labels:
                        daughters = [d for d in lineage_dict.keys() if lineage_dict[d] == c]
                        if len(daughters) > 0:
                            previous_cell_divisions[c] = len(daughters) - 1

                            if len(daughters) > 1:
                                division_edges = [e for e in element_topomesh.wisps(1)
                                                  if np.all([d in element_topomesh.regions(1, e, 2) for d in daughters])]

                                if len(division_edges) > 0:
                                    division_edge_points = element_topomesh.wisp_property('barycenter', 0).values(element_topomesh.wisp_property('vertices', 1).values(division_edges))
                                    division_edge_vectors = division_edge_points[:, 1] - division_edge_points[:, 0]
                                    division_edge_vectors *= np.sign(np.einsum("...ij,...ij->...i", division_edge_vectors, division_edge_vectors[:1]))[:, np.newaxis]
                                    division_direction = np.mean(division_edge_vectors, axis=0)
                                    division_direction /= np.linalg.norm(division_direction)
                                    cell_division_direction[c] = division_direction
                                else:
                                    cell_division_direction[c] = np.nan * np.ones(3)
                            else:
                                cell_division_direction[c] = np.nan * np.ones(3)
                        else:
                            previous_cell_divisions[c] = np.nan
                            cell_division_direction[c] = np.nan * np.ones(3)
                    sequence_next_division_directions[previous_filename] = cell_division_direction

                    previous_element_topomesh.update_wisp_property('divisions', 3, previous_cell_divisions)
                    element_topomesh.update_wisp_property('division_direction', 3, {c: cell_division_direction[lineage_dict[c]]
                    if c in lineage_dict.keys()
                    else np.nan * np.ones(3)
                                                                                    for c in element_topomesh.wisps(3)})
                    element_topomesh.update_wisp_property('division_direction_opp', 3, {c: -cell_division_direction[lineage_dict[c]]
                    if c in lineage_dict.keys()
                    else np.nan * np.ones(3)
                                                                                        for c in element_topomesh.wisps(3)})

                    figure.add_subplot(len(filenames) - 1, 2, 2 * i_t + 1)

                    mpl_draw_topomesh(previous_element_topomesh, figure, 3, property_name='divisions', colormap='jet', intensity_range=(-1, 2))
                    mpl_draw_topomesh(previous_element_topomesh, figure, 1, color='k', alpha=0.5, linewidth=0.5)
                    mpl_draw_topomesh(previous_element_topomesh, figure, 1, color='k', cell_edges=True, linewidth=2)

                    figure.gca().axis('off')
                    figure.gca().set_xlim(*extent[:2])
                    figure.gca().set_ylim(*extent[2:])

                    figure.add_subplot(len(filenames) - 1, 2, 2 * i_t + 2)

                    mpl_draw_topomesh(element_topomesh, figure, 3, property_name='division_direction', color='r', coef=3, alpha=0.5)
                    mpl_draw_topomesh(element_topomesh, figure, 3, property_name='division_direction_opp', color='r', coef=3, alpha=0.5)
                    mpl_draw_topomesh(element_topomesh, figure, 1, color='k', alpha=0.5, linewidth=0.5)
                    mpl_draw_topomesh(element_topomesh, figure, 1, color='k', cell_edges=True, linewidth=2)

                    figure.gca().axis('off')
                    figure.gca().set_xlim(*extent[:2])
                    figure.gca().set_ylim(*extent[2:])

                figure.set_size_inches(40, 20 * (len(filenames) - 1))
                figure.tight_layout()

                figure_filename = image_dirname + "/" + sequence_name + "/" + sequence_name + "_manual_lineage_cell_divisions.png"
                figure.savefig(figure_filename)

                sequence_relabelled_segmented_images = {}
                sequence_lineage_element_topomesh = {}
                for i_t, (filename, previous_filename) in enumerate(zip(filenames[1:], filenames[:-1])):
                    previous_element_topomesh = sequence_element_topomesh[previous_filename]

                    previous_seg_img = sequence_segmented_images[previous_filename]
                    seg_img = sequence_segmented_images[filename]
                    seg_img = LabelledImage(seg_img, no_label_id=0)
                    lineage_dict = sequence_lineages[filename].copy()

                    for l in seg_img.labels():
                        if not l in lineage_dict.keys():
                            lineage_dict[l] = previous_seg_img.max() + l
                    lineage_dict[1] = 1
                    lineage_dict = array_dict(lineage_dict)

                    lineage_seg_img = LabelledImage(lineage_dict.values(seg_img.get_array()).astype(seg_img.dtype),
                                                    voxelsize=seg_img.voxelsize,
                                                    no_label_id=0)
                    sequence_relabelled_segmented_images[filename] = lineage_seg_img

                    time = file_times[i_t]
                    file_dirname = image_dirname + "/" + sequence_name + "/" + filename
                    lineage_segmentation_filename = file_dirname + "/" + filename + "_" + membrane_name + "_seg_relabelled_to_t" + str(time).zfill(2) + ".inr.gz"
                    imsave(lineage_segmentation_filename, lineage_seg_img)

                    relabelled_element_topomesh = segmentation_topological_element_cell_topomesh(lineage_seg_img,
                                                                                                 dimension=2,
                                                                                                 resampling_voxelsize=None,
                                                                                                 fuse_vertices=True)

                    cell_division_direction = sequence_next_division_directions[previous_filename]
                    relabelled_element_topomesh.update_wisp_property('division_direction', 3, {c: cell_division_direction[c]
                                                                                               if c in cell_division_direction.keys()
                                                                                               else np.nan * np.ones(3)
                                                                                               for c in relabelled_element_topomesh.wisps(3)})

                    sequence_lineage_element_topomesh[filename] = relabelled_element_topomesh

                figure = plt.figure(2)
                figure.clf()

                sequence_growth_data = {}
                for i_t, (filename, previous_filename) in enumerate(zip(filenames[1:], filenames[:-1])):
                    lineage_dict = sequence_lineages[filename]

                    previous_element_topomesh = deepcopy(sequence_element_topomesh[previous_filename])
                    cells_to_remove = [c for c in previous_element_topomesh.wisps(3) if not c in lineage_dict.values()]
                    for c in cells_to_remove:
                        previous_element_topomesh.remove_wisp(3, c)

                    relabelled_element_topomesh = deepcopy(sequence_lineage_element_topomesh[filename])
                    cells_to_remove = [c for c in relabelled_element_topomesh.wisps(3) if not c in previous_element_topomesh.wisps(3)]
                    for c in cells_to_remove:
                        relabelled_element_topomesh.remove_wisp(3, c)

                    growth_data = topomesh_cell_surfacic_growth_data(previous_element_topomesh, relabelled_element_topomesh)
                    sequence_growth_data[filename] = growth_data

                    for i_m, topomesh in enumerate([previous_element_topomesh, relabelled_element_topomesh]):
                        faces_to_remove = [f for f in topomesh.wisps(2) if topomesh.nb_regions(2, f) == 0]
                        for f in faces_to_remove:
                            topomesh.remove_wisp(2, f)
                        topomesh = clean_topomesh(topomesh, degree=2, clean_properties=True)

                        figure.add_subplot(len(filenames)-1, 2, 2 * i_t + i_m + 1)

                        mpl_draw_topomesh(topomesh, figure, 3, property_name='label', colormap='glasbey', intensity_range=(0, 255))
                        mpl_draw_topomesh(topomesh, figure, 1, color='k', alpha=0.5, linewidth=0.5)
                        mpl_draw_topomesh(topomesh, figure, 1, color='k', cell_edges=True, linewidth=2)

                        figure.gca().axis('off')
                        figure.gca().set_xlim(*extent[:2])
                        figure.gca().set_ylim(*extent[2:])

                figure.set_size_inches(40, 20*(len(filenames) - 1))
                figure.tight_layout()

                figure_filename = image_dirname + "/" + sequence_name + "/" + sequence_name + "_manual_lineage_relabelled_cells.png"
                figure.savefig(figure_filename)

                sequence_cell_volumes = {}
                for filename in filenames:
                    seg_img = sequence_segmented_images[filename]
                    cell_labels = [c for c in np.unique(seg_img.get_array()) if c != 1]
                    cell_volumes = dict(zip(cell_labels,
                                            nd.sum(np.ones_like(seg_img.get_array()) * np.prod(seg_img.voxelsize),
                                                   seg_img.get_array(),
                                                   index=cell_labels)))
                    sequence_cell_volumes[filename] = array_dict(cell_volumes)

                for i_t, (filename, previous_filename) in enumerate(zip(filenames[1:], filenames[:-1])):
                    lineage_dict = sequence_lineages[filename]

                    cell_volumes = sequence_cell_volumes[filename]
                    previous_cell_volumes = sequence_cell_volumes[previous_filename]

                    seg_img = sequence_segmented_images[filename]
                    extent = [0, seg_img.extent[0], 0, seg_img.extent[1]]

                    previous_element_topomesh = sequence_element_topomesh[previous_filename]

                    previous_lineage_cells = np.unique(list(lineage_dict.values()))
                    previous_lineage_volumes = previous_cell_volumes.values(previous_lineage_cells)

                    lineage_volumes = nd.sum(cell_volumes.values(list(lineage_dict.keys())),
                                             list(lineage_dict.values()), index=previous_lineage_cells)

                    volume_ratios = dict(zip(previous_lineage_cells, lineage_volumes / previous_lineage_volumes))

                    previous_element_topomesh.update_wisp_property('image_volume_ratio', 3,
                                                                   {c: volume_ratios[c]
                                                                   if c in volume_ratios.keys()
                                                                   else np.nan
                                                                    for c in previous_element_topomesh.wisps(3)})

                figure = plt.figure(3)
                figure.clf()

                for i_t, (filename, previous_filename) in enumerate(zip(filenames[1:], filenames[:-1])):
                    seg_img = sequence_segmented_images[filename]
                    extent = [0, seg_img.extent[0], 0, seg_img.extent[1]]

                    previous_element_topomesh = sequence_element_topomesh[previous_filename]

                    growth_data = sequence_growth_data[filename]

                    for property_name in growth_data.keys():
                        print(property_name)
                        growth_property = growth_data[property_name]
                        previous_element_topomesh.update_wisp_property(property_name, 3,
                                                                       {c: growth_property[c]
                                                                       if c in growth_property.keys()
                                                                       else np.nan * np.ones(3) if 'direction' in property_name
                                                                                                   or 'vector' in property_name
                                                                       else np.nan * np.ones((3, 3)) if 'tensor' in property_name
                                                                       else np.nan
                                                                        for c in previous_element_topomesh.wisps(3)})

                    figure.add_subplot(len(filenames[1:]), 4, 4 * i_t + 1)

                    mpl_draw_topomesh(previous_element_topomesh, figure, 3, property_name='image_volume_ratio',
                                      colormap='jet', intensity_range=(0.9, 1.5))
                    mpl_draw_topomesh(previous_element_topomesh, figure, 1, color='k', alpha=0.5, linewidth=0.5)
                    mpl_draw_topomesh(previous_element_topomesh, figure, 1, color='k', cell_edges=True, linewidth=2)

                    figure.gca().axis('off')
                    figure.gca().set_xlim(*extent[:2])
                    figure.gca().set_ylim(*extent[2:])

                    figure.add_subplot(len(filenames[1:]), 4, 4 * i_t + 2)

                    mpl_draw_topomesh(previous_element_topomesh, figure, 3, property_name='surfacic_growth',
                                      colormap='jet', intensity_range=(0.9, 1.5))
                    # mpl_draw_topomesh(previous_element_topomesh, figure, 1, color='k', alpha=0.5, linewidth=0.5)
                    mpl_draw_topomesh(previous_element_topomesh, figure, 1, color='k', cell_edges=True, linewidth=2)
                    # mpl_draw_topomesh(previous_element_topomesh, figure, 3, property_name='surfacic_strain_tensor',
                    #                   color='k',coef=1)
                    mpl_draw_topomesh(previous_element_topomesh, figure, 3, property_name='main_surfacic_growth_direction',
                                      color='k', coef=3, alpha=0.5)
                    previous_element_topomesh.update_wisp_property('main_surfacic_growth_direction_opp', 3,
                                                                   {c: -d for c, d in previous_element_topomesh.wisp_property('main_surfacic_growth_direction', 3).items()})
                    mpl_draw_topomesh(previous_element_topomesh, figure, 3, property_name='main_surfacic_growth_direction_opp',
                                      color='k', coef=3, alpha=0.5)
                    del previous_element_topomesh._wisp_properties[3]['main_surfacic_growth_direction_opp']

                    figure.gca().axis('off')
                    figure.gca().set_xlim(*extent[:2])
                    figure.gca().set_ylim(*extent[2:])

                    figure.add_subplot(len(filenames[1:]), 4, 4 * i_t + 3)

                    mpl_draw_topomesh(previous_element_topomesh, figure, 3, property_name='surfacic_growth_anisotropy',
                                      colormap='YlOrRd', intensity_range=(0., 0.25))
                    mpl_draw_topomesh(previous_element_topomesh, figure, 1, color='k', cell_edges=True, linewidth=2)
                    mpl_draw_topomesh(previous_element_topomesh, figure, 3, property_name='surfacic_stretch_tensor',
                                      color='k', coef=10)

                    figure.gca().axis('off')
                    figure.gca().set_xlim(*extent[:2])
                    figure.gca().set_ylim(*extent[2:])

                    figure.add_subplot(len(filenames[1:]), 4, 4 * i_t + 4)

                    mpl_draw_topomesh(previous_element_topomesh, figure, 3, property_name='divisions',
                                      colormap='jet', intensity_range=(-1, 2))
                    mpl_draw_topomesh(previous_element_topomesh, figure, 1, color='k', cell_edges=True, linewidth=2)

                    if 'division_direction' in previous_element_topomesh.wisp_property_names(3):
                        mpl_draw_topomesh(previous_element_topomesh, figure, 3, property_name='division_direction', color='r', coef=3, alpha=0.5)

                        previous_element_topomesh.update_wisp_property('division_direction_opp', 3,
                                                                       {c: -d for c, d in previous_element_topomesh.wisp_property('division_direction', 3).items()})
                        mpl_draw_topomesh(previous_element_topomesh, figure, 3, property_name='division_direction_opp', color='r', coef=3, alpha=0.5)
                        del previous_element_topomesh._wisp_properties[3]['division_direction_opp']

                    figure.gca().axis('off')
                    figure.gca().set_xlim(*extent[:2])
                    figure.gca().set_ylim(*extent[2:])

                figure.set_size_inches(80, 20 * (len(filenames) - 1))
                figure.tight_layout()

                figure_filename = image_dirname + "/" + sequence_name + "/" + sequence_name + "_manual_surfacic_growth_estimation.png"
                figure.savefig(figure_filename)

                manual_data = {}
                for i_t, (filename, previous_filename) in enumerate(zip(filenames[1:], filenames[:-1])):
                    previous_element_topomesh = sequence_element_topomesh[previous_filename]

                    cell_volumes = sequence_cell_volumes[filename]
                    previous_element_topomesh.update_wisp_property('volume', 3, {c: cell_volumes[c]
                                                                                 if c in cell_volumes.keys()
                                                                                 else np.nan
                                                                                 for c in previous_element_topomesh.wisps(3)})

                    compute_topomesh_property(previous_element_topomesh, 'area', 2)
                    compute_topomesh_cell_property_from_faces(previous_element_topomesh, 'area', weighting='uniform', reduce='sum')

                    compute_topomesh_property(previous_element_topomesh, 'vertices', 3)
                    cell_center_vertices = {c: [v for v in previous_element_topomesh.wisp_property('vertices', 3)[c]
                                                if previous_element_topomesh.wisp_property('dimension', 0)[v] == 2][0]
                                            for c in previous_element_topomesh.wisps(3)}
                    cell_surface_centers = {c: previous_element_topomesh.wisp_property('barycenter', 0)[v]
                                            for c, v in cell_center_vertices.items()}
                    previous_element_topomesh.update_wisp_property('barycenter', 3, {c: cell_surface_centers[c] for c in previous_element_topomesh.wisps(3)})

                    previous_cell_df = topomesh_to_dataframe(previous_element_topomesh, 3, scalar_only=False)
                    previous_cell_df['label'] = [c for c in previous_element_topomesh.wisps(3)]
                    del previous_cell_df['lineage_label']
                    del previous_cell_df['barycenter']
                    del previous_cell_df['center']

                    manual_data[previous_filename] = previous_cell_df

                    manual_data_filename = image_dirname + "/" + sequence_name + "/" + previous_filename + "/" + previous_filename + "_manual_cell_data.csv"
                    previous_cell_df.to_csv(manual_data_filename, index=False)

            if args.cell_plot or args.growth_alignment:
                file_data = {}
                for filename in filenames[:-1]:
                    element_topomesh = sequence_element_topomesh[filename]

                    aligned_data_filename = image_dirname + "/" + sequence_name + "/" + filename + "/" + filename + "_cell_data.csv"
                    if os.path.exists(aligned_data_filename):
                        file_data[filename] = pd.read_csv(aligned_data_filename)
                        #file_data[filename]['min_curvature'] = file_data[filename]['mean_curvature'] - np.sqrt(np.power(file_data[filename]['mean_curvature'], 2) - file_data[filename]['gaussian_curvature'])
                        file_data[filename]['min_curvature'] = file_data[filename]['principal_curvature_min']
                        #file_data[filename]['max_curvature'] = file_data[filename]['mean_curvature'] + np.sqrt(np.power(file_data[filename]['mean_curvature'], 2) - file_data[filename]['gaussian_curvature'])
                        file_data[filename]['max_curvature'] = file_data[filename]['principal_curvature_max']

                    manual_data_filename = image_dirname + "/" + sequence_name + "/" + filename + "/" + filename + "_manual_cell_data.csv"
                    manual_df = pd.read_csv(manual_data_filename)
                    for column in ['image_volume_ratio','surfacic_growth','surfacic_growth_anisotropy','area_ratio','main_surfacic_growth_direction','surfacic_strain_tensor','divisions','division_direction']:
                        file_data[filename]['manual_next_' + column] = [manual_df[manual_df['label'] == c][column].values[0]
                                                                        if c in manual_df['label'].values
                                                                        else np.nan
                                                                        for c in file_data[filename]['label']]

                    for column in file_data[filename].columns:
                        property_dict = {c: file_data[filename][file_data[filename]['label'] == c][column].values[0]
                                            if c in file_data[filename]['label'].values
                                            else np.nan
                                         for c in element_topomesh.wisps(3)}
                        element_topomesh.update_wisp_property(column, 3, property_dict)

            if args.cell_plot:
                figure = plt.figure(0)

                #signal_names = ['GA Input', 'CLV3', 'min_curvature', 'manual_next_image_volume_ratio', 'manual_next_surfacic_growth', 'manual_next_surfacic_growth_anisotropy']
                signal_names = [s for s in sequence_element_topomesh[filenames[0]].wisp_property_names(3)
                                  if s in quantified_signals and not "Normalized_" in s]

                for i_t, filename in enumerate(filenames[:-1]):
                    seg_img = sequence_segmented_images[filename]
                    extent = [0, seg_img.extent[0], 0, seg_img.extent[1]]

                    element_topomesh = sequence_element_topomesh[filename]

                    for i_s, signal_name in enumerate(signal_names):
                        figure.add_subplot(len(filenames[:-1]), len(signal_names), len(signal_names) * i_t + i_s + 1)

                        mpl_draw_topomesh(element_topomesh, figure, 3, property_name=signal_name,
                                          colormap=signal_colormaps[signal_name],
                                          intensity_range=signal_lut_ranges[signal_name])
                        # mpl_draw_topomesh(element_topomesh, figure, 1, color='k', alpha=0.5, linewidth=0.5)
                        mpl_draw_topomesh(element_topomesh, figure, 1, color='k', cell_edges=True, linewidth=1)

                        figure.gca().axis('off')
                        figure.gca().set_xlim(*extent[:2])
                        figure.gca().set_ylim(*extent[2:])
                        figure.gca().set_title(signal_name, size=24)

                figure.set_size_inches(12 * len(signal_names), 12 * len(filenames[:-1]))
                figure.tight_layout()

                figure_filename = image_dirname + "/" + sequence_name + "/" + sequence_name + "_manual_surfacic_growth_cell_maps.png"
                figure.savefig(figure_filename)

            if args.growth_alignment:
                sequence_aligned_topomesh = {}
                for filename in filenames[:-1]:
                    img_points = file_data[filename][['center_' + dim for dim in 'xyz']].values
                    aligned_points = file_data[filename][['aligned_' + dim for dim in 'xyz']].values

                    microscope_orientation = get_experiment_microscope_orientation(exp, data_dirname)

                    T = pts2transfo(microscope_orientation * img_points, aligned_points)
                    reflection = np.sign(T[0, 0] * T[1, 1]) == -1

                    if reflection:
                        T = pts2transfo(microscope_orientation * img_points, aligned_points * np.array([1, -1, 1]))

                    R = deepcopy(T[:3, :3])
                    if reflection:
                        R *= np.array([1, -1, 1])[:, np.newaxis]

                    image_topomesh = sequence_element_topomesh[filename]

                    aligned_topomesh = topomesh_transformation(image_topomesh, transformation_matrix=T)
                    if reflection:
                        aligned_topomesh = topomesh_transformation(aligned_topomesh, transformation_matrix=np.diag([1, -1, 1, 1]))
                    compute_topomesh_property(aligned_topomesh, 'barycenter', 1)
                    compute_topomesh_property(aligned_topomesh, 'length', 1)
                    compute_topomesh_property(aligned_topomesh, 'barycenter', 2)
                    compute_topomesh_property(aligned_topomesh, 'area', 2)
                    compute_topomesh_property(aligned_topomesh, 'barycenter', 3)

                    strain_tensors = file_data[filename]['manual_next_surfacic_strain_tensor'].values
                    strain_tensors = np.array([array_from_printed_string(t, null_value=np.nan * np.ones((3, 3))) for t in strain_tensors])

                    tensor_validity = np.logical_not(np.any(np.any(np.isnan(strain_tensors), axis=1), axis=1))
                    e_vals, e_vecs = np.linalg.eigh(strain_tensors[tensor_validity])

                    normal_directions = np.nan * np.ones((len(strain_tensors), 3))
                    normal_directions[tensor_validity] = e_vecs[:, :, 0]

                    transformed_normal_directions = np.transpose(np.dot(R, normal_directions.T))
                    radial_directions = aligned_points * np.array([1, 1, 0])
                    radial_directions /= np.linalg.norm(radial_directions, axis=1)[:, np.newaxis]

                    surface_tangential_directions = np.cross(transformed_normal_directions, radial_directions)
                    surface_tangential_directions /= np.linalg.norm(surface_tangential_directions, axis=1)[:, np.newaxis]

                    surface_radial_directions = np.cross(surface_tangential_directions, transformed_normal_directions)
                    surface_radial_directions /= np.linalg.norm(surface_radial_directions, axis=1)[:, np.newaxis]
                    surface_radial_directions *= np.sign(np.einsum("...ij,...ij->...i", surface_radial_directions, radial_directions))[:, np.newaxis]

                    growth_directions = file_data[filename]['manual_next_main_surfacic_growth_direction'].values
                    growth_directions = np.array([array_from_printed_string(d, null_value=np.nan * np.ones(3)) for d in growth_directions])

                    transformed_growth_directions = np.transpose(np.dot(R, growth_directions.T))

                    growth_direction_cosines = np.einsum("...ij,...ij->...i", surface_radial_directions, transformed_growth_directions)
                    growth_direction_cosines /= np.linalg.norm(transformed_growth_directions, axis=1)
                    growth_direction_angles = np.degrees(np.arccos(np.abs(growth_direction_cosines)))
                    file_data[filename]['manual_next_main_surfacic_growth_radial_angle'] = growth_direction_angles

                    transformed_strain_tensors = np.array([np.dot(R, np.dot(strain_tensor, R.T)) for strain_tensor in strain_tensors])

                    radial_growth_intensities = np.array([np.dot(d.T / np.linalg.norm(d), np.dot(s, d / np.linalg.norm(d))) for d, s in zip(surface_radial_directions, transformed_strain_tensors)])
                    file_data[filename]['manual_next_radial_growth'] = radial_growth_intensities

                    tangential_growth_intensities = np.array(
                        [np.dot(d.T / np.linalg.norm(d), np.dot(s, d / np.linalg.norm(d))) for d, s in zip(surface_tangential_directions, transformed_strain_tensors)])
                    file_data[filename]['manual_next_tangential_growth'] = tangential_growth_intensities

                    growth_intensities = file_data[filename]['manual_next_surfacic_growth'].values
                    mean_growth_intensities = np.mean([radial_growth_intensities, tangential_growth_intensities], axis=0)

                    # growth_radialness = radial_growth_intensities / tangential_growth_intensities
                    # growth_radialness = radial_growth_intensities / growth_intensities - 1
                    # growth_radialness = (radial_growth_intensities - 1 )/(radial_growth_intensities*tangential_growth_intensities - 1)
                    # growth_radialness = np.power(radial_growth_intensities,2)/growth_intensities - 1
                    # growth_radialness = (radial_growth_intensities - tangential_growth_intensities)/(radial_growth_intensities + tangential_growth_intensities)
                    growth_radialness = np.log(radial_growth_intensities / tangential_growth_intensities) / np.log(2)
                    # growth_radialness = (radial_growth_intensities - growth_intensities) / np.linalg.norm([mean_growth_intensities for _ in range(2)],axis=0)
                    file_data[filename]['manual_next_surfacic_growth_radialness'] = growth_radialness

                    division_directions = file_data[filename]['manual_next_division_direction'].values
                    division_directions = np.array([array_from_printed_string(d, null_value=np.nan * np.ones(3)) for d in division_directions])

                    transformed_division_directions = np.transpose(np.dot(R, division_directions.T))

                    division_direction_cosines = np.einsum("...ij,...ij->...i", surface_radial_directions, transformed_division_directions)
                    division_direction_cosines /= np.linalg.norm(transformed_division_directions, axis=1)
                    division_direction_angles = np.degrees(np.arccos(np.abs(division_direction_cosines)))
                    file_data[filename]['manual_next_division_radial_angle'] = division_direction_angles

                    aligned_data_filename = image_dirname + "/" + sequence_name + "/" + filename + "/" + filename + "_cell_data.csv"
                    file_data[filename].to_csv(aligned_data_filename)

                    cell_labels = file_data[filename]['label'].values

                    strain_tensors = dict(zip(cell_labels, strain_tensors))
                    growth_directions = dict(zip(cell_labels, growth_directions))
                    normal_directions = dict(zip(cell_labels, normal_directions))
                    division_directions = dict(zip(cell_labels, division_directions))

                    image_topomesh.update_wisp_property('surfacic_strain_tensors', 3,
                                                        {c: strain_tensors[c]
                                                            if c in strain_tensors
                                                            else np.nan * np.ones((3, 3))
                                                         for c in image_topomesh.wisps(3)})
                    image_topomesh.update_wisp_property('main_surfacic_growth_direction', 3,
                                                        {c: growth_directions[c]
                                                            if c in growth_directions
                                                            else np.nan * np.ones(3)
                                                         for c in image_topomesh.wisps(3)})
                    image_topomesh.update_wisp_property('main_surfacic_growth_direction_opp', 3,
                                                        {c: -growth_directions[c]
                                                            if c in growth_directions
                                                            else np.nan * np.ones(3)
                                                         for c in image_topomesh.wisps(3)})
                    image_topomesh.update_wisp_property('division_direction', 3,
                                                        {c: division_directions[c]
                                                            if c in division_directions
                                                            else np.nan * np.ones(3)
                                                         for c in image_topomesh.wisps(3)})
                    image_topomesh.update_wisp_property('division_direction_opp', 3,
                                                        {c: -division_directions[c]
                                                            if c in division_directions
                                                            else np.nan * np.ones(3)
                                                         for c in image_topomesh.wisps(3)})
                    image_topomesh.update_wisp_property('normal', 0,
                                                        {c: normal_directions[c]
                                                            if c in normal_directions
                                                            else np.nan * np.ones(3)
                                                         for c in image_topomesh.wisps(3)})

                    transformed_strain_tensors = dict(zip(cell_labels, transformed_strain_tensors))
                    transformed_growth_directions = dict(zip(cell_labels, transformed_growth_directions))
                    transformed_normal_directions = dict(zip(cell_labels, transformed_normal_directions))
                    transformed_divisions_directions = dict(zip(cell_labels, transformed_division_directions))
                    surface_radial_directions = dict(zip(cell_labels, surface_radial_directions))
                    growth_direction_angles = dict(zip(cell_labels, growth_direction_angles))
                    radial_growth_intensities = dict(zip(cell_labels, radial_growth_intensities))
                    tangential_growth_intensities = dict(zip(cell_labels, tangential_growth_intensities))
                    growth_radialness = dict(zip(cell_labels, growth_radialness))
                    division_direction_angles = dict(zip(cell_labels, division_direction_angles))

                    aligned_topomesh.update_wisp_property('surfacic_strain_tensors', 3,
                                                          {c: transformed_strain_tensors[c]
                                                              if c in transformed_strain_tensors
                                                              else np.nan * np.ones((3, 3))
                                                           for c in aligned_topomesh.wisps(3)})
                    aligned_topomesh.update_wisp_property('main_surfacic_growth_direction', 3,
                                                          {c: transformed_growth_directions[c]
                                                              if c in transformed_growth_directions
                                                              else np.nan * np.ones(3)
                                                           for c in aligned_topomesh.wisps(3)})
                    aligned_topomesh.update_wisp_property('division_direction', 3,
                                                          {c: transformed_divisions_directions[c]
                                                              if c in transformed_divisions_directions
                                                              else np.nan * np.ones(3)
                                                           for c in image_topomesh.wisps(3)})
                    aligned_topomesh.update_wisp_property('division_direction_opp', 3,
                                                          {c: -transformed_divisions_directions[c]
                                                              if c in transformed_divisions_directions
                                                              else np.nan * np.ones(3)
                                                           for c in image_topomesh.wisps(3)})
                    aligned_topomesh.update_wisp_property('normal', 3,
                                                          {c: transformed_normal_directions[c]
                                                              if c in transformed_normal_directions
                                                              else np.nan * np.ones(3)
                                                           for c in aligned_topomesh.wisps(3)})
                    aligned_topomesh.update_wisp_property('surfacic_radial_direction', 3,
                                                          {c: surface_radial_directions[c]
                                                              if c in surface_radial_directions
                                                              else np.nan * np.ones(3)
                                                           for c in aligned_topomesh.wisps(3)})
                    aligned_topomesh.update_wisp_property('main_surfacic_growth_radial_angle', 3,
                                                          {c: growth_direction_angles[c]
                                                              if c in growth_direction_angles
                                                              else np.nan
                                                           for c in aligned_topomesh.wisps(3)})
                    aligned_topomesh.update_wisp_property('radial_growth', 3,
                                                          {c: radial_growth_intensities[c]
                                                              if c in radial_growth_intensities
                                                              else np.nan
                                                           for c in aligned_topomesh.wisps(3)})
                    aligned_topomesh.update_wisp_property('surfacic_growth_radialness', 3,
                                                          {c: growth_radialness[c]
                                                              if c in growth_radialness
                                                              else np.nan
                                                           for c in aligned_topomesh.wisps(3)})
                    aligned_topomesh.update_wisp_property('division_radial_angle', 3,
                                                          {c: division_direction_angles[c]
                                                              if c in division_direction_angles
                                                              else np.nan
                                                           for c in aligned_topomesh.wisps(3)})

                    sequence_aligned_topomesh[filename] = aligned_topomesh

                figure = plt.figure(0)
                figure.clf()

                for i_f, filename in enumerate(file_data.keys()):
                    aligned_topomesh = sequence_aligned_topomesh[filename]

                    figure.add_subplot(len(file_data), 4, 4 * i_f + 1)

                    aligned_topomesh.update_wisp_property('main_surfacic_growth_direction_opp', 3,
                                                          {c:-d for c,d in aligned_topomesh.wisp_property('main_surfacic_growth_direction',3).items()})
                    aligned_topomesh.update_wisp_property('division_direction_opp', 3,
                                                          {c:-d for c,d in aligned_topomesh.wisp_property('division_direction',3).items()})

                    mpl_draw_topomesh(aligned_topomesh, figure, 3, property_name='surfacic_radial_direction',
                                      color='r', coef=6, alpha=0.5)
                    mpl_draw_topomesh(aligned_topomesh, figure, 3, property_name='main_surfacic_growth_direction',
                                      color='k', coef=3, alpha=0.5, linewidth=2)
                    mpl_draw_topomesh(aligned_topomesh, figure, 3, property_name='main_surfacic_growth_direction_opp',
                                      color='k', coef=3, alpha=0.5, linewidth=2)
                    mpl_draw_topomesh(aligned_topomesh, figure, 3, property_name='division_direction',
                                      color='b', coef=3, alpha=0.5, linewidth=2)
                    mpl_draw_topomesh(aligned_topomesh, figure, 3, property_name='division_direction_opp',
                                      color='b', coef=3, alpha=0.5, linewidth=2)

                    # mpl_draw_topomesh(aligned_topomesh, figure, 3, property_name='surfacic_strain_tensors',
                    #                              color='k', coef=3, alpha=0.5)
                    mpl_draw_topomesh(aligned_topomesh, figure, 1, color='k', cell_edges=True, linewidth=1)
                    del aligned_topomesh._wisp_properties[3]['main_surfacic_growth_direction_opp']
                    del aligned_topomesh._wisp_properties[3]['division_direction_opp']

                    if i_f == 0:
                        figure.gca().set_title("Aligned growth directions", size=18)
                    figure.gca().set_xlim(-100, 100)
                    figure.gca().set_ylim(-100, 100)
                    figure.gca().axis('off')

                    figure.add_subplot(len(file_data), 4, 4 * i_f + 2)

                    signal_name = 'main_surfacic_growth_radial_angle'
                    col = mpl_draw_topomesh(aligned_topomesh, figure, 3, property_name=signal_name,
                                            colormap=signal_colormaps['manual_next_' + signal_name],
                                            intensity_range=signal_lut_ranges['manual_next_' + signal_name],
                                            size=100, color='none')
                    mpl_draw_topomesh(aligned_topomesh, figure, 1, color='k', cell_edges=True, linewidth=1)

                    if i_f == 0:
                        figure.gca().set_title("Main surfacic growth radial angle ($^\circ$)", size=18)
                    figure.gca().set_xlim(-100, 100)
                    figure.gca().set_ylim(-100, 100)
                    figure.gca().axis('off')

                    cax = inset_axes(figure.gca(), width="3%", height="25%", loc='lower right')
                    cbar = figure.colorbar(col, cax=cax, pad=0.)
                    cax.yaxis.set_ticks_position('left')
                    #cbar.set_clim(*signal_lut_ranges['manual_next_' + signal_name])

                    # figure.add_subplot(len(file_data), 4, 4 * i_f + 3)
                    #
                    # signal_name = 'radial_growth'
                    # col = mpl_draw_topomesh(aligned_topomesh, figure, 3, property_name=signal_name,
                    #                         colormap=signal_colormaps['manual_next_' + signal_name],
                    #                         intensity_range=signal_lut_ranges['manual_next_' + signal_name],
                    #                         size=100, color='none')
                    # mpl_draw_topomesh(aligned_topomesh, figure, 1, color='k', cell_edges=True, linewidth=1)
                    #
                    # if i_f == 0:
                    #     figure.gca().set_title("Radial surfacic growth intensity", size=18)
                    # figure.gca().set_xlim(-100, 100)
                    # figure.gca().set_ylim(-100, 100)
                    # figure.gca().axis('off')
                    #
                    # cax = inset_axes(figure.gca(), width="3%", height="25%", loc='lower right')
                    # cbar = figure.colorbar(col, cax=cax, pad=0.)
                    # cax.yaxis.set_ticks_position('left')
                    # cbar.set_clim(*signal_lut_ranges['manual_next_' + signal_name])

                    figure.add_subplot(len(file_data), 4, 4 * i_f + 3)

                    signal_name = 'surfacic_growth_radialness'
                    col = mpl_draw_topomesh(aligned_topomesh, figure, 3, property_name=signal_name,
                                            colormap=signal_colormaps['manual_next_' + signal_name],
                                            intensity_range=signal_lut_ranges['manual_next_' + signal_name],
                                            size=100, color='none')
                    mpl_draw_topomesh(aligned_topomesh, figure, 1, color='k', cell_edges=True, linewidth=1)

                    if i_f == 0:
                        figure.gca().set_title("Radial/tangential surfacic growth ratio (log)", size=18)
                    figure.gca().set_xlim(-100, 100)
                    figure.gca().set_ylim(-100, 100)
                    figure.gca().axis('off')

                    cax = inset_axes(figure.gca(), width="3%", height="25%", loc='lower right')
                    cbar = figure.colorbar(col, cax=cax, pad=0.)
                    cax.yaxis.set_ticks_position('left')
                    #cbar.set_clim(*signal_lut_ranges['manual_next_' + signal_name])

                    figure.add_subplot(len(file_data), 4, 4 * i_f + 4)

                    signal_name = 'division_radial_angle'
                    col = mpl_draw_topomesh(aligned_topomesh, figure, 3, property_name=signal_name,
                                            colormap=signal_colormaps['manual_next_' + signal_name],
                                            intensity_range=signal_lut_ranges['manual_next_' + signal_name],
                                            size=100, color='none')
                    mpl_draw_topomesh(aligned_topomesh, figure, 1, color='k', cell_edges=True, linewidth=1)

                    if i_f == 0:
                        figure.gca().set_title("Division radial angle ($^\circ$)", size=18)
                    figure.gca().set_xlim(-100, 100)
                    figure.gca().set_ylim(-100, 100)
                    figure.gca().axis('off')

                    cax = inset_axes(figure.gca(), width="3%", height="25%", loc='lower right')
                    cbar = figure.colorbar(col, cax=cax, pad=0.)
                    cax.yaxis.set_ticks_position('left')
                    #cbar.set_clim(*signal_lut_ranges['manual_next_' + signal_name])

                figure.set_size_inches(12 * 4, 12 * len(file_data))
                figure.tight_layout()

                figure.savefig(image_dirname + "/" + sequence_name + "/" + sequence_name + "_manual_growth_radialness.png")


if __name__ == "__main__":
    main()