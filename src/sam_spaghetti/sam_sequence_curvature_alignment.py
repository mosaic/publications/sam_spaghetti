import os
import logging

import numpy as np
import scipy.ndimage as nd
import pandas as pd
import matplotlib.pyplot as plt

from tissue_nukem_3d.signal_map import SignalMap
from tissue_nukem_3d.signal_map_visualization import plot_signal_map

from sam_spaghetti.sam_sequence_loading import load_sequence_signal_data, load_sequence_rigid_transformations
from sam_spaghetti.utils.signal_luts import signal_colormaps, signal_ranges, signal_lut_ranges

from sam_atlas.centering import center_sam_sequence
from sam_atlas.alignment import centered_meristem_curvature_map_alignment, load_aligned_sam_database


def align_sam_sequence_curvature_database(sequence_name, image_dirname, sequence_sam_centers=None, database_dirname=None, save_files=True, curvature_type='mean_curvature', r_max=120, cell_radius=5., density_k=0.25, microscope_orientation=-1, verbose=False, debug=False, loglevel=0):

    logging.getLogger().setLevel(logging.INFO if verbose else logging.DEBUG if debug else logging.ERROR)

    aligned_sequence_database = load_aligned_sam_database(database_dirname)

    sequence_data = load_sequence_signal_data(sequence_name, image_dirname, nuclei=False)
    if len(sequence_data) == 1:
        sequence_rigid_transforms = {}
    else:
        sequence_rigid_transforms = load_sequence_rigid_transformations(sequence_name,image_dirname)

    center_sam_sequence(sequence_data, sequence_rigid_transforms, sequence_sam_centers, r_max=r_max, cell_radius=cell_radius, density_k=density_k, microscope_orientation=microscope_orientation)

    if save_files:
        figure = plt.figure(1)
        figure.clf()
        figure.patch.set_facecolor('w')

        for i_file, filename in enumerate(sequence_data.keys()):
            X = sequence_data[filename]['registered_x'].values
            Y = sequence_data[filename]['registered_y'].values
            Z = sequence_data[filename]['registered_z'].values

            figure.gca().scatter(X, Y, c=i_file * np.ones_like(X), cmap='jet', linewidth=0, alpha=0.6, vmin=-1, vmax=5)
            figure.gca().axis('equal')

        figure.set_size_inches(10, 10)
        figure.savefig(image_dirname + "/" + sequence_name + "/" + sequence_name + "_L1_registered_nuclei.png")

        figure = plt.figure(2)
        figure.clf()
        figure.patch.set_facecolor('w')

        for i_file, filename in enumerate(sequence_data.keys()):
            rotated_positions = sequence_data[filename][['centered_' + dim for dim in 'xyz']].values
            figure.gca().scatter(np.linalg.norm(rotated_positions[:, :2], axis=1), rotated_positions[:, 2],
                                 c=i_file * np.ones_like(rotated_positions[:, 0]), cmap='jet', linewidth=0, alpha=0.6, vmin=-1, vmax=5)
            figure.gca().set_xlim(0, 120)
            figure.gca().set_ylim(-50, 10)

        figure.set_size_inches(10, 5)
        figure.savefig(image_dirname + "/" + sequence_name + "/" + sequence_name + "_vertical_axis_optimization.png")

    centered_meristem_curvature_map_alignment(sequence_data, aligned_sequence_database, curvature_type=curvature_type, map_resolution=1, pz_radius=70)

    if save_files:
        figure = plt.figure(0)
        figure.clf()

        for i_file, filename in enumerate(sequence_data.keys()):
            file_data = sequence_data[filename]

            aligned_data_filename = image_dirname + "/" + sequence_name + "/" + filename + "/" + filename + "_cell_data.csv"
            file_data.to_csv(aligned_data_filename,index=False)

            figure.add_subplot(3, len(sequence_data), i_file + 1)

            signal_name = 'CLV3'
            signal_map = SignalMap(file_data, extent=100, resolution=1, position_name='centered')
            signal_map.compute_signal_map(signal_name)

            plot_signal_map(signal_map, signal_name, figure,
                            colormap=signal_colormaps[signal_name],
                            signal_range=signal_ranges[signal_name],
                            signal_lut_range=signal_lut_ranges[signal_name])
            figure.gca().set_title("Centered " + filename)

            figure.add_subplot(3, len(sequence_data), len(sequence_data) + i_file + 1)

            # signal_map = SignalMap(file_data, extent=100, resolution=1, position_name='centered')
            signal_map.compute_signal_map(curvature_type)

            plot_signal_map(signal_map, curvature_type, figure,
                            colormap=signal_colormaps[curvature_type],
                            signal_range=signal_ranges[curvature_type],
                            signal_lut_range=signal_lut_ranges[curvature_type])
            figure.gca().set_title("Centered " + filename)

            figure.add_subplot(3, len(sequence_data), 2*len(sequence_data) + i_file + 1)

            signal_map = SignalMap(file_data,extent=100,resolution=1,position_name='aligned')
            signal_map.compute_signal_map(curvature_type)

            plot_signal_map(signal_map, curvature_type, figure,
                            colormap=signal_colormaps[curvature_type],
                            signal_range=signal_ranges[curvature_type],
                            signal_lut_range=signal_lut_ranges[curvature_type])
            figure.gca().set_title("Aligned " + filename)

        figure.set_size_inches(10*len(sequence_data),10 * 3)
        figure.tight_layout()
        figure.savefig(image_dirname + "/" + sequence_name + "/" + sequence_name + "_curvature_alignment.png")

    return sequence_data

