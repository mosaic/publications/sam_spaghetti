# {# pkglts, version
#  -*- coding: utf-8 -*-

MAJOR = 0
"""(int) Version major component."""

MINOR = 9
"""(int) Version minor component."""

POST = 1
"""(int) Version post or bugfix component."""

__version__ = f"{MAJOR:d}.{MINOR:d}.{POST:d}"
# #}
