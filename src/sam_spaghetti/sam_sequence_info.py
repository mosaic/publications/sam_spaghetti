import numpy as np
import pandas as pd
import os

import sam_spaghetti

sam_spaghetti_dirname = sam_spaghetti.__path__[0]+"/../../share/data"


def get_experiment_name(exp, dirname=sam_spaghetti_dirname):
    experiment_file = dirname+"/experiment_info.csv"
    experiment_data = pd.read_csv(experiment_file,sep=';')
    if not 'experiment' in experiment_data.columns:
        experiment_data = pd.read_csv(experiment_file,sep=',')
    experiment_sequences = dict(zip(experiment_data['experiment'],experiment_data['experiment_name']))
    return experiment_sequences.get(exp,"")


def get_experiment_microscopy(exp, dirname=sam_spaghetti_dirname):
    experiment_file = dirname+"/experiment_info.csv"
    experiment_data = pd.read_csv(experiment_file,sep=';')
    if not 'experiment' in experiment_data.columns:
        experiment_data = pd.read_csv(experiment_file,sep=',')
    experiment_data = experiment_data.replace(np.nan,"")
    experiment_microscopy = dict(zip(experiment_data['experiment'],experiment_data['microscopy_directory']))
    return experiment_microscopy.get(exp,"")


def get_experiment_channels(exp, dirname=sam_spaghetti_dirname):
    experiment_file = dirname+"/experiment_info.csv"
    experiment_data = pd.read_csv(experiment_file,sep=';')
    if not 'experiment' in experiment_data.columns:
        experiment_data = pd.read_csv(experiment_file,sep=',')
    experiment_data = experiment_data.replace(np.nan,"")
    experiment_channels = dict(zip(experiment_data['experiment'],experiment_data['channel_names']))
    channels = experiment_channels.get(exp)
    return eval(channels) if channels != "" else None


def get_experiment_signals(exp, dirname=sam_spaghetti_dirname):
    experiment_file = dirname+"/experiment_info.csv"
    experiment_data = pd.read_csv(experiment_file,sep=';')
    if not 'experiment' in experiment_data.columns:
        experiment_data = pd.read_csv(experiment_file,sep=',')
    experiment_data = experiment_data.replace(np.nan,"")
    experiment_signals = dict(zip(experiment_data['experiment'],experiment_data['signal_names']))
    signals = experiment_signals.get(exp)
    return eval(signals) if signals != "" else None


def get_experiment_reference(exp, dirname=sam_spaghetti_dirname):
    experiment_file = dirname+"/experiment_info.csv"
    experiment_data = pd.read_csv(experiment_file,sep=';')
    if not 'experiment' in experiment_data.columns:
        experiment_data = pd.read_csv(experiment_file,sep=',')
    experiment_data = experiment_data.replace(np.nan,"")
    experiment_references = dict(zip(experiment_data['experiment'],experiment_data['reference_name']))
    reference = experiment_references.get(exp)
    return reference if reference != "" else None


def get_experiment_membrane(exp, dirname=sam_spaghetti_dirname):
    """Get the name of the membrane channel for the experiment.

    Parameters
    ----------
    exp: str
        Experiment identifier
    dirname: str, optional
        Path to the data directory

    Returns
    -------
    str
        Name of the membrane image channel

    """
    experiment_file = dirname+"/experiment_info.csv"
    experiment_data = pd.read_csv(experiment_file,sep=None)
    experiment_data = experiment_data.replace(np.nan,"")
    experiment_membranes = dict(zip(experiment_data['experiment'],experiment_data['membrane_name']))
    membrane = experiment_membranes.get(exp)
    return membrane if membrane != "" else None


def get_experiment_microscope_orientation(exp, dirname=sam_spaghetti_dirname):
    experiment_file = dirname+"/experiment_info.csv"
    experiment_data = pd.read_csv(experiment_file,sep=';')
    if not 'experiment' in experiment_data.columns:
        experiment_data = pd.read_csv(experiment_file,sep=',')
    experiment_data = experiment_data.replace(np.nan,"")
    micoscope_orientation = ""
    if 'microscope_orientation' in experiment_data.columns:
        experiment_micoscope_orientations = dict(zip(experiment_data['experiment'],experiment_data['microscope_orientation']))
        micoscope_orientation = experiment_micoscope_orientations.get(exp)
    return micoscope_orientation if micoscope_orientation != "" else -1


def get_nomenclature_name(czi_file, dirname=sam_spaghetti_dirname, experiment=None):
    czi_filename = os.path.split(czi_file)[1]
    nomenclature_file = dirname+"/nomenclature.csv"
    nomenclature_data = pd.read_csv(nomenclature_file,sep=',')
    if not 'filename' in nomenclature_data.columns:
        nomenclature_data = pd.read_csv(nomenclature_file,sep=';')
    if 'filename' in nomenclature_data.columns:
        if experiment is not None:
            nomenclature_data = nomenclature_data[nomenclature_data['experiment']==experiment]
        nomenclature_names = [get_experiment_name(exp,dirname)+"_sam"+str(sam_id).zfill(2)+"_t"+str(t).zfill(2) for exp,sam_id,t in nomenclature_data[['experiment','sam_id','hour_time']].values]
        nomenclature_names = dict(zip(nomenclature_data['filename'], nomenclature_names))
        # print czi_filename
        return nomenclature_names.get(czi_filename,None)
    else:
        return


def get_nomenclature_info(sequence_name, dirname=sam_spaghetti_dirname):
    nomenclature_file = dirname+"/nomenclature.csv"
    nomenclature_data = pd.read_csv(nomenclature_file,sep=',')
    if not 'filename' in nomenclature_data.columns:
        nomenclature_data = pd.read_csv(nomenclature_file,sep=';')
    if 'filename' in nomenclature_data.columns:
        sequence_names = [get_experiment_name(exp, dirname)+"_sam"+str(sam_id).zfill(2) for exp,sam_id in nomenclature_data[['experiment', 'sam_id']].values]
        nomenclature_data['sequence_name'] = sequence_names
        sequence_sam_info = {c: None for c in ['experiment', 'sam_id']}
        if sequence_name in sequence_names:
            sequence_info_data = nomenclature_data[nomenclature_data['sequence_name'] == sequence_name]
            for column in ['experiment', 'sam_id']:
                if column in sequence_info_data.columns:
                    sequence_sam_info[column] = sequence_info_data[column].values[0]
        return sequence_sam_info
    else:
        return {}

def get_sequence_orientation(sequence_name, dirname=sam_spaghetti_dirname):
    orientation_file = dirname + "/nuclei_image_sam_orientation.csv"
    info_file = dirname + "/sam_info.csv"
    if os.path.exists(orientation_file):
        orientation_data = pd.read_csv(orientation_file,sep=",")
        if not 'experiment' in orientation_data.columns:
            orientation_data = pd.read_csv(orientation_file,sep=";")
        orientation_data['sequence_name'] = [get_experiment_name(exp,dirname)+"_sam"+str(sam_id).zfill(2) for exp,sam_id in orientation_data[['experiment','sam_id']].values]
        if sequence_name in orientation_data['sequence_name'].values:
            meristem_orientation = int(orientation_data[orientation_data['sequence_name']==sequence_name]['orientation'])
            return meristem_orientation
        else:
            raise(KeyError("No SAM orientation information could be found for sequence "+str(sequence_name)))
    elif os.path.exists(info_file):
        info_data = pd.read_csv(info_file,sep=",")
        if not 'experiment' in info_data.columns:
            info_data = pd.read_csv(info_file,sep=";")
        info_data['sequence_name'] = [get_experiment_name(exp,dirname)+"_sam"+str(sam_id).zfill(2) for exp,sam_id in info_data[['experiment','sam_id']].values]
        if sequence_name in info_data['sequence_name'].values and 'orientation' in info_data.columns:
            meristem_orientation = int(info_data[info_data['sequence_name']==sequence_name]['orientation'])
            return meristem_orientation
        else:
            raise(KeyError("No SAM orientation information could be found for sequence "+str(sequence_name)))
    else:
        raise(KeyError("No information file could be found!"))



def get_sequence_sam_centers(sequence_name, dirname):
    center_file = dirname + "/sam_center.csv"
    center_data = pd.read_csv(center_file, sep=",")
    if not 'experiment' in center_data.columns:
        center_data = pd.read_csv(center_file, sep=";")
    center_data['sequence_name'] = [get_experiment_name(exp, dirname) + "_sam" + str(sam_id).zfill(2) for exp, sam_id in center_data[['experiment', 'sam_id']].values]
    if sequence_name in center_data['sequence_name'].values:
        sequence_center_data = center_data[center_data['sequence_name'] == sequence_name]

        sequence_sam_center = {}
        for time, x, y in sequence_center_data[['hour_time', 'center_x', 'center_y']].values:
            filename = sequence_name + "_t" + str(int(time)).zfill(2)
            sequence_sam_center[filename] = np.array([x, y],float)
        return sequence_sam_center
    else:
        print("No SAM center information could be found for sequence " + str(sequence_name))
        return None


def get_sequence_info(sequence_name, dirname):
    info_file = dirname + "/sam_info.csv"
    if os.path.exists(info_file):
        info_data = pd.read_csv(info_file, sep=",")
        if not 'experiment' in info_data.columns:
            info_data = pd.read_csv(info_file, sep=";")
        info_data['sequence_name'] = [get_experiment_name(exp, dirname) + "_sam" + str(sam_id).zfill(2) for exp, sam_id in info_data[['experiment', 'sam_id']].values]
        info_columns = [c for c in info_data.columns if c not in ['experiment','sam_id','sequence_name']]

        sequence_sam_info = {c:None for c in info_columns}
        if sequence_name in info_data['sequence_name'].values:
            sequence_info_data = info_data[info_data['sequence_name'] == sequence_name]
            for column, value in zip(info_columns,sequence_info_data[info_columns].values[0]):
                if not pd.isnull(value):
                    sequence_sam_info[column] = value
    else:
        sequence_sam_info = {}
    return sequence_sam_info


def update_lut_ranges(dirname=sam_spaghetti_dirname):
    from sam_spaghetti.utils.signal_luts import signal_colormaps, signal_ranges, signal_lut_ranges, channel_ranges, quantified_signals
    lut_file = dirname+"/signal_ranges.csv"
    if os.path.exists(lut_file):
        lut_data = pd.read_csv(lut_file,sep=None)
        print(lut_data)
        signal_colormaps.update(dict([(s,c) for s,c in lut_data[['signal_name','colormap']].values if not pd.isnull(c)]))
        signal_ranges.update(dict([(s,eval(r)) for s,r in lut_data[['signal_name','signal_range']].values if not pd.isnull(r)]))
        signal_lut_ranges.update(dict([(s,eval(r)) for s,r in lut_data[['signal_name','color_range']].values if not pd.isnull(r)]))
        channel_ranges.update(dict([(s,eval(r)) for s,r in lut_data[['signal_name','channel_range']].values if not pd.isnull(r)]))

        quantified_signals += [str(s) for s in lut_data['signal_name'].values]
        quantified_signals += ["Normalized_" + str(s) for s in lut_data['signal_name'].values]
