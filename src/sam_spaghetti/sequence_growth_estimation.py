import os
import logging

from copy import deepcopy
from time import time as current_time

import numpy as np
from scipy import ndimage as nd
import pandas as pd
import matplotlib.pyplot as plt

from sklearn import linear_model

from sam_spaghetti.sam_sequence_loading import load_sequence_segmented_images, load_sequence_signal_data, load_sequence_cell_meshes
from sam_spaghetti.sam_sequence_loading import load_sequence_rigid_transformations, load_sequence_vectorfield_transformations
from sam_spaghetti.utils.signal_luts import quantified_signals

from timagetk.components import LabelledImage
from timagetk.plugins.resampling import isometric_resampling
from timagetk.algorithms.trsf import create_trsf, apply_trsf, allocate_c_bal_matrix
from timagetk.wrapping.bal_trsf import TRSF_TYPE_DICT, TRSF_UNIT_DICT

from tissue_analysis.tissue_analysis import TissueImage, TissueAnalysis
from tissue_analysis.array_tools import find_geometric_median

from draco_stem.grifone.grifone import segmentation_topological_element_cell_topomesh

from cellcomplex.property_topomesh.io import save_ply_property_topomesh,read_ply_property_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_cell_property_from_faces
from cellcomplex.property_topomesh.extraction import property_filtering_sub_topomesh
from cellcomplex.property_topomesh.transformation import topomesh_apply_vectorfield_transformation
from cellcomplex.property_topomesh.visualization.matplotlib import mpl_draw_topomesh
from cellcomplex.property_topomesh.visualization.svg import SvgTopomesh

from tissue_nukem_3d.growth_estimation import surfacic_growth_estimation, volumetric_growth_estimation


def compute_growth(sequence_name, image_dirname, save_files=True, maximal_length=18., microscope_orientation=-1, growth_type='surfacic', verbose=False, debug=False, loglevel=0):
    logging.getLogger().setLevel(logging.INFO if verbose else logging.DEBUG if debug else logging.ERROR)

    sequence_data = load_sequence_signal_data(sequence_name, image_dirname, normalized=True, aligned=False, verbose=verbose, debug=debug, loglevel=loglevel)
    if len(sequence_data) == 0:
        sequence_data = load_sequence_signal_data(sequence_name, image_dirname, nuclei=False, aligned=False, verbose=verbose, debug=debug, loglevel=loglevel)
    sequence_rigid_transforms = load_sequence_rigid_transformations(sequence_name, image_dirname, verbose=verbose, debug=debug, loglevel=loglevel)
    sequence_vectorfield_transforms = load_sequence_vectorfield_transformations(sequence_name, image_dirname, verbose=verbose, debug=debug, loglevel=loglevel)

    if growth_type == 'surfacic':
        sequence_data, sequence_triangulations = surfacic_growth_estimation(sequence_data,sequence_rigid_transforms,sequence_vectorfield_transforms,maximal_length=maximal_length,microscope_orientation=microscope_orientation,quantified_signals=quantified_signals, verbose=verbose, debug=debug, loglevel=loglevel)
    elif growth_type == 'volumetric':
        sequence_data, sequence_triangulations = volumetric_growth_estimation(sequence_data,sequence_rigid_transforms,sequence_vectorfield_transforms,maximal_length=maximal_length,microscope_orientation=microscope_orientation,quantified_signals=quantified_signals, verbose=verbose, debug=debug, loglevel=loglevel)

    if save_files:
        for filename in sequence_data:
            normalized=True
            aligned=False
            data_filename = "".join([image_dirname+"/"+sequence_name+"/"+filename+"/"+filename,"_aligned_L1" if aligned else "","_normalized" if normalized else "","_signal_data.csv"])
            sequence_data[filename].to_csv(data_filename,index=False)
            properties_to_save = {d: [] for d in range(4)}
            properties_to_save[0] += ['layer']
            if growth_type == 'volumetric':
                triangulation_filename = "".join([image_dirname + "/" + sequence_name + "/" + filename + "/" + filename, "_registered_tetrahedrization.ply"])
                for direction in ['previous','next']:
                    for prop in ['strain_tensor','stretch_tensor','volumetric_growth','volumetric_growth_anisotropy','main_growth_direction']:
                        properties_to_save[0] += [direction + "_"+ prop]
                        properties_to_save[3] += [direction + "_"+ prop]
            elif growth_type == 'surfacic':
                triangulation_filename = "".join([image_dirname + "/" + sequence_name + "/" + filename + "/" + filename, "_registered_L1_triangulation.ply"])
                for direction in ['previous','next']:
                    for prop in ['surfacic_strain_tensor','surfacic_stretch_tensor','surfacic_growth','surfacic_growth_anisotropy','main_surfacic_growth_direction']:
                        properties_to_save[0] += [direction + "_"+ prop]
                        properties_to_save[2] += [direction + "_"+ prop]
            save_ply_property_topomesh(sequence_triangulations[filename],triangulation_filename,properties_to_save=properties_to_save)

    return sequence_data


def segmentation_element_topomeshes(sequence_name, image_dirname, save_files=True, resampling_voxelsize=None, microscope_orientation=-1, surface=True, verbose=False, debug=False, loglevel=0):

    sequence_segmented_images = load_sequence_segmented_images(sequence_name, image_dirname, verbose=verbose, debug=debug, loglevel=loglevel + 1)
    sequence_element_topomesh = load_sequence_cell_meshes(sequence_name, image_dirname, surface=surface, verbose=verbose, debug=debug, loglevel=loglevel+1)

    for filename in sequence_segmented_images.keys():
        if not filename in sequence_element_topomesh.keys():
            seg_img = sequence_segmented_images[filename]

            element_topomesh = segmentation_topological_element_cell_topomesh(seg_img,
                                                                              dimension=2 if surface else 3,
                                                                              resampling_voxelsize=resampling_voxelsize,
                                                                              fuse_vertices=surface)

            sequence_element_topomesh[filename] = element_topomesh

    for filename in sequence_element_topomesh.keys():
        element_topomesh = sequence_element_topomesh[filename]
        if save_files:
            ply_filename = image_dirname + "/" + sequence_name + "/" + filename + "/" + filename + ("_l1" if surface else "") + "_cell_topomesh.ply"
            properties_to_save = {}
            properties_to_save[0] = ['dimension']
            properties_to_save[1] = ['length']
            properties_to_save[2] = ['area', 'label']
            properties_to_save[3] = ['area', 'volume', 'center'] + [p for p in element_topomesh.wisp_property_names(3) if 'next' in p]
            save_ply_property_topomesh(element_topomesh, ply_filename, properties_to_save=properties_to_save)

            if surface:
                svg_filename = image_dirname + "/" + sequence_name + "/" + filename + "/" + filename + "_l1_cells.svg"

                svg = SvgTopomesh(element_topomesh, svg_filename, z_slicing=False, scale=8)
                svg.add_layer(3, use_ids=True, colormap='glasbey', value_range=(0, 255), opacity=0.05)
                svg.add_layer(1, cell_edges=True, draw_ids=False)
                # svg.add_layer(0, property_name='dimension', colormap='jet',value_range=(-1,3))
                svg.add_layer(3, opacity=0, draw_ids=True)
                svg.write()

    return sequence_element_topomesh


def compute_growth_from_segmentation(sequence_name, image_dirname, save_files=True, resampling_voxelsize=None, microscope_orientation=-1, growth_type='surfacic', verbose=False, debug=False, loglevel=0):

    sequence_data = load_sequence_signal_data(sequence_name, image_dirname, nuclei=False, normalized=False, aligned=False, verbose=verbose, debug=debug, loglevel=loglevel+1)
    sequence_segmented_images = load_sequence_segmented_images(sequence_name, image_dirname, verbose=verbose, debug=debug, loglevel=loglevel+1)
    sequence_rigid_transforms = load_sequence_rigid_transformations(sequence_name, image_dirname, verbose=verbose, debug=debug, loglevel=loglevel+1)
    sequence_vectorfield_transforms = load_sequence_vectorfield_transformations(sequence_name, image_dirname, verbose=verbose, debug=debug, loglevel=loglevel+1)

    sequence_element_topomesh = segmentation_element_topomeshes(sequence_name, image_dirname, surface=(growth_type == 'surfacic'), save_files=True,
                                                                resampling_voxelsize=resampling_voxelsize, microscope_orientation=microscope_orientation,
                                                                verbose=verbose, debug=debug, loglevel=loglevel)

    sequence_growth_data, sequence_element_topomesh = segmentation_growth_estimation(sequence_segmented_images, sequence_rigid_transforms, sequence_vectorfield_transforms, sequence_element_topomesh,
                                                                                     growth_type=growth_type, resampling_voxelsize=resampling_voxelsize)

    if save_files:
        for i_file, filename in enumerate(sequence_growth_data.keys()):
            df = sequence_data[filename]
            for column in [c for c in sequence_growth_data[filename].columns if c != 'label']:
                column_dict = {c:v for c,v in sequence_growth_data[filename][['label',column]].values}
                df[column] = [column_dict[c] for c in df['label'].values]
            #data_filename = image_dirname + "/" + sequence_name + "/" + filename + "/" + filename + "_cell_" + growth_type + "_growth_data.csv"
            data_filename = image_dirname + "/" + sequence_name + "/" + filename + "/" + filename + "_cell_data.csv"
            df.to_csv(data_filename, index=False)

        for i_file, filename in enumerate(sequence_element_topomesh.keys()):
            element_topomesh = sequence_element_topomesh[filename]
            ply_filename = image_dirname + "/" + sequence_name + "/" + filename + "/" + filename + ("_l1" if growth_type == 'surfacic' else "") + "_cell_topomesh.ply"
            properties_to_save = {}
            properties_to_save[0] = ['dimension']
            properties_to_save[1] = ['length']
            properties_to_save[2] = ['area', 'label']
            properties_to_save[3] = ['area', 'volume', 'center'] + [p for p in element_topomesh.wisp_property_names(3) if 'next' in p]
            save_ply_property_topomesh(element_topomesh, ply_filename, properties_to_save=properties_to_save)

        # figure = plt.figure(0)
        # figure.clf()
        #
        # for i_file, filename in enumerate(list(sequence_growth_data.keys())[:-1]):
        #     seg_img = sequence_segmented_images[filename]
        #
        #     extent = [0, seg_img.extent[0], 0, seg_img.extent[1]]
        #
        #     figure.add_subplot(len(sequence_growth_data) - 1, 3, i_file * 3 + 1)
        #
        #     projected_seg_img, projection_coords = labelled_image_projection(seg_img, direction=-1, return_coords=True)
        #     figure.gca().imshow(projected_seg_img[::-1] % 256, cmap='glasbey', vmin=0, vmax=255, extent=extent)
        #
        #     next_filename = list(sequence_growth_data.keys())[i_file + 1]
        #     subsampling = 8
        #
        #     figure.add_subplot(len(sequence_growth_data) - 1, 3, i_file * 3 + 2)
        #
        #     invert_vector_field_transform = sequence_vectorfield_transforms[(filename, next_filename)]
        #     invert_vector_field = np.transpose([invert_vector_field_transform.vx.to_spatial_image(),
        #                                         invert_vector_field_transform.vy.to_spatial_image(),
        #                                         invert_vector_field_transform.vz.to_spatial_image()], (1, 2, 3, 0))
        #
        #     projected_invert_vector_field = invert_vector_field[projection_coords].reshape(projected_seg_img.shape + (3,))
        #     xx, yy = np.meshgrid(np.linspace(0, seg_img.extent[0], seg_img.shape[0]),
        #                          np.linspace(0, seg_img.extent[1], seg_img.shape[1]),
        #                          indexing='ij')
        #
        #     figure.gca().quiver(xx[::subsampling, ::subsampling],
        #                         yy[::subsampling, ::subsampling],
        #                         projected_invert_vector_field[::subsampling, ::subsampling, 0],
        #                         projected_invert_vector_field[::subsampling, ::subsampling, 1],
        #                         scale=np.sqrt(1. / subsampling), units='xy', pivot='mid', alpha=0.5)
        #
        #     figure.add_subplot(len(sequence_growth_data) - 1, 3, i_file * 3 + 3)
        #
        #     vector_field_transform = sequence_vectorfield_transforms[(next_filename, filename)]
        #     vector_field = np.transpose([vector_field_transform.vx.to_spatial_image(),
        #                                  vector_field_transform.vy.to_spatial_image(),
        #                                  vector_field_transform.vz.to_spatial_image()], (1, 2, 3, 0))
        #
        #     projected_vector_field = vector_field[projection_coords].reshape(projected_seg_img.shape + (3,))
        #     xx, yy = np.meshgrid(np.linspace(0, seg_img.extent[0], seg_img.shape[0]),
        #                          np.linspace(0, seg_img.extent[1], seg_img.shape[1]),
        #                          indexing='ij')
        #
        #     figure.gca().quiver(xx[::subsampling, ::subsampling],
        #                         yy[::subsampling, ::subsampling],
        #                         projected_vector_field[::subsampling, ::subsampling, 0],
        #                         projected_vector_field[::subsampling, ::subsampling, 1],
        #                         scale=np.sqrt(1. / subsampling), units='xy', pivot='mid', alpha=0.5)
        #
        # figure.set_size_inches(20 * 3, 20 * (len(sequence_growth_data) - 1))
        # figure.tight_layout()
        # figure.savefig(image_dirname + "/" + sequence_name + "/" + sequence_name + "_segmentation_vector_field.png")

        figure = plt.figure(1)
        figure.clf()

        for i_file, filename in enumerate(list(sequence_growth_data.keys())[:-1]):
            seg_img = sequence_segmented_images[filename]
            element_topomesh = sequence_element_topomesh[filename]

            if growth_type == 'volumetric':
                l1_element_topomesh = property_filtering_sub_topomesh(element_topomesh, 'epidermis', 2, (1, 1))
                compute_topomesh_property(element_topomesh, 'faces', 3)
            elif growth_type == 'surfacic':
                l1_element_topomesh = element_topomesh

            # X = sequence_growth_data[filename]['center_x'].values
            # Y = sequence_growth_data[filename]['center_y'].values
            # layer = sequence_growth_data[filename]['layer'].values
            # if growth_type == 'volumetric':
            #     growth_ratios = sequence_growth_data[filename]['next_volume_ratio'].values
            #     growth = sequence_growth_data[filename]['next_volumetric_growth'].values
            #     stretch_tensors = sequence_growth_data[filename]['next_stretch_tensor'].values
            # elif growth_type == 'surfacic':
            #     growth_ratios = sequence_growth_data[filename]['next_mesh_area_ratio'].values
            #     growth = sequence_growth_data[filename]['next_surfacic_growth'].values
            #     stretch_tensors = sequence_growth_data[filename]['next_surfacic_stretch_tensor'].values

            # actors = []
            #
            # vertex_actor = VtkActorTopomesh(element_topomesh,0,'dimension',glyph_scale=0.5)
            # vertex_actor.update(colormap='jet',value_range=(-1,3))
            # actors += [vertex_actor]
            #
            # edge_actor = VtkActorTopomesh(element_topomesh,1)
            # edge_actor = VtkActorTopomesh(element_topomesh,1,line_glyph='tube',glyph_scale=0.1)
            # edge_actor.update(colormap='gray')
            # actors += [edge_actor]
            #
            # # face_actor = VtkActorTopomesh(element_topomesh,2,'label')
            # # face_actor.update(colormap='glasbey',value_range=(0,255))
            # # actors += [face_actor.actor]
            #
            # cell_actor = VtkActorTopomesh(element_topomesh,3,'label')
            # cell_actor.update(colormap='glasbey',value_range=(0,255))
            # actors += [cell_actor]
            #
            # vtk_display_actors(actors)

            extent = [0, seg_img.extent[0], 0, seg_img.extent[1]]

            figure.add_subplot(len(sequence_growth_data) - 1, 4, i_file * 4 + 1)

            # projected_seg_img = labelled_image_projection(seg_img, direction=microscope_orientation)
            # figure.gca().imshow(projected_seg_img[::-1] % 256, cmap='glasbey', vmin=0, vmax=255, extent=extent)

            # figure.gca().scatter(X[layer == 1], Y[layer == 1], color='k', alpha=0.5, s=100)
            mpl_draw_topomesh(l1_element_topomesh, figure, 3, property_name='label', colormap='glasbey', intensity_range=(0, 255))
            mpl_draw_topomesh(l1_element_topomesh, figure, 1, color='w', alpha=0.5)
            # mpl_draw_topomesh(l1_element_topomesh,figure,1,color='w',cell_edges=True,linewidth=2)
            mpl_draw_topomesh(l1_element_topomesh, figure, 0, property_name='dimension', colormap='jet', intensity_range=(-1, 3), color='none')
            # for v, d in l1_element_topomesh.wisp_property('dimension', 0).items():
            #     color = 'orange' if d == 2 else 'chartreuse' if d == 1 else 'deepskyblue'
            #     x = l1_element_topomesh.wisp_property('barycenter', 0)[v][0]
            #     y = l1_element_topomesh.wisp_property('barycenter', 0)[v][1]
            #     figure.gca().text(x, y, str(tuple(l1_element_topomesh.wisp_property('cells', 0)[v])), color=color)

            # figure.gca().scatter(X,Y,c=layer,cmap='jet',vmin=0,vmax=4,alpha=0.5)
            # figure.gca().scatter(X, Y, alpha=0.5)

            figure.gca().axis('equal')
            figure.gca().set_xlim(*extent[:2])
            figure.gca().set_ylim(*extent[2:])

            if growth_type == 'volumetric':
                growth_metric = 'next_volumetric_growth'
                tensor_name = 'next_stretch_tensor'
            elif growth_type == 'surfacic':
                growth_metric = 'next_surfacic_growth'
                tensor_name = 'next_surfacic_stretch_tensor'

            figure.add_subplot(len(sequence_growth_data) - 1, 4, i_file * 4 + 2)

            X = [l1_element_topomesh.wisp_property('barycenter', 0)[v][0] for v in l1_element_topomesh.wisps(0)]
            Y = [l1_element_topomesh.wisp_property('barycenter', 0)[v][1] for v in l1_element_topomesh.wisps(0)]
            Z = [l1_element_topomesh.wisp_property('barycenter', 0)[v][2] for v in l1_element_topomesh.wisps(0)]

            coords = np.round(np.transpose([X,Y,Z])/np.array(seg_img.voxelsize)).astype(int)
            coords = np.minimum(np.array(seg_img.shape)-1,np.maximum(coords,0))
            coords = tuple(np.transpose(coords))

            next_filename = list(sequence_growth_data.keys())[i_file + 1]

            vector_field_transform = sequence_vectorfield_transforms[(filename, next_filename)]
            vector_field = np.transpose([vector_field_transform.vx.to_spatial_image(),
                                         vector_field_transform.vy.to_spatial_image(),
                                         vector_field_transform.vz.to_spatial_image()], (1, 2, 3, 0))

            point_displacement = vector_field[coords]

            figure.gca().quiver(X,Y, point_displacement[:, 0],point_displacement[:, 1],
                                scale=np.sqrt(1./7.), units='xy', pivot='mid', alpha=0.5)

            figure.gca().axis('equal')
            figure.gca().set_xlim(*extent[:2])
            figure.gca().set_ylim(*extent[2:])

            figure.add_subplot(len(sequence_growth_data) - 1, 4, i_file * 4 + 3)

            # figure.gca().scatter(X[layer == 1],
            #                      Y[layer == 1],
            #                      c=growth[layer == 1],
            #                      s=400, cmap='jet', vmin=0.9, vmax=1.5, ec='k')
            mpl_draw_topomesh(l1_element_topomesh, figure, 3, property_name=growth_metric, colormap='jet', intensity_range=(0.9, 1.5))
            mpl_draw_topomesh(l1_element_topomesh, figure, 1, color='k', cell_edges=True)

            figure.gca().axis('equal')
            figure.gca().set_xlim(*extent[:2])
            figure.gca().set_ylim(*extent[2:])
            #
            # figure.add_subplot(len(sequence_growth_data) - 1, 5, i_file * 5 + 4)
            #
            # figure.gca().plot([0, 3], [0, 3], color='k', alpha=0.25)
            #
            # # figure.gca().scatter(growth_ratios,growth,c=layer,cmap='jet',vmin=0,vmax=4,alpha=0.5)
            # figure.gca().scatter(element_topomesh.wisp_property(reference_metric, 3).values(list(element_topomesh.wisps(3))),
            #                      element_topomesh.wisp_property(growth_metric, 3).values(list(element_topomesh.wisps(3))),
            #                      c=[1 if element_topomesh.wisp_property('epidermis', 3)[c] else 2 for c in element_topomesh.wisps(3)],
            #                      cmap='jet', vmin=0, vmax=4, alpha=0.5)
            #
            # figure.gca().set_xlim(0.9, 1.5)
            # figure.gca().set_ylim(0.9, 1.5)

            figure.add_subplot(len(sequence_growth_data) - 1, 4, i_file * 4 + 4)

            # plot_tensor_data(figure,X[layer == 1],Y[layer == 1],stretch_tensors[layer == 1],np.ones_like(X[layer == 1]), tensor_style='crosshair', colormap='gray', value_range=(0, 0), scale=5.)
            # plot_tensor_data(figure, X[layer == 1], Y[layer == 1], stretch_tensors[layer == 1], np.ones_like(X[layer == 1]), tensor_style='ellipse', colormap='gray', value_range=(0, 0), scale=5.,alpha=0.2)

            mpl_draw_topomesh(l1_element_topomesh, figure, 3, property_name=tensor_name, colormap='gray', coef=10.)
            # mpl_draw_topomesh(l1_element_topomesh, figure, 1, color='k', cell_edges=True)

            figure.gca().axis('equal')
            figure.gca().set_xlim(*extent[:2])
            figure.gca().set_ylim(*extent[2:])

        figure.set_size_inches(20 * 4, 20 * (len(sequence_growth_data) - 1))
        figure.tight_layout()
        figure.savefig(image_dirname + "/" + sequence_name + "/" + sequence_name + "_segmentation_" + growth_type + "_growth.png")


def topomesh_cell_volumetric_growth_data(previous_topomesh, next_topomesh, dimensions=[2, 1, 0]):

    print("\n# - Extracting points of interest:")
    # - Extract Points Of Interest:
    previous_points, next_points = {}, {}

    compute_topomesh_property(previous_topomesh, 'cells', 0)
    compute_topomesh_property(previous_topomesh, 'epidermis', 0)
    compute_topomesh_property(next_topomesh, 'cells', 0)
    compute_topomesh_property(next_topomesh, 'epidermis', 0)

    compute_topomesh_property(previous_topomesh, 'barycenter', 3)
    compute_topomesh_property(previous_topomesh, 'epidermis', 3)
    compute_topomesh_property(previous_topomesh, 'vertices', 3)
    compute_topomesh_property(previous_topomesh, 'volume', 3)
    
    compute_topomesh_property(next_topomesh, 'barycenter', 3)
    compute_topomesh_property(next_topomesh, 'epidermis', 3)
    compute_topomesh_property(next_topomesh, 'vertices', 3)
    compute_topomesh_property(next_topomesh, 'volume', 3)

    previous_cells = list(previous_topomesh.wisps(3))
    next_cells = list(next_topomesh.wisps(3))

    previous_cell_centers = {c:previous_topomesh.wisp_property('barycenter',3)[c] for c in previous_cells}
    next_cell_centers = {c:next_topomesh.wisp_property('barycenter',3)[c] for c in next_cells}

    previous_cell_volumes = {c:previous_topomesh.wisp_property('volume',3)[c] for c in previous_cells}
    next_cell_volumes = {c:next_topomesh.wisp_property('volume',3)[c] for c in next_cells}

    previous_element_vertices = [v for v in previous_topomesh.wisps(0) if previous_topomesh.wisp_property('dimension', 0)[v] in dimensions]
    previous_element_medians = {tuple(([1] if previous_topomesh.wisp_property('epidermis', 0)[v] else []) + list(previous_topomesh.wisp_property('cells', 0)[v])): previous_topomesh.wisp_property('barycenter', 0)[v] for v in previous_element_vertices}
    next_element_vertices = [v for v in next_topomesh.wisps(0) if next_topomesh.wisp_property('dimension', 0)[v] in dimensions]
    next_element_medians = {tuple(([1] if next_topomesh.wisp_property('epidermis', 0)[v] else []) + list(next_topomesh.wisp_property('cells', 0)[v])): next_topomesh.wisp_property('barycenter', 0)[v] for v in next_element_vertices}
    element_ids = set(previous_element_medians.keys()) & set(next_element_medians.keys())
    msg = "Found {} common ids between T_n (n={}) and T_n+1 (n={}) element median!"
    print(msg.format(len(element_ids), len(previous_element_medians), len(next_element_medians)))
    previous_points.update({k: v for k, v in previous_element_medians.items() if k in element_ids})
    next_points.update({k: v for k, v in next_element_medians.items() if k in element_ids})

    print("\n# - Computing cells 3D deformation estimators...")
    # - Deformation estimators:

    growth_data = {}
    for field in ['strain_tensor','stretch_tensor','volumetric_growth','volumetric_growth_anisotropy','main_growth_direction','volume_ratio']:
        growth_data[field] = {}

    #previous_slice = labelled_image_projection(previous_seg_img,direction=-1)
    #next_slice = labelled_image_projection(next_seg_img,direction=-1)

    #for c_id, landmarks in landmark_3d.items():
    for c_id in set(previous_cells) & set(next_cells):
        previous_landmark_keys = [k for k in previous_points.keys() if c_id in k]
        next_landmark_keys = [k for k in next_points.keys() if c_id in k]
        landmark_keys = set(previous_landmark_keys) & set(next_landmark_keys)

        previous_landmarks = []
        next_landmarks = []

        # - If less than 4 landmarks for a cell, do not compute projection:
        if len(landmark_keys) <= 5:
            print("Cell {} has less than 5 landmarks!".format(c_id))
            continue

        for k in landmark_keys:
            previous_landmarks += [previous_points[k] - previous_cell_centers[c_id]]
            next_landmarks += [next_points[k] - next_cell_centers[c_id]]

        #regr = linear_model.Ridge(alpha=1e-5, fit_intercept=False)
        regr = linear_model.LinearRegression(fit_intercept=False)
        regr.fit(previous_landmarks, next_landmarks)

        strain_matrix = regr.coef_
        #regr_eval, regr_evec = np.linalg.eigh(np.dot(regr.coef_.T,regr.coef_))
        #strain_matrix = np.dot(regr_evec,np.dot(np.diag(np.sqrt(regr_eval)),regr_evec.T))


        #previous_base, strain_values, transform_base = np.linalg.svd(strain_matrix)
        transform_base, strain_values, previous_base = np.linalg.svd(strain_matrix)
        strain_matrix = np.dot(previous_base.T, np.dot(np.diag(strain_values), previous_base))

        growth_data['strain_tensor'][c_id] = strain_matrix
        growth_data['stretch_tensor'][c_id] = strain_matrix - np.diag(np.ones(3))

        growth_data['volumetric_growth'][c_id] = np.prod(strain_values)
        #growth_data['volumetric_growth_anisotropy'][c_id] = np.power((3. / 2.) * np.power(strain_values - strain_values.mean(), 2).sum() / np.power(strain_values, 2).sum(), 1 / 2.)
        growth_data['volumetric_growth_anisotropy'][c_id] = np.linalg.norm(strain_values - np.mean(strain_values)) / np.linalg.norm([np.mean(strain_values) for _ in range(3)])
        growth_data['main_growth_direction'][c_id] = strain_values[0] * previous_base[:,0]
        
        growth_data['volume_ratio'][c_id] = next_cell_volumes[c_id]/previous_cell_volumes[c_id]

    return growth_data


def topomesh_cell_surfacic_growth_data(previous_topomesh, next_topomesh, dimensions=[2,1,0]):

    print("\n# - Extracting points of interest:")
    # - Extract Points Of Interest:
    previous_points, next_points = {}, {}

    compute_topomesh_property(previous_topomesh,'cells',0)
    compute_topomesh_property(previous_topomesh,'epidermis',0)
    compute_topomesh_property(next_topomesh,'cells',0)
    compute_topomesh_property(next_topomesh,'epidermis',0)

    compute_topomesh_property(previous_topomesh,'barycenter',3)
    compute_topomesh_property(previous_topomesh,'epidermis',3)
    compute_topomesh_property(previous_topomesh,'vertices',3)
    compute_topomesh_property(previous_topomesh, 'area', 2)
    compute_topomesh_cell_property_from_faces(previous_topomesh, 'area', weighting='uniform', reduce='sum')

    compute_topomesh_property(next_topomesh,'barycenter',3)
    compute_topomesh_property(next_topomesh,'epidermis',3)
    compute_topomesh_property(next_topomesh,'vertices',3)
    compute_topomesh_property(next_topomesh, 'length', 1)
    compute_topomesh_property(next_topomesh, 'area', 2)
    compute_topomesh_cell_property_from_faces(next_topomesh, 'area', weighting='uniform', reduce='sum')

    previous_l1_cells = [c for c in previous_topomesh.wisps(3) if previous_topomesh.wisp_property('epidermis',3)[c]]
    next_l1_cells = [c for c in next_topomesh.wisps(3) if next_topomesh.wisp_property('epidermis',3)[c]]

    print("\n# - Computing wall centers:")
    previous_wall_vertex = {c:[v for v in previous_topomesh.wisp_property('vertices',3)[c] if previous_topomesh.wisp_property('dimension', 0)[v]==2][0] for c in previous_l1_cells}
    previous_l1_cell_centers = {c:previous_topomesh.wisp_property('barycenter',0)[previous_wall_vertex[c]] for c in previous_l1_cells}
    #previous_l1_cell_centers = {c:previous_topomesh.wisp_property('barycenter',3)[c] for c in previous_l1_cells}
    next_wall_vertex = {c:[v for v in next_topomesh.wisp_property('vertices',3)[c] if next_topomesh.wisp_property('dimension', 0)[v]==2][0] for c in next_l1_cells}
    next_l1_cell_centers = {c:next_topomesh.wisp_property('barycenter',0)[next_wall_vertex[c]] for c in next_l1_cells}
    #next_l1_cell_centers = {c:previous_topomesh.wisp_property('barycenter',3)[c] for c in previous_l1_cells}

    previous_areas = {c:previous_topomesh.wisp_property('area', 3)[c] if c in previous_topomesh.wisps(3) else np.nan for c in previous_l1_cells}
    next_areas = {c:next_topomesh.wisp_property('area', 3)[c] if c in next_topomesh.wisps(3) else np.nan for c in next_l1_cells}

    previous_element_vertices = [v for v in previous_topomesh.wisps(0) if previous_topomesh.wisp_property('dimension', 0)[v] in dimensions and previous_topomesh.wisp_property('epidermis', 0)[v]]
    previous_element_medians = {tuple(list(previous_topomesh.wisp_property('cells', 0)[v])): previous_topomesh.wisp_property('barycenter', 0)[v] for v in previous_element_vertices}
    next_element_vertices = [v for v in next_topomesh.wisps(0) if next_topomesh.wisp_property('dimension', 0)[v] in dimensions and next_topomesh.wisp_property('epidermis', 0)[v]]
    next_element_medians = {tuple(list(next_topomesh.wisp_property('cells', 0)[v])): next_topomesh.wisp_property('barycenter', 0)[v] for v in next_element_vertices}
    element_ids = set(previous_element_medians.keys()) & set(next_element_medians.keys())
    msg = "Found {} common ids between T_n (n={}) and T_n+1 (n={}) element median!"
    print(msg.format(len(element_ids), len(previous_element_medians), len(next_element_medians)))
    previous_points.update({k: v for k, v in previous_element_medians.items() if k in element_ids})
    next_points.update({k: v for k, v in next_element_medians.items() if k in element_ids})

    print("\n# - Computing L1 cells 2D deformation estimators...")
    # - Deformation estimators:

    growth_data = {c:{} for c in ['surfacic_strain_tensor','surfacic_stretch_tensor','surfacic_growth',
                                  'surfacic_growth_anisotropy',
                                  'main_surfacic_growth_direction','area_ratio']}

    if 'division_direction' in next_topomesh.wisp_property_names(3):
        growth_data['division_direction'] = {}

    for c_id in set(previous_l1_cells) & set(next_l1_cells):
        previous_landmark_keys = [k for k in previous_points.keys() if (c_id in k)]
        next_landmark_keys = [k for k in next_points.keys() if (c_id in k)]
        landmark_keys = set(previous_landmark_keys) & set(next_landmark_keys)

        previous_landmarks = []
        next_landmarks = []

        # - If less than 3 landmarks for a cell, do not compute projection:
        if len(landmark_keys) <= 4:
            print("Cell {} has less than 4 landmarks!".format(c_id))
            continue

        for k in landmark_keys:
            previous_landmarks += [previous_points[k] - previous_l1_cell_centers[c_id]]
            next_landmarks += [next_points[k] - next_l1_cell_centers[c_id]]

        if 'division_direction' in next_topomesh.wisp_property_names(3):
            next_division_direction = next_topomesh.wisp_property('division_direction',3)[c_id]

            #regr = linear_model.Ridge(alpha=1e-5, fit_intercept=False)
            regr = linear_model.LinearRegression(fit_intercept=True)
            regr.fit(previous_landmarks, next_landmarks)

            strain_matrix = regr.coef_
            next_base, strain_values, previous_base = np.linalg.svd(strain_matrix)
            #previous_base, strain_values, next_base = np.linalg.svd(strain_matrix)
            previous_division_direction = np.dot(previous_base.T, np.dot(next_base.T, next_division_direction))
            previous_division_direction /= np.linalg.norm(previous_division_direction)
            growth_data['division_direction'][c_id] = previous_division_direction

        previous_cov = np.cov(previous_landmarks,rowvar=False)
        previous_eval, previous_evec = np.linalg.eigh(previous_cov)
        previous_normal = previous_evec[:, np.argmin(np.abs(previous_eval))]

        previous_proj_x_axis = np.cross([0,1,0],previous_normal)
        if np.linalg.norm(previous_proj_x_axis) < 1e-1:
            previous_proj_x_axis = np.cross([1,0,0],previous_normal)
        previous_proj_x_axis /= np.linalg.norm(previous_proj_x_axis)
        previous_proj_y_axis = np.cross(previous_normal,previous_proj_x_axis)
        previous_proj_y_axis /= np.linalg.norm(previous_proj_y_axis)

        previous_base = np.array([previous_proj_x_axis,previous_proj_y_axis,previous_normal])
        previous_projected_landmarks = np.einsum("...ij,...j->...i",previous_base,previous_landmarks)[:,:2]

        next_cov = np.cov(next_landmarks, rowvar=False)
        next_eval, next_evec = np.linalg.eigh(next_cov)
        next_normal = next_evec[:, np.argmin(np.abs(next_eval))]
        if np.dot(previous_normal,next_normal)<0:
            next_normal *= -1

        next_proj_x_axis = np.cross([0, 1, 0], next_normal)
        if np.linalg.norm(next_proj_x_axis) < 1e-1:
            next_proj_x_axis = np.cross([1,0,0],next_normal)
        next_proj_x_axis /= np.linalg.norm(next_proj_x_axis)
        next_proj_y_axis = np.cross(next_normal, next_proj_x_axis)
        next_proj_y_axis /= np.linalg.norm(next_proj_y_axis)

        next_base = np.array([next_proj_x_axis, next_proj_y_axis, next_normal])
        next_projected_landmarks = np.einsum("...ij,...j->...i", next_base, next_landmarks)[:, :2]

        #regr = linear_model.Ridge(alpha=1e-5, fit_intercept=False)
        regr = linear_model.LinearRegression(fit_intercept=False)
        regr.fit(previous_projected_landmarks, next_projected_landmarks)
        projected_strain_matrix = np.eye(3)

        #regr_eval, regr_evec = np.linalg.eigh(np.dot(regr.coef_.T,regr.coef_))
        #projected_strain_matrix[:2,:2] = np.dot(regr_evec.T, np.dot(np.diag(np.sqrt(regr_eval)), regr_evec))

        next_proj_base, proj_strain_values, previous_proj_base = np.linalg.svd(regr.coef_)
        projected_strain_matrix[:2, :2] = np.dot(previous_proj_base.T, np.dot(np.diag(proj_strain_values), previous_proj_base))

        projected_strain_matrix[2, 2] = 0

        strain_matrix = np.dot(previous_base.T, np.dot(projected_strain_matrix, previous_base))

        growth_data['surfacic_strain_tensor'][c_id] = strain_matrix
        identity_matrix = np.dot(previous_base.T, np.dot(np.diag([1,1,0]), previous_base))
        growth_data['surfacic_stretch_tensor'][c_id] = strain_matrix - identity_matrix

        previous_base, strain_values, next_base = np.linalg.svd(strain_matrix)
        strain_values = strain_values[:-1]

        growth_data['surfacic_growth'][c_id] = np.prod(strain_values)
        #growth_data['surfacic_growth_anisotropy'][c_id] = np.sqrt(2) * np.linalg.norm(strain_values - np.mean(strain_values)) / np.linalg.norm(strain_values)
        growth_data['surfacic_growth_anisotropy'][c_id] = np.linalg.norm(strain_values - np.mean(strain_values)) / np.linalg.norm ([np.mean(strain_values) for _ in range(2)])
        growth_data['main_surfacic_growth_direction'][c_id] = strain_values[0] * previous_base[:,0]

        growth_data['area_ratio'][c_id] = next_areas[c_id]/previous_areas[c_id]

    return growth_data


def segmentation_growth_estimation(sequence_segmented_images, sequence_rigid_transforms, sequence_vectorfield_transforms, sequence_element_topomesh={}, growth_type='volumetric', resampling_voxelsize=None):
    """
    """

    filenames = np.sort(list(sequence_segmented_images.keys()))
    print(filenames)

    previous_transform = create_trsf()

    sequence_growth_data = {}

    for i_file, (reference_filename, floating_filename) in enumerate(zip(filenames[:-1], filenames[1:])):

        rigid_matrix = sequence_rigid_transforms[(reference_filename,floating_filename)]
        rigid_transform = create_trsf(param_str_2='-identity', trsf_type=TRSF_TYPE_DICT['RIGID_3D'], trsf_unit=TRSF_UNIT_DICT['REAL_UNIT'])
        allocate_c_bal_matrix(rigid_transform.mat.c_struct, rigid_matrix)

        invert_rigid_matrix = sequence_rigid_transforms[(floating_filename,reference_filename)]
        invert_rigid_transform = create_trsf(param_str_2='-identity', trsf_type=TRSF_TYPE_DICT['RIGID_3D'], trsf_unit=TRSF_UNIT_DICT['REAL_UNIT'])
        allocate_c_bal_matrix(invert_rigid_transform.mat.c_struct, invert_rigid_matrix)

        vector_field_transform = sequence_vectorfield_transforms[(reference_filename,floating_filename)]
        invert_vector_field_transform = sequence_vectorfield_transforms[(floating_filename,reference_filename)]

        # reference_seg_img = sequence_segmented_images[reference_filename]

        # invert_registered_seg_img = apply_trsf(reference_seg_img, vector_field_transform, param_str_1='-nearest')
        # invert_registered_seg_img = apply_trsf(reference_seg_img, vector_field_transform, param_str_1='-nearest')
        # invert_registered_seg_img[invert_registered_seg_img==0]=1

        reference_seg_img = LabelledImage(sequence_segmented_images[reference_filename], no_label_id=0)
        floating_seg_img = LabelledImage(sequence_segmented_images[floating_filename], no_label_id=0)
        # invert_registered_seg_img = TissueImage(invert_registered_seg_img, background=1, no_label_id=0)

        # print("--> Creating tissue analysis for",reference_filename)
        # reference_tissue = TissueAnalysis(reference_seg_img, auto_init=False)
        reference_labels = [l for l in reference_seg_img.labels() if l!=1]
        floating_labels = [l for l in floating_seg_img.labels() if l!=1]
        # reference_cell_centers = reference_tissue.label.center_of_mass(real=True)
        # reference_l1_cells = reference_tissue.label.list_epidermal_labels_from_neighbors()
        # #reference_l1_cells = reference_seg_img.neighbors(1)
        # reference_layer = np.array([1 if c in reference_l1_cells else 2 for c in reference_labels])
        #
        # print("--> Creating tissue analysis for",floating_filename)
        # invert_registered_tissue = TissueAnalysis(invert_registered_seg_img, auto_init=False)
        # invert_registered_l1_cells = invert_registered_tissue.label.list_epidermal_labels_from_neighbors()

        if not reference_filename in sequence_element_topomesh.keys():
            print("--> Extracting surfacic element complex", reference_filename)
            reference_element_topomesh = segmentation_topological_element_cell_topomesh(reference_seg_img,
                                                                                        dimension=2 if growth_type=='surfacic' else 3,
                                                                                        resampling_voxelsize=resampling_voxelsize,
                                                                                        fuse_vertices= growth_type=='surfacic')
        else:
            reference_element_topomesh = sequence_element_topomesh[reference_filename]

        # print("--> Extracting surfacic element complex", floating_filename)
        # invert_registered_element_topomesh = segmentation_topological_element_cell_topomesh(invert_registered_seg_img,
        #                                                                                     dimension=2 if growth_type=='surfacic' else 3,
        #                                                                                     resampling_voxelsize=resampling_voxelsize,
        #                                                                                     fuse_vertices= growth_type=='surfacic')
        print("--> Applying deformation to surfacic element complex", floating_filename)
        invert_registered_element_topomesh = topomesh_apply_vectorfield_transformation(reference_element_topomesh, vector_field_transform)

        # ply_filename = image_dirname + "/" + sequence_name + "/" + reference_filename + "/" + reference_filename + "_to_" + floating_filename[-3:] + ("_l1" if growth_type == 'surfacic' else "") + "_grifone_topomesh.ply"
        # save_ply_property_topomesh(invert_registered_element_topomesh,ply_filename)

        # if growth_type == 'volumetric':
        #     reference_volumes = reference_tissue.label.volume(real=True)
        #     reference_volumes = np.array([reference_volumes[c] for c in reference_labels])
            # invert_registered_volumes = invert_registered_tissue.label.volume(real=True)
            # invert_registered_volumes = np.array([invert_registered_volumes[c]
            #                                       if c in invert_registered_seg_img.cells() else np.nan
            #                                       for c in reference_labels])
            # next_volume_ratios = invert_registered_volumes/reference_volumes

            # reference_next_volume_ratios = dict(zip(reference_labels, next_volume_ratios))
            # reference_element_topomesh.update_wisp_property('next_image_volume_ratio', 3, {c:reference_next_volume_ratios[c] if c in reference_labels else np.nan for c in reference_element_topomesh.wisps(3)})

        # elif growth_type == 'surfacic':

            # print("--> Computing image surface areas for", reference_filename)
            # reference_surface_areas = {}
            # reference_tissue.wall._kernels = None
            # reference_tissue.wall._property['area'] = {}
            # reference_tissue.wall._property['area_voxel'] = {}
            # for l in reference_l1_cells:
            #     reference_surface_areas[l] = reference_tissue.wall.wall_area_from_label_neighbors(l, [1])[(1, l)]
            #
            # print("--> Computing image surface areas for", floating_filename)
            # invert_registered_surface_areas = {}
            # invert_registered_tissue.wall._kernels = None
            # invert_registered_tissue.wall._property['area'] = {}
            # invert_registered_tissue.wall._property['area_voxel'] = {}
            # for l in invert_registered_l1_cells:
            #     invert_registered_surface_areas[l] = invert_registered_tissue.wall.wall_area_from_label_neighbors(l, [1])[(1, l)]
            #
            # reference_areas = np.array([reference_surface_areas[l] if l in reference_l1_cells else np.nan for l in reference_labels])
            # invert_registered_areas = np.array([invert_registered_surface_areas[l] if l in invert_registered_l1_cells else np.nan for l in reference_labels])
            # next_area_ratios = invert_registered_areas/reference_areas

            # compute_topomesh_property(reference_element_topomesh, 'area', 2)
            # compute_topomesh_cell_property_from_faces(reference_element_topomesh, 'area', weighting='uniform', reduce='sum')
            #
            # compute_topomesh_property(invert_registered_element_topomesh, 'area', 2)
            # compute_topomesh_cell_property_from_faces(invert_registered_element_topomesh, 'area', weighting='uniform', reduce='sum')

            # reference_mesh_areas = np.array([reference_element_topomesh.wisp_property('area',3)[l] if l in reference_element_topomesh.wisps(3) else np.nan for l in reference_labels])
            # invert_registered_mesh_areas = np.array([invert_registered_element_topomesh.wisp_property('area', 3)[l] if l in invert_registered_element_topomesh.wisps(3) else np.nan for l in reference_labels])
            # next_mesh_area_ratios = invert_registered_mesh_areas/reference_mesh_areas

            # reference_next_area_ratios = dict(zip(reference_labels, next_area_ratios))
            # reference_element_topomesh.update_wisp_property('next_image_area_ratio', 3, {c:reference_next_area_ratios[c] if c in reference_labels else np.nan for c in reference_element_topomesh.wisps(3)})
            #
            # reference_next_mesh_area_ratios = dict(zip(reference_labels, next_mesh_area_ratios))
            # reference_element_topomesh.update_wisp_property('next_area_ratio', 3, {c:reference_next_mesh_area_ratios[c] if c in reference_labels else np.nan for c in reference_element_topomesh.wisps(3)})

        print("--> Computing",growth_type,"growth",reference_filename,"->",floating_filename)
        if growth_type == 'volumetric':
            next_growth_data = topomesh_cell_volumetric_growth_data(reference_element_topomesh, invert_registered_element_topomesh)
        elif growth_type == 'surfacic':
            next_growth_data = topomesh_cell_surfacic_growth_data(reference_element_topomesh, invert_registered_element_topomesh)

        for col in next_growth_data.keys():
            default_value = np.nan
            if 'tensor' in col:
                default_value = np.nan * np.ones((3,3))
            elif 'direction' in col:
                default_value = np.nan * np.ones(3)
            reference_element_topomesh.update_wisp_property('next_' + col, 3, {c:next_growth_data[col][c] if c in next_growth_data[col] else default_value for c in reference_element_topomesh.wisps(3)})

        sequence_element_topomesh[reference_filename] = reference_element_topomesh

        if not reference_filename in sequence_growth_data:
            sequence_growth_data[reference_filename] = pd.DataFrame({'label':reference_labels})
        # for i_dim,dim in enumerate(['x','y','z']):
        #     sequence_growth_data[reference_filename]['cell_center_'+dim] = [reference_cell_centers[c][i_dim] for c in reference_labels]
        #sequence_growth_data[reference_filename]['layer'] = reference_layer

        # if growth_type == 'volumetric':
        #     #sequence_growth_data[reference_filename]['volume'] = reference_volumes
        #     sequence_growth_data[reference_filename]['next_volume_ratio'] = next_volume_ratios
        # elif growth_type == 'surfacic':
        #     #sequence_growth_data[reference_filename]['area'] = reference_areas
        #     #sequence_growth_data[reference_filename]['next_area_ratio'] = next_area_ratios
        #     sequence_growth_data[reference_filename]['next_area_ratio'] = next_mesh_area_ratios

        for col in next_growth_data.keys():
            sequence_growth_data[reference_filename]['next_'+col] = [next_growth_data[col][c] if c in next_growth_data[col] else np.nan for c in reference_labels]

        if i_file == len(filenames)-2:
            sequence_growth_data[floating_filename] = pd.DataFrame({'label':floating_labels})
            sequence_growth_data[floating_filename]['next_volume_ratio'] = np.nan
            for col in next_growth_data.keys():
                sequence_growth_data[floating_filename]['next_' + col] = np.nan

    return sequence_growth_data, sequence_element_topomesh



