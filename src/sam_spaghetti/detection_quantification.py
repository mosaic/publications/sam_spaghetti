import sys
import os
import logging
from time import time as current_time

import numpy as np
import scipy.ndimage as nd
import pandas as pd

from tissue_nukem_3d.microscopy_images import imread
from tissue_nukem_3d.microscopy_images.read_microscopy_image import read_czi_image, read_lsm_image, read_tiff_image

from tissue_nukem_3d.nuclei_image_topomesh import nuclei_image_topomesh

from tissue_nukem_3d.utils.matplotlib_tools import view_image_projection

from timagetk.components import LabelledImage
from timagetk.io import imsave
from timagetk.plugins import labels_post_processing

from cellcomplex.property_topomesh.creation import vertex_topomesh
from cellcomplex.property_topomesh.io import save_ply_property_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_vertex_property_from_faces
from cellcomplex.property_topomesh.extraction import property_filtering_sub_topomesh, topomesh_connected_components, topomesh_connected_wisps, sub_topomesh
from cellcomplex.property_topomesh.morphology import topomesh_binary_property_morphological_operation, topomesh_binary_property_fill_holes

from cellcomplex.property_topomesh.utils.pandas_tools import topomesh_to_dataframe

from sam_spaghetti.sam_sequence_loading import load_sequence_signal_data, load_sequence_segmented_images, load_sequence_surface_meshes
from sam_spaghetti.segmentation_quantification import segmented_image_layer_curvature_data



# channel_compute_ratios = ['DIIV']
channel_compute_ratios = []


def detect_and_quantify(img_dict, reference_name='TagBFP', signal_names=None, compute_ratios=None, save_files=True, image_dirname=None, nomenclature_name=None, microscope_orientation=-1, surface_voxelsize=1., verbose=True, debug=False, loglevel=0):
    """
    """
    logging.getLogger().setLevel(logging.INFO if verbose else logging.DEBUG if debug else logging.ERROR)

    sequence_name = nomenclature_name[:-4]
    
    if signal_names is None:
        signal_names = img_dict.keys()

    if compute_ratios is None:
        compute_ratios = [signal_name in channel_compute_ratios for signal_name in signal_names]

    logging.info("".join(["  " for l in range(loglevel)])+"--> Detecting and quantifying")
    # topomesh = nuclei_image_topomesh(nomenclature_names[nomenclature_name],dirname=image_dirname,reference_name=reference_name,signal_names=signal_names,compute_ratios=compute_ratios,redetect=redetect, recompute=recompute,subsampling=4)
    threshold = 16 if img_dict[reference_name].dtype==np.uint8 else 3000
    topomesh, surface_topomesh = nuclei_image_topomesh(img_dict, reference_name=reference_name, signal_names=signal_names, compute_ratios=compute_ratios, microscope_orientation=microscope_orientation, radius_range=(0.8,1.4), threshold=threshold, nuclei_sigma=0.75, surface_mode='image', surface_voxelsize=surface_voxelsize, return_surface=True)

    if save_files:
        topomesh_file = image_dirname+"/"+sequence_name+"/"+nomenclature_name+"/"+nomenclature_name+"_nuclei_signal_curvature_topomesh.ply"
        logging.info("".join(["  " for l in range(loglevel)])+"  --> Saving detected nuclei")
        save_ply_property_topomesh(topomesh,topomesh_file,properties_to_save=dict([(0,signal_names+['layer','mean_curvature','gaussian_curvature']),(1,[]),(2,[]),(3,[])]))

        surface_file = image_dirname+"/"+sequence_name+"/"+nomenclature_name+"/"+nomenclature_name+"_surface_topomesh.ply"
        logging.info("".join(["  " for l in range(loglevel)])+"  --> Saving surface mesh")
        save_ply_property_topomesh(surface_topomesh,surface_file,properties_to_save={0: ['mean_curvature', 'gaussian_curvature', 'principal_curvature_min', 'principal_curvature_max'],
                                                                                     1: [],
                                                                                     2: ['area', 'mean_curvature', 'gaussian_curvature', 'principal_curvature_min', 'principal_curvature_max'],
                                                                                     3: []})

    df = topomesh_to_dataframe(topomesh,0)
    if save_files:
        logging.info("".join(["  " for l in range(loglevel)])+"  --> Saving nuclei data")
        if ('DIIV' in df.columns)&(reference_name in df.columns):
            df['qDII'] = df['DIIV'].values/df[reference_name].values
        if ('RGAV' in df.columns)&(reference_name in df.columns):
            df['qRGA'] = df['RGAV'].values/df[reference_name].values

        df['label'] = df.index.values
        df.to_csv(image_dirname+"/"+sequence_name+"/"+nomenclature_name+"/"+nomenclature_name+"_signal_data.csv",index=False)  

    results = (df, topomesh, img_dict, surface_topomesh)

    return results


def detect_from_segmentation_and_quantify(img_dict, seg_img, reference_name='TagBFP', signal_names=None, compute_ratios=None, save_files=True, image_dirname=None, nomenclature_name=None, microscope_orientation=-1, surface_voxelsize=1., surface_matching='vertex', verbose=True, debug=False, loglevel=0):

    sequence_name = nomenclature_name[:-4]

    if not isinstance(seg_img, LabelledImage):
        seg_img = LabelledImage(seg_img, no_label_id=0)

    if signal_names is None:
        signal_names = img_dict.keys()

    if compute_ratios is None:
        compute_ratios = [signal_name in channel_compute_ratios for signal_name in signal_names]

    logging.info("".join(["  " for l in range(loglevel)])+"--> Detecting and quantifying")
    threshold = 16 if img_dict[reference_name].dtype==np.uint8 else 3000
    topomesh = nuclei_image_topomesh(img_dict, segmented_img=seg_img,
                                               reference_name=reference_name, signal_names=signal_names, compute_ratios=compute_ratios,
                                               microscope_orientation=microscope_orientation, radius_range=(0.8, 1.4), threshold=threshold, nuclei_sigma=0.75,
                                               compute_layer=False, compute_curvature=False, return_surface=False)

    cell_df, surface_topomesh = segmented_image_layer_curvature_data(seg_img,resampling_voxelsize=surface_voxelsize,
                                                                     microscope_orientation=microscope_orientation,
                                                                     surface_matching=surface_matching, return_surface=True)

    for property_name in ['layer','mean_curvature','gaussian_curvature']:
        topomesh.update_wisp_property(property_name,0,dict(zip(cell_df['label'].values,cell_df[property_name].values)))

    if save_files:
        topomesh_file = image_dirname + "/" + sequence_name + "/" + nomenclature_name + "/" + nomenclature_name + "_nuclei_signal_curvature_topomesh.ply"
        logging.info("".join(["  " for l in range(loglevel)])+"  --> Saving detected nuclei")
        save_ply_property_topomesh(topomesh, topomesh_file, properties_to_save=dict([(0, signal_names + ['layer', 'mean_curvature', 'gaussian_curvature']), (1, []), (2, []), (3, [])]))

        surface_file = image_dirname + "/" + sequence_name + "/" + nomenclature_name + "/" + nomenclature_name + "_surface_topomesh.ply"
        logging.info("".join(["  " for l in range(loglevel)])+"  --> Saving surface mesh")
        save_ply_property_topomesh(surface_topomesh, surface_file, properties_to_save=dict([(0, ['mean_curvature', 'gaussian_curvature']), (1, []), (2, []), (3, [])]))

    df = topomesh_to_dataframe(topomesh, 0)
    for col in cell_df.columns:
        cell_dict = dict(zip(cell_df['label'].values,cell_df[col].values))
        df[col] = [cell_dict[c] for c in df.index]

    if save_files:
        logging.info("".join(["  " for l in range(loglevel)])+"  --> Saving nuclei data")
        if ('DIIV' in df.columns) & (reference_name in df.columns):
            df['qDII'] = df['DIIV'].values / df[reference_name].values
        if ('RGAV' in df.columns) & (reference_name in df.columns):
            df['qRGA'] = df['RGAV'].values / df[reference_name].values

        df.to_csv(image_dirname + "/" + sequence_name + "/" + nomenclature_name + "/" + nomenclature_name + "_cell_data.csv", index=False)

    results = (df, topomesh, img_dict, surface_topomesh)

    return results


def quantify_from_segmentation(seg_img, save_files=True, image_dirname=None, nomenclature_name=None, microscope_orientation=-1, surface_voxelsize=1., surface_matching='vertex', verbose=True, debug=False, loglevel=0):

    sequence_name = nomenclature_name[:-4]

    if not isinstance(seg_img, LabelledImage):
        seg_img = LabelledImage(seg_img, no_label_id=0)

    cell_df, surface_topomesh = segmented_image_layer_curvature_data(seg_img,resampling_voxelsize=surface_voxelsize,
                                                                     microscope_orientation=microscope_orientation,
                                                                     surface_matching=surface_matching,
                                                                     return_surface=True)

    cell_positions = dict(zip(cell_df['label'].values, microscope_orientation*cell_df['cell_center'].values))
    topomesh = vertex_topomesh(cell_positions)

    for property_name in ['label','layer','mean_curvature','gaussian_curvature']:
        topomesh.update_wisp_property(property_name,0,dict(zip(cell_df['label'].values,cell_df[property_name].values)))

    if save_files:
        topomesh_file = image_dirname + "/" + sequence_name + "/" + nomenclature_name + "/" + nomenclature_name + "_nuclei_signal_curvature_topomesh.ply"
        logging.info("".join(["  " for l in range(loglevel)])+"  --> Saving detected cells")
        save_ply_property_topomesh(topomesh, topomesh_file, properties_to_save=dict([(0, ['layer', 'mean_curvature', 'gaussian_curvature']), (1, []), (2, []), (3, [])]))

        surface_file = image_dirname + "/" + sequence_name + "/" + nomenclature_name + "/" + nomenclature_name + "_surface_topomesh.ply"
        logging.info("".join(["  " for l in range(loglevel)])+"  --> Saving surface mesh")
        save_ply_property_topomesh(surface_topomesh, surface_file, properties_to_save=dict([(0, ['cell', 'mean_curvature', 'gaussian_curvature', 'principal_curvature_min', 'principal_curvature_max']),
                                                                                            (1, []),
                                                                                            (2, ['area', 'mean_curvature', 'gaussian_curvature', 'principal_curvature_min', 'principal_curvature_max']),
                                                                                            (3, [])]))

    df = topomesh_to_dataframe(topomesh, 0)
    for col in cell_df.columns:
        cell_dict = dict(zip(cell_df['label'].values,cell_df[col].values))
        df[col] = [cell_dict[c] for c in df.index]

    if save_files:
        logging.info("".join(["  " for l in range(loglevel)])+"  --> Saving cell data")

        df.to_csv(image_dirname + "/" + sequence_name + "/" + nomenclature_name + "/" + nomenclature_name + "_cell_data.csv", index=False)

    results = (df, topomesh, surface_topomesh)

    return results


def quantify_cell_signal(seg_img, img_dict, signal_names=None, quantification_erosion_radius=0.4, save_files=True, image_dirname=None, nomenclature_name=None, microscope_orientation=-1, verbose=True, debug=False, loglevel=0):

    sequence_name = nomenclature_name[:-4]
    data_filename = image_dirname + "/" + sequence_name + "/" + nomenclature_name + "/" + nomenclature_name + "_cell_data.csv"
    cell_df = pd.read_csv(data_filename)

    cell_labels = cell_df['label'].values

    if len(signal_names) > 0:
        erosion_radius = int(np.ceil(np.mean([quantification_erosion_radius/v for v in seg_img.voxelsize])))
        eroded_seg_img = labels_post_processing(seg_img, method='erosion', radius=erosion_radius, iterations=1)

        for i_s, signal_name in enumerate(signal_names):
            logging.info("".join(["  " for l in range(loglevel)])+"  --> Quantifying {} signal".format(signal_name))
            signal_img = img_dict[signal_name]

            cell_signals = nd.mean(signal_img.get_array().astype(float),
                                   eroded_seg_img.get_array(),
                                   index=cell_labels)

            cell_df[signal_name] = cell_signals

    if save_files:
        logging.info("".join(["  " for l in range(loglevel)])+"  --> Saving cell data")

        cell_df.to_csv(data_filename, index=False)


def cell_binary_property_morphological_operation(cell_df, property_name, method='erosion', iterations=1, layer_restriction=1):
    from cellcomplex.utils import array_dict

    assert 'neighbors' in cell_df.columns
    assert property_name in cell_df.columns

    cell_labels = list(cell_df['label'].values)
    cell_neighbors = dict(zip(cell_labels,[eval(str(n)) for n in cell_df['neighbors'].values]))

    if layer_restriction is not None:
        assert 'layer' in cell_df.columns
        cell_layer = dict(zip(cell_labels,cell_df['layer'].values))
        layer_labels = [l for l in cell_labels if cell_layer[l] == layer_restriction]
    else:
        layer_labels = cell_labels


    label_neighbors = [n for c in cell_labels for n in cell_neighbors[c] if n in layer_labels]
    label_neighbor_labels = [c for c in cell_labels for n in cell_neighbors[c] if n in layer_labels]

    for iteration in range(iterations):
        cell_property = array_dict(dict(zip(cell_labels,cell_df[property_name].values)))
        label_neighbor_properties = cell_property.values(cell_labels + label_neighbors)
        if method == 'erosion':
            morpho_property = nd.minimum(label_neighbor_properties != 0, cell_labels + label_neighbor_labels, index=cell_labels).astype(int)
        elif method == 'dilation':
            morpho_property = nd.maximum(label_neighbor_properties != 0, cell_labels + label_neighbor_labels, index=cell_labels).astype(int)
        elif method == 'opening':
            morpho_property = nd.minimum(label_neighbor_properties != 0, cell_labels + label_neighbor_labels, index=cell_labels).astype(int)
            label_neighbor_properties = array_dict(morpho_property, keys=cell_labels).values(cell_labels + label_neighbors)
            morpho_property = nd.maximum(label_neighbor_properties != 0, cell_labels + label_neighbor_labels, index=cell_labels).astype(int)
        elif method == 'closing':
            morpho_property = nd.maximum(label_neighbor_properties != 0, cell_labels + label_neighbor_labels, index=cell_labels).astype(int)
            label_neighbor_properties = array_dict(morpho_property, keys=cell_labels).values(cell_labels + label_neighbors)
            morpho_property = nd.minimum(label_neighbor_properties != 0, cell_labels + label_neighbor_labels, index=cell_labels).astype(int)
        else:
            morpho_property = cell_property.values(cell_labels)

        if layer_restriction is not None:
            morpho_property *= (array_dict(cell_layer).values(cell_labels) == layer_restriction).astype(int)

        cell_df[property_name] = morpho_property


def meristem_delineation(cell_df, surface_topomesh, seg_img=None, cell_labels=None, min_curvature_threshold=-5e-3):

    if cell_labels is None:
        if seg_img is None:
            cell_labels = cell_df['label'].values
        else:
            cell_labels = np.unique(seg_img.get_array())[1:]

    compute_topomesh_property(surface_topomesh, 'area', 2)
    compute_topomesh_property(surface_topomesh, 'barycenter', 2)
    if not 'principal_curvature_min' in surface_topomesh.wisp_property_names(2):
        compute_topomesh_property(surface_topomesh, 'normal', 2, normal_method='orientation')
        compute_topomesh_vertex_property_from_faces(surface_topomesh, 'normal', neighborhood=3, adjacency_sigma=1.2)
        compute_topomesh_property(surface_topomesh, 'mean_curvature', 2)

    surface_topomesh.update_wisp_property('meristem', 2, {f: surface_topomesh.wisp_property('principal_curvature_min', 2)[f] > min_curvature_threshold for f in surface_topomesh.wisps(2)})
    for iteration in range(3):
        topomesh_binary_property_morphological_operation(surface_topomesh, 'meristem', 2, 'dilation', iterations=1, contour_value=0)
        topomesh_binary_property_morphological_operation(surface_topomesh, 'meristem', 2, 'erosion', iterations=1, contour_value=1)
    topomesh_binary_property_fill_holes(surface_topomesh, 'meristem', 2)

    compute_topomesh_property(surface_topomesh, 'faces', 0)
    vertex_face_meristem = surface_topomesh.wisp_property('meristem', 2).values(surface_topomesh.wisp_property('faces', 0).values(list(surface_topomesh.wisps(0))))
    # vertex_meristem = list(map(np.all,vertex_face_meristem))
    vertex_meristem = list(map(lambda m: int(np.mean(m) > 0.5), vertex_face_meristem))
    # vertex_meristem = list(map(np.any,vertex_face_meristem))

    surface_topomesh.update_wisp_property('meristem', 0, dict(zip(surface_topomesh.wisps(0), vertex_meristem)))
    for iteration in range(3):
        topomesh_binary_property_morphological_operation(surface_topomesh, 'meristem', 0, 'dilation', iterations=1, contour_value=0)
        topomesh_binary_property_morphological_operation(surface_topomesh, 'meristem', 0, 'erosion', iterations=1, contour_value=1)

    convex_topomesh = property_filtering_sub_topomesh(surface_topomesh, 'meristem', 0, (1, 1))

    meristem_wisps = topomesh_connected_wisps(convex_topomesh, degree=2)
    meristem_components_areas = np.array([surface_topomesh.wisp_property('area', 2).values(w).sum() for w in meristem_wisps])
    meristem_components_center = np.array([surface_topomesh.wisp_property('barycenter', 2).values(w).mean(axis=0) for w in meristem_wisps])
    if seg_img is None:
        img_center = np.nanmean(cell_df[[f"center_{dim}" for dim in 'xyz']].values, axis=0)
    else:
        img_center = np.array(seg_img.extent)/2.
    meristem_components_scores = np.power(meristem_components_areas, 1 / 2) / np.linalg.norm((meristem_components_center - img_center)[:, :2], axis=1)

    meristem_topomesh = sub_topomesh(surface_topomesh, 2, meristem_wisps[np.argmax(meristem_components_scores)])
    surface_topomesh.update_wisp_property('meristem', 0, {v: int(v in meristem_topomesh.wisps(0)) for v in surface_topomesh.wisps(0)})
    # topomesh_binary_property_morphological_operation(surface_topomesh, 'meristem', 0, 'dilation', iterations=1, contour_value=0)

    vertex_meristem = surface_topomesh.wisp_property('meristem', 0).values(list(surface_topomesh.wisps(0)))
    if surface_topomesh.has_wisp_property('cell', 0, is_computed=True):
        vertex_cells = surface_topomesh.wisp_property('cell', 0).values(list(surface_topomesh.wisps(0)))
    else:
        l1_cell_df = cell_df[cell_df['layer']==1]
        l1_cells = l1_cell_df['label'].values
        l1_cell_centers = l1_cell_df[[f"center_{dim}" for dim in 'xyz']].values

        vertex_points = surface_topomesh.wisp_property('barycenter', 0).values(list(surface_topomesh.wisps(0)))
        vertex_cell_distances = np.linalg.norm(vertex_points[:, np.newaxis] - l1_cell_centers[np.newaxis], axis=-1)
        vertex_cells = l1_cells[np.argmin(vertex_cell_distances, axis=-1)]
        surface_topomesh.update_wisp_property('cell', 0, dict(zip(surface_topomesh.wisps(0), vertex_cells)))

    cell_meristem = (nd.sum(vertex_meristem, vertex_cells, index=cell_labels) > 0).astype(int)

    return cell_meristem


def extract_meristem_cells(sequence_name, image_dirname, nuclei=False, save_files=True, min_curvature_threshold=-5e-3, verbose=False, debug=False, loglevel=0):

    sequence_data = load_sequence_signal_data(sequence_name, image_dirname, nuclei=nuclei)
    if not nuclei:
        sequence_segmented_images = load_sequence_segmented_images(sequence_name, image_dirname)
    sequence_surface_meshes = load_sequence_surface_meshes(sequence_name, image_dirname)

    logging.getLogger().setLevel(logging.INFO if verbose else logging.DEBUG if debug else logging.ERROR)

    for filename in sequence_surface_meshes.keys():
        logging.info("".join(["  " for l in range(loglevel)]) + "  --> Extracting meristem cells : {}".format(filename))

        cell_df = sequence_data[filename]
        cell_labels = cell_df['label'].values

        if nuclei:
            seg_img = None
        else:
            seg_img = sequence_segmented_images[filename]
        surface_topomesh = sequence_surface_meshes[filename]

        cell_meristem = meristem_delineation(cell_df, surface_topomesh, seg_img=seg_img, cell_labels=cell_labels, min_curvature_threshold=min_curvature_threshold)

        cell_df['meristem'] = cell_meristem
        if not nuclei:
            cell_binary_property_morphological_operation(cell_df, 'meristem', method='closing', iterations=3)
            cell_binary_property_morphological_operation(cell_df, 'meristem', method='opening', iterations=3)

        if save_files:
            data_filename = image_dirname + "/" + sequence_name + "/" + filename + "/" + filename + "_"+("signal" if nuclei else "cell")+"_data.csv"
            cell_df.to_csv(data_filename, index=False)

            surface_file = image_dirname + "/" + sequence_name + "/" + filename + "/" + filename + "_surface_topomesh.ply"
            logging.info("".join(["  " for l in range(loglevel)]) + "  --> Saving surface mesh")
            save_ply_property_topomesh(surface_topomesh, surface_file,
                                       properties_to_save=dict([(0, ['cell', 'mean_curvature', 'gaussian_curvature', 'principal_curvature_min', 'principal_curvature_max', 'meristem']),
                                                                (1, []),
                                                                (2, ['area', 'mean_curvature', 'gaussian_curvature', 'principal_curvature_min', 'principal_curvature_max', 'meristem']),
                                                                (3, [])]))


